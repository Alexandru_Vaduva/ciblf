# LaTeX2HTML 2002 (1.62)
# Associate images original text with physical files.


$key = q/{mbox{boldmathmu}}_{jm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img371.png"
 ALT="$ {\mbox{\boldmath $\mu$}}_{jm}$">|; 

$key = q/fbox{texttt{Unmark}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img687.png"
 ALT="\fbox{\texttt{Unmark}}">|; 

$key = q/+15057;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img969.png"
 ALT="$ +15057$">|; 

$key = q/displaystylemathcalE_{textrm{word-{itn}-gram}}(w_1,ldots,w_{i})=mathcalE(w_{i-n+1},ldots,w_{i});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="314" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img505.png"
 ALT="$\displaystyle \mathcal E_{\textrm{word-{\it n}-gram}}(w_1, \ldots, w_{i}) = \mathcal E(w_{i-n+1}, \ldots, w_{i})$">|; 

$key = q/displaystyle{mbox{boldmathB}}_m={mbox{boldmathC}}_m^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img347.png"
 ALT="$\displaystyle {\mbox{\boldmath$B$}}_m = {\mbox{\boldmath$C$}}_m^{-1}
$">|; 

$key = q/+8522;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img949.png"
 ALT="$ +8522$">|; 

$key = q/%latex2htmlidmarker49525textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}ef{Fig.thechapter.arabic{figctr}ExampleTranscriptions}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="582" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img209.png"
 ALT="% latex2html id marker 49525
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Example Transcriptions}
\end{center}\end{center} }$">|; 

$key = q/displaystyleP_{textrm{class'}}(w_i;|;w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img514.png"
 ALT="$\displaystyle P_{\textrm{class'}}(w_i \;\vert\; w_{i-1})$">|; 

$key = q/displaystylealpha^2{bfp}_{ri}{bfG}^{(i)-1}_r{bfp}_{ri}^T+alpha{bfp}_{ri}{bfG}^{(i)-1}_r{bfk}^{(i)T}_r-beta=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="301" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img441.png"
 ALT="$\displaystyle \alpha^2{\bf p}_{ri}{\bf G}^{(i)-1}_r{\bf p}_{ri}^T +
\alpha{\bf p}_{ri}{\bf G}^{(i)-1}_r{\bf k}^{(i)T}_r - \beta=0$">|; 

$key = q/displaystyle{calQ}({calM},{hat{calM}})=K+frac{1}{2}sum_{r=1}^Rleft[betalog({bfp}boldmathw}}^T_{rj}-2{mbox{boldmathw}}_{rj}{bfk}^{(j)}_rright)}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="513" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img433.png"
 ALT="$\displaystyle {\cal Q}({\cal M},{\hat{\cal M}}) = K +
\frac{1}{2}
\sum_{r=1}^R\...
...ldmath$w$}}^T_{rj} - 2{\mbox{\boldmath$w$}}_{rj}{\bf k}^{(j)}_r
\right)}\right]$">|; 

$key = q/+15525;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img994.png"
 ALT="$ +15525$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img593.png"
 ALT="$ H$">|; 

$key = q/N_q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img264.png"
 ALT="$ N_q$">|; 

$key = q/displaystyle{s^{prime}}_n=left{0.54-0.46cosleft(frac{2pi(n-1)}{N-1}right)right}s_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img132.png"
 ALT="$\displaystyle {s^{\prime}}_n = \left\{ 0.54 - 0.46 \cos \left( \frac{2 \pi (n-1)}{N-1} \right) \right\} s_n$">|; 

$key = q/pm8151;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img937.png"
 ALT="$ \pm 8151$">|; 

$key = q/framebox[13.5cm]{parbox{12cm}{vspace{0.5cm}enumerate{item{bfseriesInitialise}:{f_{mathrm{M}_mathrm{C}}{enumerate{enumerate{enumerate{vspace{0.5cm}}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="614" HEIGHT="432" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img555.png"
 ALT="\framebox[13.5cm]{\parbox{12cm}{\vspace{0.5cm}
\begin{enumerate}
\item {\bfserie...
...hrm{M}_\mathrm{C}}$\end{enumerate}\end{enumerate}\end{enumerate}\vspace{0.5cm}}}">|; 

$key = q/left{R_1,dots,R_Cright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="97" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img405.png"
 ALT="$ \left\{R_1,\dots,R_C\right\}$">|; 

$key = q/hat{P}(O|M);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img90.png"
 ALT="$ \hat{P}(O\vert M)$">|; 

$key = q/frac{1}{A}sum_{age1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img570.png"
 ALT="$ \frac{1}{A}\sum_{a\ge 1}$">|; 

$key = q/+??11;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img720.png"
 ALT="$ +??11$">|; 

$key = q/pm3130;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img812.png"
 ALT="$ \pm 3130$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img140.png"
 ALT="$ p$">|; 

$key = q/ntimesleft(n+1right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img334.png"
 ALT="$ n \times \left( n + 1 \right)$">|; 

$key = q/displaystylephi_j(t)=max_ileft{phi_i(t-1)a_{ij}right}b_j({mbox{boldmatho}}_t).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="236" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img87.png"
 ALT="$\displaystyle \phi_j(t) = \max_i \left\{ \phi_i(t-1) a_{ij} \right\} b_j({\mbox{\boldmath$o$}}_t).$">|; 

$key = q/p(w_n|w_1,word_2,ldots,w_{n-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="189" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img620.png"
 ALT="$ p(w_n \vert w_1, word_2, \ldots, w_{n-1})$">|; 

$key = q/c_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img567.png"
 ALT="$ c_a$">|; 

$key = q/+7250;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img922.png"
 ALT="$ +7250$">|; 

$key = q/+1032;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img729.png"
 ALT="$ +1032$">|; 

$key = q/frac{1}{c_0};(1;-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img569.png"
 ALT="$ \frac{1}{c_0} \; ( 1\;-$">|; 

$key = q/+6521;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img889.png"
 ALT="$ +6521$">|; 

$key = q/+15330;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img978.png"
 ALT="$ +15330$">|; 

$key = q/x_i^{(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img452.png"
 ALT="$ x_i^{(n)}$">|; 

$key = q/mathcalE(.);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img499.png"
 ALT="$ \mathcal
E(.)$">|; 

$key = q/%latex2htmlidmarker50839textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}ef{Fig.thechapter.arabic{figctr}Abinaryregressiontree}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="306" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img357.png"
 ALT="% latex2html id marker 50839
$\textstyle \parbox{55mm}{ \begin{center}\setlength...
...apter.\arabic{figctr}  A binary regression tree}
\end{center}\end{center} }$">|; 

$key = q/+6173;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img859.png"
 ALT="$ +6173$">|; 

$key = q/+2126;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img757.png"
 ALT="$ +2126$">|; 

$key = q/displaystylealpha^{(q)}_{N_q}(t)=sum_{i=2}^{N_q-1}alpha^{(q)}_{i}(t)a^{(q)}_{iN_q};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img299.png"
 ALT="$\displaystyle \alpha^{(q)}_{N_q}(t) =
\sum_{i=2}^{N_q-1} \alpha^{(q)}_{i}(t) a^{(q)}_{iN_q}
$">|; 

$key = q/displaystylehatP(w_i|w_{i-n+1},ldots,w_{i-1})=frac{C(w_{i-n+1},ldots,w_i)}{C(w_{i-n+1},ldots,w_{i-1})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="338" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img493.png"
 ALT="$\displaystyle \hat P(w_i \vert w_{i-n+1}, \ldots, w_{i-1}) = \frac{C(w_{i-n+1}, \ldots, w_i)}{C(w_{i-n+1}, \ldots, w_{i-1})}$">|; 

$key = q/textstyleparbox{40mm}{center{setlength{epsfxsize}{40mm}epsfbox{HTKFigsslashslashoverview.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="185" HEIGHT="232" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img2.png"
 ALT="$\textstyle \parbox{40mm}{ \begin{center}\setlength{\epsfxsize}{40mm}
\epsfbox{HTKFigs//overview.eps}
\end{center} }$">|; 

$key = q/+2020;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img744.png"
 ALT="$ +2020$">|; 

$key = q/%latex2htmlidmarker48336textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}ehechapter.arabic{figctr}MessageEncodingslashDecoding}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="265" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img3.png"
 ALT="% latex2html id marker 48336
$\textstyle \parbox{50mm}{ \begin{center}\setlength...
...pter.\arabic{figctr}  Message Encoding/Decoding}
\end{center}\end{center} }$">|; 

$key = q/i=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img158.png"
 ALT="$ i=1$">|; 

$key = q/+15450;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img990.png"
 ALT="$ +15450$">|; 

$key = q/{mbox{boldmathk}}^{(i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="20" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img403.png"
 ALT="$ {\mbox{\boldmath $k$}}^{(i)}$">|; 

$key = q/P(S_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img679.png"
 ALT="$ P(S_k)$">|; 

$key = q/(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img985.png"
 ALT="$ (a,b)$">|; 

$key = q/surd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img627.png"
 ALT="$ \surd$">|; 

$key = q/%latex2htmlidmarker51243textstyleparbox{110mm}{center{setlength{epsfxsize}{110mmabic{figctr}DictionaryConstructionusingtextsc{HDMan}}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="502" HEIGHT="504" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img469.png"
 ALT="% latex2html id marker 51243
$\textstyle \parbox{110mm}{ \begin{center}\setlengt...
...}  Dictionary Construction using \textsc{HDMan}}
\end{center}\end{center} }$">|; 

$key = q/N=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img475.png"
 ALT="$ N=1$">|; 

$key = q/displaystyle{mbox{boldmathW}}=left[mbox{}{mbox{boldmathb}}mbox{}{mbox{boldmathA}}mbox{}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="92" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img338.png"
 ALT="$\displaystyle {\mbox{\boldmath$W$}} = \left[\mbox{ }{\mbox{\boldmath$b$}}\mbox{ }{\mbox{\boldmath$A$}}\mbox{ }\right]$">|; 

$key = q/+6552;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img892.png"
 ALT="$ +6552$">|; 

$key = q/t=T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img300.png"
 ALT="$ t=T$">|; 

$key = q/{mbox{boldmathG}}^{(i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img402.png"
 ALT="$ {\mbox{\boldmath $G$}}^{(i)}$">|; 

$key = q/+15152;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img973.png"
 ALT="$ +15152$">|; 

$key = q/%latex2htmlidmarker48669textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmchapter.arabic{figctr}RecordingWordBoundaryDecisions}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="354" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img98.png"
 ALT="% latex2html id marker 48669
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...abic{figctr}  Recording Word Boundary Decisions}
\end{center}\end{center} }$">|; 

$key = q/v_s({mbox{boldmatho}}_{st});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img218.png"
 ALT="$ v_s({\mbox{\boldmath $o$}}_{st})$">|; 

$key = q/displaystyleleft{{array}{l@{quad:quad}l}alpha(w_{i-n+1},ldots,w_{i-1}),.,hat{P}()}{c(w_{i-n+1},ldots,w_{i-1})}&c(w_{i-n+1},ldots,w_i)>k{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="558" HEIGHT="86" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img587.png"
 ALT="$\displaystyle \left\{ \begin{array}{l@{\quad:\quad}l} \alpha(w_{i-n+1}, \ldots,...
...{i-n+1}, \ldots, w_{i-1})} &amp; c(w_{i-n+1}, \ldots, w_i) &gt; k \end{array}\right.$">|; 

$key = q/P_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img312.png"
 ALT="$ P_r$">|; 

$key = q/%latex2htmlidmarker51235textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}eer{textbf{Fig.thechapter.arabic{figctr}DecimalSyntax}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="184" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img467.png"
 ALT="% latex2html id marker 51235
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...ig. \thechapter.\arabic{figctr}  Decimal Syntax}
\end{center}\end{center} }$">|; 

$key = q/);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img661.png"
 ALT="$ )$">|; 

$key = q/+15620;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img997.png"
 ALT="$ +15620$">|; 

$key = q/<<;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img656.png"
 ALT="$ &#171;$">|; 

$key = q/framebox[80mm]{minipage{{80mm}program{mbox{{sim{textsf{o}}>mbox{{<{textsf{HMMSettextsf{u``mean12''}}>>mbox{{sim{textsf{v``var3''}}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="484" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img250.png"
 ALT="\framebox[80mm]{
\begin{minipage}{80mm}
\begin{program}
\mbox{$\sim$\textsf{o}...
...n12''}} \\\\
\&gt;\&gt; \mbox{$\sim$\textsf{v \lq\lq var3''}}
\end{program} \end{minipage} }">|; 

$key = q/-2189;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img763.png"
 ALT="$ -2189$">|; 

$key = q/(w_1,w_2,w_3,ldots);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="112" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img522.png"
 ALT="$ (w_1, w_2, w_3,
\ldots)$">|; 

$key = q/%latex2htmlidmarker48668textstyleparbox{65mm}{center{setlength{epsfxsize}{65mm}ecognitionNetworkforContinuouslySpokenWordRecognition}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="298" HEIGHT="328" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img97.png"
 ALT="% latex2html id marker 48668
$\textstyle \parbox{65mm}{ \begin{center}\setlength...
... Network for Continuously
Spoken Word Recognition}
\end{center}\end{center} }$">|; 

$key = q/+6254;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img868.png"
 ALT="$ +6254$">|; 

$key = q/hat{{mbox{boldmathSigma}}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img414.png"
 ALT="$ \hat{{\mbox{\boldmath $\Sigma$}}}_{m_r}$">|; 

$key = q/RND();MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img134.png"
 ALT="$ RND()$">|; 

$key = q/Q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img263.png"
 ALT="$ Q$">|; 

$key = q/fbox{texttt{Rec}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img694.png"
 ALT="\fbox{\texttt{Rec}}">|; 

$key = q/displaystylebeta^{(q)}_i(T)=a^{(q)}_{iN_q}beta^{(q)}_{N_q}(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="157" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img302.png"
 ALT="$\displaystyle \beta^{(q)}_i(T) = a^{(q)}_{iN_q} \beta^{(q)}_{N_q}(T)
$">|; 

$key = q/+5071;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img835.png"
 ALT="$ +5071$">|; 

$key = q/N=sum_{i=1}^Lmax[N(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img650.png"
 ALT="$ N = \sum_{i=1}^L \max [N(i)$">|; 

$key = q/+6374;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img885.png"
 ALT="$ +6374$">|; 

$key = q/+2327;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img776.png"
 ALT="$ +2327$">|; 

$key = q/-15540;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img995.png"
 ALT="$ -15540$">|; 

$key = q/+16625;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img999.png"
 ALT="$ +16625$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img525.png"
 ALT="$ y$">|; 

$key = q/displaystylephi_j(1)=a_{1j}b_j({mbox{boldmatho}}_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img89.png"
 ALT="$\displaystyle \phi_j(1) = a_{1j} b_j({\mbox{\boldmath$o$}}_1)$">|; 

$key = q/+1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img187.png"
 ALT="$ +1.0$">|; 

$key = q/+2221;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img765.png"
 ALT="$ +2221$">|; 

$key = q/+8230;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img940.png"
 ALT="$ +8230$">|; 

$key = q/-3330;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img822.png"
 ALT="$ -3330$">|; 

$key = q/displaystyleP({mbox{boldmathO}},X|M)=a_{12}b_2({mbox{boldmatho}}_1)a_{22}b_2({mbox{boldmatho}}_2)a_{23}b_3({mbox{boldmatho}}_3)ldots;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="322" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img24.png"
 ALT="$\displaystyle P({\mbox{\boldmath$O$}},X\vert M) = a_{12} b_2({\mbox{\boldmath$o...
... a_{22} b_2({\mbox{\boldmath$o$}}_2) a_{23} b_3({\mbox{\boldmath$o$}}_3) \ldots$">|; 

$key = q/displaystyled_t=c_t-c_{t-1},;;;tgeqT-Theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="191" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img182.png"
 ALT="$\displaystyle d_t = c_t - c_{t-1}, \;\;\; t \geq T-\Theta$">|; 

$key = q/displaystylebeta^{(q)}_1(T)=sum^{N_q-1}_{j=2}a^{(q)}_{1j}b^{(q)}_j({mbox{boldmatho}}_T)beta^{(q)}_j(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="243" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img303.png"
 ALT="$\displaystyle \beta^{(q)}_1(T) = \sum^{N_q - 1}_{j=2}
a^{(q)}_{1j} b^{(q)}_j({\mbox{\boldmath$o$}}_T) \beta^{(q)}_j(T)
$">|; 

$key = q/pm1330;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img736.png"
 ALT="$ \pm 1330$">|; 

$key = q/O(log_2N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img448.png"
 ALT="$ O(\log_2 N)$">|; 

$key = q/-7324;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img929.png"
 ALT="$ -7324$">|; 

$key = q/Rightarrow;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img654.png"
 ALT="$ \Rightarrow$">|; 

$key = q/-15451;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img991.png"
 ALT="$ -15451$">|; 

$key = q/1.0times10^{-20};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="83" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img624.png"
 ALT="$ 1.0\times10^{-20}$">|; 

$key = q/{a_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img142.png"
 ALT="$ \{a_i \}$">|; 

$key = q/c_{jsm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img42.png"
 ALT="$ c_{jsm}$">|; 

$key = q/displaystylephi_N(T)=max_iphi_i(T)a_{iN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img270.png"
 ALT="$\displaystyle \phi_N(T) = \max_i \phi_i(T) a_{iN}
$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.labs.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="355" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img208.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.labs.eps}
\end{center} }$">|; 

$key = q/displaystyleL_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img79.png"
 ALT="$\displaystyle L_j(t)$">|; 

$key = q/displaystylek_i^{(i)}=left{r_i+sum_{j=1}^{i-1}a_j^{(i-1)}r_{i-j}right}slashE^{(i-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img154.png"
 ALT="$\displaystyle k_i^{(i)} = \left\{ r_i + \sum_{j=1}^{i-1} a_j^{(i-1)} r_{i-j} \right\} / E^{(i-1)}$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.disc.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="369" HEIGHT="326" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img446.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.disc.eps}
\end{center} }$">|; 

$key = q/%latex2htmlidmarker49217textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmg.thechapter.arabic{figctr}ExampleStreamConstruction}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="366" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img204.png"
 ALT="% latex2html id marker 49217
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...er.\arabic{figctr}  Example Stream Construction}
\end{center}\end{center} }$">|; 

$key = q/{mbox{boldmathSigma}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img383.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}_{m_r}$">|; 

$key = q/%latex2htmlidmarker51203textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}e.thechapter.arabic{figctr}AWordNetworkUsingNullNodes}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="209" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img458.png"
 ALT="% latex2html id marker 51203
$\textstyle \parbox{80mm}{ \begin{center}\setlength...
...arabic{figctr}  A Word Network Using Null Nodes}
\end{center}\end{center} }$">|; 

$key = q/%latex2htmlidmarker51266textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}ef{Fig.thechapter.arabic{figctr}RecognitionProcessing}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="839" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img476.png"
 ALT="% latex2html id marker 51266
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Recognition Processing}
\end{center}\end{center} }$">|; 

$key = q/pm1450;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img740.png"
 ALT="$ \pm 1450$">|; 

$key = q/mathcal{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img559.png"
 ALT="$ \mathcal{A}$">|; 

$key = q/+6020;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img850.png"
 ALT="$ +6020$">|; 

$key = q/hat{mathcal{M}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="19" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img376.png"
 ALT="$ \hat{\mathcal{M}}$">|; 

$key = q/{d_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img222.png"
 ALT="$ \{d_k\}$">|; 

$key = q/x_{max};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img200.png"
 ALT="$ x_{max}$">|; 

$key = q/+7323;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img928.png"
 ALT="$ +7323$">|; 

$key = q/alphabeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img629.png"
 ALT="$ \alpha \beta $">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{m_r}={mbox{boldmathH}}_r{mbox{boldmathmu}}_x{boldmathH}}}_r{{mbox{boldmathSigma}}}_{m_r}{{mbox{boldmathH}}}_r^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="300" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img432.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{m_r} = {\mbox{\boldmath$H$}}_r{\mb...
...{\boldmath$H$}}}_r{{\mbox{\boldmath$\Sigma$}}}_{m_r}{{\mbox{\boldmath$H$}}}_r^T$">|; 

$key = q/+15055;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img967.png"
 ALT="$ +15055$">|; 

$key = q/q_{m_r}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img391.png"
 ALT="$ q_{m_r}(t)$">|; 

$key = q/%latex2htmlidmarker49825textstyleparbox{60mm}{noindentfbox{parbox{60mm}{hspace{6{textbf{Fig.thechapter.arabic{figctr}ADefinitionUsingMacros}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="639" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img235.png"
 ALT="% latex2html id marker 49825
$\textstyle \parbox{60mm}{\noindent
\fbox{ \parbox...
...g. \thechapter.\arabic{figctr}  A Definition Using Macros}
\end{center}
}$">|; 

$key = q/0.5;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img651.png"
 ALT="$ 0.5$">|; 

$key = q/displaystylehat{P}({mbox{boldmathO}}|M)=max_Xleft{a_{x(0)x(1)}prod_{t=1}^Tb_{x(t)}({mbox{boldmatho}}_t)a_{x(t)x(t+1)}right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="352" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img30.png"
 ALT="$\displaystyle \hat{P}({\mbox{\boldmath$O$}}\vert M) = \max_X \left\{ a_{x(0)x(1)} \prod_{t=1}^T b_{x(t)}({\mbox{\boldmath$o$}}_t) a_{x(t)x(t+1)} \right\}$">|; 

$key = q/alpha_j(t)beta_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img261.png"
 ALT="$ \alpha_j(t)\beta_j(t)$">|; 

$key = q/{mbox{boldmathH}}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img343.png"
 ALT="$ {\mbox{\boldmath $H$}}_m$">|; 

$key = q/%latex2htmlidmarker51236textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmchapter.arabic{figctr}Back-offBigramWord-LoopNetwork}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="394" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img468.png"
 ALT="% latex2html id marker 51236
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...abic{figctr}  Back-off Bigram Word-Loop Network}
\end{center}\end{center} }$">|; 

$key = q/+7025;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img902.png"
 ALT="$ +7025$">|; 

$key = q/displaystylehat{a}_{iN}=frac{sum_{r=1}^Rfrac{1}{P_r}alpha^r_i(T)beta^r_i(T)}{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r}alpha^r_i(t)beta^r_i(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="229" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img316.png"
 ALT="$\displaystyle \hat{a}_{iN} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\alpha^r_i(T)\be...
...(T)
}{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=1}^{T_r}
\alpha^r_i(t)\beta^r_i(t)
}
$">|; 

$key = q/M_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img31.png"
 ALT="$ M_i$">|; 

$key = q/%latex2htmlidmarker49211textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmbf{Fig.thechapter.arabic{figctr}EndpointerParameters}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="360" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img203.png"
 ALT="% latex2html id marker 49211
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...echapter.\arabic{figctr}  Endpointer Parameters}
\end{center}\end{center} }$">|; 

$key = q/+2422;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img783.png"
 ALT="$ +2422$">|; 

$key = q/fbox{texttt{Speech}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img713.png"
 ALT="\fbox{\texttt{Speech}}">|; 

$key = q/displaystyleH=-lim_{mtoinfty}frac{1}{m}log_2P(w_1,w_2,ldots,w_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="273" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img595.png"
 ALT="$\displaystyle H = - \lim_{m \to \infty} \frac{1}{m} \log_2 P(w_1, w_2, \ldots, w_m)$">|; 

$key = q/displaystyle;=;;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img538.png"
 ALT="$\displaystyle \;=\;$">|; 

$key = q/displaystylealpha_j(t)beta_j(t)=P({mbox{boldmathO}},x(t)=j|M).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="222" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img78.png"
 ALT="$\displaystyle \alpha_j(t) \beta_j(t) = P({\mbox{\boldmath$O$}},x(t)=j \vert M).$">|; 

$key = q/-3289;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img821.png"
 ALT="$ -3289$">|; 

$key = q/mem_{bytes}=(n+1)*4*b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="185" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img608.png"
 ALT="$ mem_{bytes} = (n+1)*4*b$">|; 

$key = q/i,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img465.png"
 ALT="$ i,j$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img1.png"
 ALT="$ n$">|; 

$key = q/{bfW}_4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img361.png"
 ALT="$ {\bf W}_4$">|; 

$key = q/%latex2htmlidmarker49886textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmextbf{Fig.thechapter.arabic{figctr}DefiningaModelSet}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="478" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img239.png"
 ALT="% latex2html id marker 49886
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...hechapter.\arabic{figctr}  Defining a Model Set}
\end{center}\end{center} }$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}_{m}={mbox{boldmathB}}_m^T{mbox{boldmathH}}_m{mbox{boldmathB}}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="133" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img342.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}}_{m} = {\mbox{\boldmath$B$}}_m^T{\mbox{\boldmath$H$}}_m{\mbox{\boldmath$B$}}_m
$">|; 

$key = q/+1030;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img727.png"
 ALT="$ +1030$">|; 

$key = q/+6171;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img857.png"
 ALT="$ +6171$">|; 

$key = q/(w_1,w_2,ldots,w_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img623.png"
 ALT="$ (w_1, w_2, \ldots,
w_n)$">|; 

$key = q/+2124;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img755.png"
 ALT="$ +2124$">|; 

$key = q/%latex2htmlidmarker48717textstyleparbox{85mm}{center{setlength{epsfxsize}{85mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step6}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="393" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img113.png"
 ALT="% latex2html id marker 48717
$\textstyle \parbox{85mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 6}
\end{center}\end{center} }$">|; 

$key = q/{m_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img170.png"
 ALT="$ \{m_j\}$">|; 

$key = q/displaystylealpha_N(T)=sum_{i=2}^{N-1}alpha_i(T)a_{iN}.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="170" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img69.png"
 ALT="$\displaystyle \alpha_N(T) = \sum_{i=2}^{N-1} \alpha_i(T) a_{iN}.$">|; 

$key = q/{mbox{boldmathxi}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img335.png"
 ALT="$ {\mbox{\boldmath $\xi$}}$">|; 

$key = q/P({mbox{boldmathO}}|M);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img71.png"
 ALT="$ P({\mbox{\boldmath $O$}}\vert M)$">|; 

$key = q/-16640;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1002.png"
 ALT="$ -16640$">|; 

$key = q/+15345;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img981.png"
 ALT="$ +15345$">|; 

$key = q/w_1,w_2,ldots,w_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="107" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img591.png"
 ALT="$ w_1, w_2,
\ldots, w_m$">|; 

$key = q/ast;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img251.png"
 ALT="$ \ast$">|; 

$key = q/c(.);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img588.png"
 ALT="$ c(.)$">|; 

$key = q/+6221;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img862.png"
 ALT="$ +6221$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashrecipe.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="235" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img104.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//recipe.eps}
\end{center} }$">|; 

$key = q/+2662;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img808.png"
 ALT="$ +2662$">|; 

$key = q/{{mbox{boldmathW}}}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img400.png"
 ALT="$ {{\mbox{\boldmath $W$}}}_r$">|; 

$key = q/G(w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img513.png"
 ALT="$ G(w_{i-1})$">|; 

$key = q/{figure}preform{<verbatim_mark>verbatim561#preform{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="376" HEIGHT="147" BORDER="0"
 SRC="|."$dir".q|img356.png"
 ALT="\begin{figure}\begin{verbatim}&nbsp;b \lq\lq baseclass_4.base''
&lt;MMFIDMASK&gt; CUED_WSJ*
...
...2-4].mix[1-12]}
&lt;CLASS&gt; 4 {four.state[2-4].mix[1-12]}\end{verbatim}\end{figure}">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img165.png"
 ALT="$ \alpha$">|; 

$key = q/+8253;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img946.png"
 ALT="$ +8253$">|; 

$key = q/%latex2htmlidmarker48719textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}eer{textbf{Fig.thechapter.arabic{figctr}SilenceModels}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="322" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img114.png"
 ALT="% latex2html id marker 48719
$\textstyle \parbox{55mm}{ \begin{center}\setlength...
...ig. \thechapter.\arabic{figctr}  Silence Models}
\end{center}\end{center} }$">|; 

$key = q/displaystylep(i,j)=left{{array}{ll}(N(i,j)-D)slashN(i)&mbox{ifN(i,j)>t}b(i)p(j)&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="332" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img460.png"
 ALT="$\displaystyle p(i,j) = \left\{
\begin{array}{ll}
(N(i,j) - D )/N(i) &amp; \mbox{if $N(i,j) &gt; t$} \\\\
b(i) p(j) &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{m_r}={mbox{boldmathW}}_r{mbox{boldmathxi}}_},::::hat{{mbox{boldmathSigma}}}_{m_r}={{mbox{boldmathSigma}}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="215" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img393.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{m_r} = {\mbox{\boldmath$W$}}_r{\mb...
...:\:
\hat{{\mbox{\boldmath$\Sigma$}}}_{m_r} = {{\mbox{\boldmath$\Sigma$}}}_{m_r}$">|; 

$key = q/+6550;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img890.png"
 ALT="$ +6550$">|; 

$key = q/+3232;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img819.png"
 ALT="$ +3232$">|; 

$key = q/+7070;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img911.png"
 ALT="$ +7070$">|; 

$key = q/t>1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img296.png"
 ALT="$ t &gt; 1$">|; 

$key = q/+15150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img971.png"
 ALT="$ +15150$">|; 

$key = q/displaystyleb_{j}({mbox{boldmatho}}_t)=prod_{s=1}^Sleft{P_{js}[v_s({mbox{boldmatho}}_{st})]right}^{gamma_s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="200" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img217.png"
 ALT="$\displaystyle b_{j}({\mbox{\boldmath$o$}}_t) = \prod_{s=1}^S \left\{ P_{js}[v_s({\mbox{\boldmath$o$}}_{st})] \right\}^{\gamma_s}$">|; 

$key = q/1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img174.png"
 ALT="$ 1.0$">|; 

$key = q/-2326;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img775.png"
 ALT="$ -2326$">|; 

$key = q/t-deltat;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img64.png"
 ALT="$ t-\delta t$">|; 

$key = q/+7120;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img913.png"
 ALT="$ +7120$">|; 

$key = q/%latex2htmlidmarker49818textstyleparbox{50mm}{noindentfbox{parbox{50mm}{hspace{5{textbf{Fig.thechapter.arabic{figctr}SimpleMacroDefinitions}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="251" HEIGHT="238" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img234.png"
 ALT="% latex2html id marker 49818
$\textstyle \parbox{50mm}{\noindent
\fbox{ \parbox...
...ig. \thechapter.\arabic{figctr}  Simple Macro Definitions}
\end{center}
}$">|; 

$key = q/t_s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img667.png"
 ALT="$ t_s$">|; 

$key = q/;;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img625.png"
 ALT="$ \;$">|; 

$key = q/+6252;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img866.png"
 ALT="$ +6252$">|; 

$key = q/+??01;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img716.png"
 ALT="$ +??01$">|; 

$key = q/%latex2htmlidmarker48696textstyleparbox{110mm}{center{setlength{epsfxsize}{110mmFig.thechapter.arabic{figctr}GrammarforVoiceDialling}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="502" HEIGHT="401" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img105.png"
 ALT="% latex2html id marker 48696
$\textstyle \parbox{110mm}{ \begin{center}\setlengt...
...ter.\arabic{figctr}  Grammar for Voice Dialling}
\end{center}\end{center} }$">|; 

$key = q/{mbox{boldmathA}}={mbox{boldmathH}}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img351.png"
 ALT="$ {\mbox{\boldmath $A$}}={\mbox{\boldmath $H$}}^{-1}$">|; 

$key = q/displaystylealpha^{(q)}_{1}(t)=left{{array}{cl}0&mbox{ifq=1}alpha^{(q-1)}_{N_{q-+alpha^{(q-1)}_1(t)a^{(q-1)}_{1N_{q-1}}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="399" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img297.png"
 ALT="$\displaystyle \alpha^{(q)}_{1}(t) =
\left\{ \begin{array}{cl}
0 &amp; \mbox{if $q=...
...
\alpha^{(q-1)}_1(t) a^{(q-1)}_{1N_{q-1}}&amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/framebox[60mm]{minipage{{60mm}program{mbox{{sim{textsf{o}}mbox{{<{textsf{VecSize}>>>20.90.7>mbox{{<{textsf{Variance}{>{}>>>21.51.0program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="332" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img243.png"
 ALT="\framebox[60mm]{
\begin{minipage}{60mm}
\begin{program}
\mbox{$\sim$\textsf{o}...
... \mbox{$&lt;$\textsf{Variance}$&gt;$} \&gt;\&gt;\&gt; 2 1.5 1.0
\end{program} \end{minipage} }">|; 

$key = q/displaystyle=frac{H-I}{N}times100%;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img666.png"
 ALT="$\displaystyle = \frac{H-I}{N} \times 100\%$">|; 

$key = q/%latex2htmlidmarker50437textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}etbf{Fig.thechapter.arabic{figctr}TrainingSubwordHMMs}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="599" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img255.png"
 ALT="% latex2html id marker 50437
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...echapter.\arabic{figctr}  Training Subword HMMs}
\end{center}\end{center} }$">|; 

$key = q/+5175;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img843.png"
 ALT="$ +5175$">|; 

$key = q/M_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img386.png"
 ALT="$ M_r$">|; 

$key = q/+7031;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img904.png"
 ALT="$ +7031$">|; 

$key = q/beta_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img72.png"
 ALT="$ \beta_j(t)$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img641.png"
 ALT="$ c$">|; 

$key = q/forallw:;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img501.png"
 ALT="$ \forall w:$">|; 

$key = q/{{mbox{boldmathw}}}_{rj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img395.png"
 ALT="$ {{\mbox{\boldmath $w$}}}_{rj}$">|; 

$key = q/+6372;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img883.png"
 ALT="$ +6372$">|; 

$key = q/%latex2htmlidmarker50442textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}eFig.thechapter.arabic{figctr}FlatStartInitialisation}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="274" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img258.png"
 ALT="% latex2html id marker 50442
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...pter.\arabic{figctr}  Flat Start Initialisation}
\end{center}\end{center} }$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img337.png"
 ALT="$ w$">|; 

$key = q/a_0equiv1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img141.png"
 ALT="$ a_0 \equiv 1$">|; 

$key = q/{mbox{boldmathA}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img339.png"
 ALT="$ {\mbox{\boldmath $A$}}$">|; 

$key = q/times32;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img703.png"
 ALT="$ \times 32$">|; 

$key = q/pm3031;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img811.png"
 ALT="$ \pm 3031$">|; 

$key = q/{w_1w_2ldotsw_N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img614.png"
 ALT="$ \{w_1 w_2
\ldots w_N\}$">|; 

$key = q/hat{beta}(w_{i-n+1},ldotsw_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img584.png"
 ALT="$ \hat{\beta}(w_{i-n+1},
\ldots w_{i-1})$">|; 

$key = q/pm8520;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img947.png"
 ALT="$ \pm 8520$">|; 

$key = q/({mbox{boldmathO}}^r|lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img313.png"
 ALT="$ ({\mbox{\boldmath $O$}}^r \vert \lambda)$">|; 

$key = q/+15440;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img987.png"
 ALT="$ +15440$">|; 

$key = q/+??9?;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img726.png"
 ALT="$ +??9?$">|; 

$key = q/%latex2htmlidmarker49899textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}eextbf{Fig.thechapter.arabic{figctr}TiedMixtureSystem}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="258" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img241.png"
 ALT="% latex2html id marker 49899
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...thechapter.\arabic{figctr}  Tied Mixture System}
\end{center}\end{center} }$">|; 

$key = q/displaystyle{bfa}_{ri}={bfc}_{ri}{bfG}^{(i)-1}_rsqrt{left(frac{beta_r}{{bfc}_{ri}{bfG}_r^{(i)-1}{bfc}^T_{ri}}right)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="242" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img430.png"
 ALT="$\displaystyle {\bf a}_{ri} ={\bf c}_{ri}{\bf G}^{(i)-1}_r
\sqrt{\left(\frac{\beta_r}{{\bf c}_{ri}{\bf G}_r^{(i)-1}{\bf c}^T_{ri}}\right)}$">|; 

$key = q/P({mbox{boldmathO}}|M_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img85.png"
 ALT="$ P({\mbox{\boldmath $O$}}\vert M_i)$">|; 

$key = q/(w_1,w_2,ldots,w_{n-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="132" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img619.png"
 ALT="$ (w_1, w_2, \ldots, w_{n-1})$">|; 

$key = q/displaystylebeta_1(1)=sum_{j=2}^{N-1}a_{1j}b_j({mbox{boldmatho}}_1)beta_j(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="192" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img287.png"
 ALT="$\displaystyle \beta_1(1) = \sum_{j=2}^{N-1} a_{1j} b_j({\mbox{\boldmath$o$}}_1) \beta_j(1)
$">|; 

$key = q/{bfc}_{ri};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img425.png"
 ALT="$ {\bf c}_{ri}$">|; 

$key = q/%latex2htmlidmarker48704textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step3}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="376" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img108.png"
 ALT="% latex2html id marker 48704
$\textstyle \parbox{50mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 3}
\end{center}\end{center} }$">|; 

$key = q/%latex2htmlidmarker48399textstyleparbox{85mm}{center{setlength{epsfxsize}{85mm}eig.thechapter.arabic{figctr}TheMarkovGenerationModel}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="345" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img25.png"
 ALT="% latex2html id marker 48399
$\textstyle \parbox{85mm}{ \begin{center}\setlength...
...er.\arabic{figctr}  The Markov Generation Model}
\end{center}\end{center} }$">|; 

$key = q/+7271;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img925.png"
 ALT="$ +7271$">|; 

$key = q/{bfA}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img423.png"
 ALT="$ {\bf A}_r$">|; 

$key = q/t<T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img304.png"
 ALT="$ t&lt;T$">|; 

$key = q/%latex2htmlidmarker49850textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmpter.arabic{figctr}HMMHierarchyandPotentialTiePoints}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="388" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img236.png"
 ALT="% latex2html id marker 49850
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...figctr}  HMM Hierarchy and Potential Tie Points}
\end{center}\end{center} }$">|; 

$key = q/T>tgeq1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img286.png"
 ALT="$ T&gt;t \geq 1$">|; 

$key = q/+2250;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img771.png"
 ALT="$ +2250$">|; 

$key = q/displaystylealpha_N(T)=sum_{i=2}^{N-1}alpha_i(T)a_{iN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="166" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img284.png"
 ALT="$\displaystyle \alpha_N(T) = \sum_{i=2}^{N-1} \alpha_i(T) a_{iN}
$">|; 

$key = q/+7321;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img926.png"
 ALT="$ +7321$">|; 

$key = q/sum_{xinmathbb{W}}C(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img549.png"
 ALT="$ \sum_{x \in \mathbb{W}} C(x)$">|; 

$key = q/+8050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img935.png"
 ALT="$ +8050$">|; 

$key = q/-8221;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img939.png"
 ALT="$ -8221$">|; 

$key = q/P_e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img677.png"
 ALT="$ P_e$">|; 

$key = q/+15053;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img965.png"
 ALT="$ +15053$">|; 

$key = q/displaystylehat{a}^{(q)}_{1j}=frac{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r-1}alphaeta^{(q)r}_1(t)+alpha^{(q)r}_{1}(t)a^{(q)}_{1N_q}beta^{(q+1)r}_1(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="432" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img326.png"
 ALT="$\displaystyle \hat{a}^{(q)}_{1j} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=1}...
...1(t)\beta^{(q)r}_1(t)
+ \alpha^{(q)r}_{1}(t)a^{(q)}_{1N_q}\beta^{(q+1)r}_1(t)}
$">|; 

$key = q/{figure}preform{<verbatim_mark>verbatim563#preform{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="324" HEIGHT="432" BORDER="0"
 SRC="|."$dir".q|img364.png"
 ALT="\begin{figure}\begin{verbatim}&nbsp;a \lq\lq cued''
&lt;ADAPTKIND&gt; CLASS
&lt;BASECLASSES&gt;&nbsp;...
...-0.032
-0.017 1.041
&lt;XFORMWGTSET&gt;
&lt;CLASSXFORM&gt; 1 1\end{verbatim}\end{figure}">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashheadapt.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="399" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img331.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//headapt.eps}
\end{center} }$">|; 

$key = q/P({O}|lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="55" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img664.png"
 ALT="$ P({O}\vert\lambda)$">|; 

$key = q/j=1,i-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img153.png"
 ALT="$ j = 1,i-1$">|; 

$key = q/+5270;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img845.png"
 ALT="$ +5270$">|; 

$key = q/X=1,2,2,3,4,4,5,6;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img19.png"
 ALT="$ X=1,2,2,3,4,4,5,6$">|; 

$key = q/{mbox{boldmathmu}}_{jsm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img266.png"
 ALT="$ {\mbox{\boldmath $\mu$}}_{jsm}$">|; 

$key = q/PP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img597.png"
 ALT="$ PP$">|; 

$key = q/b_{js}({mbox{boldmathx}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img637.png"
 ALT="$ b_{js}({\mbox{\boldmath $x$}})$">|; 

$key = q/displaystylesum_{xinmathbb{W}}C(x).logC(x);-;sum_{xinmathbb{W}}C(x).logC(G(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="313" HEIGHT="50" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img542.png"
 ALT="$\displaystyle \sum_{x \in \mathbb{W}} C(x) . \log C(x)
\;-\; \sum_{x \in \mathbb{W}} C(x) . \log C(G(x))$">|; 

$key = q/displaystyleP_{textrm{class}}(w_i;|;w_{i-1})=P(w_i;|;G(w_{i}));times;P(G(w_i);|;G(w_{i-1}));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="399" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img520.png"
 ALT="$\displaystyle P_{\textrm{class}}(w_i \;\vert\; w_{i-1}) = P(w_i \;\vert\; G(w_{i})) \;\times\; P(G(w_i) \;\vert\; G(w_{i-1}))$">|; 

$key = q/D;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img463.png"
 ALT="$ D$">|; 

$key = q/L_{m_r}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="52" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img384.png"
 ALT="$ L_{m_r}(t)$">|; 

$key = q/C(G(w));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="66" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img531.png"
 ALT="$ C(G(w))$">|; 

$key = q/displaystylefrac{P({mbox{boldmathO}},x(t)=j|M)}{P({mbox{boldmathO}}|M)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="131" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img82.png"
 ALT="$\displaystyle \frac{P({\mbox{\boldmath$O$}},x(t)=j \vert M)}{P({\mbox{\boldmath$O$}}\vert M)}$">|; 

$key = q/+2420;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img781.png"
 ALT="$ +2420$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img23.png"
 ALT="$ X$">|; 

$key = q/+5320;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img847.png"
 ALT="$ +5320$">|; 

$key = q/a=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img562.png"
 ALT="$ a=0$">|; 

$key = q/%latex2htmlidmarker51202textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}extbf{Fig.thechapter.arabic{figctr}ASimpleWordNetwork}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="234" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img457.png"
 ALT="% latex2html id marker 51202
$\textstyle \parbox{80mm}{ \begin{center}\setlength...
...echapter.\arabic{figctr}  A Simple Word Network}
\end{center}\end{center} }$">|; 

$key = q/+8220;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img938.png"
 ALT="$ +8220$">|; 

$key = q/10x-20;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img477.png"
 ALT="$ 10x - 20$">|; 

$key = q/{bfW}_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img359.png"
 ALT="$ {\bf W}_2$">|; 

$key = q/displaystylebeta^{(q)}_{N_q}(T)=left{{array}{cl}1&mbox{ifq=Q}beta^{(q+1)}_{N_{q+1}}(T)a^{(q+1)}_{1N_{q+1}}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="301" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img301.png"
 ALT="$\displaystyle \beta^{(q)}_{N_q}(T) =
\left\{ \begin{array}{cl}
1 &amp; \mbox{if $q...
...(q+1)}_{N_{q+1}}(T) a^{(q+1)}_{1N_{q+1}} &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/%latex2htmlidmarker51817textstyleparbox{110mm}{center{setlength{epsfxsize}{110mmctr}Themainstagesinbuildingaclass-basedlanguagemodel}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="502" HEIGHT="607" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img605.png"
 ALT="% latex2html id marker 51817
$\textstyle \parbox{110mm}{ \begin{center}\setlengt...
... stages in building a
class-based language model}
\end{center}\end{center} }$">|; 

$key = q/P_{max}({O}|lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="79" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img639.png"
 ALT="$ P_{max}({O}\vert\lambda)$">|; 

$key = q/P_mathrm{discount}(mathcal{A})=frac{a'}{A,};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img565.png"
 ALT="$ P_\mathrm{discount}(\mathcal{A}) = \frac{a'}{A }$">|; 

$key = q/+2228;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img770.png"
 ALT="$ +2228$">|; 

$key = q/C(w);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img530.png"
 ALT="$ C(w)$">|; 

$key = q/+2122;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img753.png"
 ALT="$ +2122$">|; 

$key = q/+5022;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img831.png"
 ALT="$ +5022$">|; 

$key = q/displaystyleD(lambda)=sum_{n=1}^Nsum_{i=1}^Mx_i^{(n)}left[frac{w_i^{(n)}-bar{w}_i^{(n)}}{lambdaw_i^{(n)}+(1-lambda)bar{w}_i^{(n)}}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="304" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img454.png"
 ALT="$\displaystyle D(\lambda) = \sum_{n=1}^N \sum_{i=1}^M x_i^{(n)} \left[ \frac{w_i...
...- \bar{w}_i^{(n)}}{ \lambda w_i^{(n)} + (1 - \lambda ) \bar{w}_i^{(n)}} \right]$">|; 

$key = q/+6325;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img876.png"
 ALT="$ +6325$">|; 

$key = q/pm1231;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img732.png"
 ALT="$ \pm 1231$">|; 

$key = q/+2660;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img806.png"
 ALT="$ +2660$">|; 

$key = q/1<i<N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img76.png"
 ALT="$ 1&lt;i&lt;N$">|; 

$key = q/displaystyleP_e=2^{H_e};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img678.png"
 ALT="$\displaystyle P_e = 2^{H_e}$">|; 

$key = q/+8251;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img944.png"
 ALT="$ +8251$">|; 

$key = q/w_1,ldots,w_{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img592.png"
 ALT="$ w_1, \ldots,
w_{i-1}$">|; 

$key = q/8631;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img957.png"
 ALT="$ 8631$">|; 

$key = q/bar{w}_i^{(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img451.png"
 ALT="$ \bar{w}_i^{(n)}$">|; 

$key = q/{mbox{boldmathH}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img349.png"
 ALT="$ {\mbox{\boldmath $H$}}$">|; 

$key = q/{bfk}^{(i)}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img437.png"
 ALT="$ {\bf k}^{(i)}_r$">|; 

$key = q/displaystyle{bfG}^{(i)}_r=sum_{m_r=1}^{M_r}frac{1}{sigma_{m_ri}^{2}}sum_{t=1}^TLthmu}}}_{m_r})({mbox{boldmatho}}(t)-hat{{mbox{boldmathmu}}}_{m_r})^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="391" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img428.png"
 ALT="$\displaystyle {\bf G}^{(i)}_r=\sum_{m_r=1}^{M_r}
\frac{1}{\sigma_{m_ri}^{2}}
\s...
...\mu$}}}_{m_r})
({\mbox{\boldmath$o$}}(t)-\hat{{\mbox{\boldmath$\mu$}}}_{m_r})^T$">|; 

$key = q/displaystylephi_1(1)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img88.png"
 ALT="$\displaystyle \phi_1(1) = 1$">|; 

$key = q/w_{i-n+1},ldots,w_{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img492.png"
 ALT="$ w_{i-n+1}, \ldots, w_{i-1}$">|; 

$key = q/-2639;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img800.png"
 ALT="$ -2639$">|; 

$key = q/fbox{texttt{Volume}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img696.png"
 ALT="\fbox{\texttt{Volume}}">|; 

$key = q/P=P({mbox{boldmathO}}|M);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="98" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img84.png"
 ALT="$ P=P({\mbox{\boldmath $O$}}\vert M)$">|; 

$key = q/displaystyleP_{js}[v]=exp(-d_{js}[v]slash2371.8);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img245.png"
 ALT="$\displaystyle P_{js}[v] = exp(-d_{js}[v]/2371.8)$">|; 

$key = q/+6250;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img864.png"
 ALT="$ +6250$">|; 

$key = q/{mbox{boldmath(2pi)^n}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img252.png"
 ALT="$ {\mbox{\boldmath $(2 \pi)^n$}}$">|; 

$key = q/displaystyleL_{m_r}(t)=p(q_{m_r}(t);|;mathcal{M},{mbox{boldmathO}}_T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="200" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img390.png"
 ALT="$\displaystyle L_{m_r}(t)= p(q_{m_r}(t)\;\vert\;\mathcal{M}, {\mbox{\boldmath$O$}}_T)
$">|; 

$key = q/{mbox{boldmathmu}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img45.png"
 ALT="$ {\mbox{\boldmath $\mu$}}$">|; 

$key = q/displaystyleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img196.png"
 ALT="$\displaystyle A$">|; 

$key = q/times16;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img702.png"
 ALT="$ \times 16$">|; 

$key = q/A=sum_{age1}a,.,c_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img568.png"
 ALT="$ A = \sum_{a\ge 1} a . c_a$">|; 

$key = q/+5173;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img841.png"
 ALT="$ +5173$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img22.png"
 ALT="$ M$">|; 

$key = q/pm1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img186.png"
 ALT="$ \pm 1$">|; 

$key = q/displaystylephi_j(t)=left[max_iphi_i(t-1)a_{ij}right]b_j({mbox{boldmatho}}_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="230" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img271.png"
 ALT="$\displaystyle \phi_j(t) = \left[ \max_i \phi_i(t-1) a_{ij} \right] b_j({\mbox{\boldmath$o$}}_t)
$">|; 

$key = q/displaystyle(f)=2595log_{10}(1+frac{f}{700});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img163.png"
 ALT="$\displaystyle (f) = 2595 \log_{10}(1 + \frac{f}{700})$">|; 

$key = q/{b_{j}({mbox{boldmatho}}_t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="61" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img34.png"
 ALT="$ \{b_{j}({\mbox{\boldmath $o$}}_t)\}$">|; 

$key = q/Deltat;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img291.png"
 ALT="$ \Delta t$">|; 

$key = q/A_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img273.png"
 ALT="$ A_{ij}$">|; 

$key = q/+2429;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img789.png"
 ALT="$ +2429$">|; 

$key = q/+??16;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img724.png"
 ALT="$ +??16$">|; 

$key = q/displaystylep(i,j)=left{{array}{ll}alphaN(i,j)slashN(i)&mbox{ifN(i)>0}1slashL&mbox{ifN(i)=0}f&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="286" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img644.png"
 ALT="$\displaystyle p(i,j) = \left\{
\begin{array}{ll}
\alpha N(i,j)/N(i) &amp; \mbox{if ...
...$} \\\\
1/L &amp; \mbox{if $N(i) = 0$} \\\\
f &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img560.png"
 ALT="$ a$">|; 

$key = q/+6370;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img881.png"
 ALT="$ +6370$">|; 

$key = q/displaystyle;-;2sum_{ginmathbb{G}}C(g).logC(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="159" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img545.png"
 ALT="$\displaystyle \;-\; 2 \sum_{g \in \mathbb{G}} C(g) . \log C(g)$">|; 

$key = q/displaystyleU^r_j(t)=left{{array}{cl}a_{1j}&mbox{ift=1}sum^{N-1}_{i=2}alpha^r_i(t-1)a_{ij}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="305" HEIGHT="56" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img318.png"
 ALT="$\displaystyle U^r_j(t) = \left\{ \begin{array}{cl} a_{1j} &amp; \mbox{if $t=1$}  \sum^{N-1}_{i=2} \alpha^r_i(t-1) a_{ij} &amp; \mbox{otherwise} \end{array} \right.$">|; 

$key = q/[-1.0,+1.0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img135.png"
 ALT="$ [-1.0, +1.0)$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img649.png"
 ALT="$ u$">|; 

$key = q/fbox{texttt{Select}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img708.png"
 ALT="\fbox{\texttt{Select}}">|; 

$key = q/times1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img698.png"
 ALT="$ \times 1$">|; 

$key = q/(n-1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img583.png"
 ALT="$ (n-1)$">|; 

$key = q/displaystyleP(w_i|{mbox{boldmathO}})=frac{P({mbox{boldmathO}}|w_i)P(w_i)}{P({mbox{boldmathO}})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="190" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img11.png"
 ALT="$\displaystyle P(w_i \vert {\mbox{\boldmath$O$}}) = \frac{P({\mbox{\boldmath$O$}}\vert w_i) P(w_i)}{P({\mbox{\boldmath$O$}})}$">|; 

$key = q/displaystylebeta^{(q)}_i(t)=a^{(q)}_{iN_q}beta^{(q)}_{N_q}(t)+sum_{j=2}^{N_q-1}a^{(q)}_{ij}b^{(q)}_j({mbox{boldmatho}}_{t+1})beta^{(q)}_{j}(t+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="365" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img306.png"
 ALT="$\displaystyle \beta^{(q)}_i(t) =
a^{(q)}_{iN_q} \beta^{(q)}_{N_q}(t) +
\sum_{j...
...-1} a^{(q)}_{ij}
b^{(q)}_j({\mbox{\boldmath$o$}}_{t+1}) \beta^{(q)}_{j}(t+1)
$">|; 

$key = q/V_s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img634.png"
 ALT="$ V_s$">|; 

$key = q/%latex2htmlidmarker48720textstyleparbox{110mm}{center{setlength{epsfxsize}{110mmctr}center{textbf{Fig.thechapter.arabic{figctr}Step7}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="502" HEIGHT="278" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img115.png"
 ALT="% latex2html id marker 48720
$\textstyle \parbox{110mm}{ \begin{center}\setlengt...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 7}
\end{center}\end{center} }$">|; 

$key = q/pm7023;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img900.png"
 ALT="$ \pm 7023$">|; 

$key = q/+17052;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1010.png"
 ALT="$ +17052$">|; 

$key = q/%latex2htmlidmarker51809textstyleparbox{65mm}{center{setlength{epsfxsize}{65mm}eigctr}Themainstagesinbuildingan{n{-gramlanguagemodel}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="298" HEIGHT="493" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img604.png"
 ALT="% latex2html id marker 51809
$\textstyle \parbox{65mm}{ \begin{center}\setlength...
...ain stages in building an $n$-gram language
model}
\end{center}\end{center} }$">|; 

$key = q/T_g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img640.png"
 ALT="$ T_g$">|; 

$key = q/8623;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img954.png"
 ALT="$ 8623$">|; 

$key = q/+7060;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img910.png"
 ALT="$ +7060$">|; 

$key = q/t=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img292.png"
 ALT="$ t=1$">|; 

$key = q/G(w);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img532.png"
 ALT="$ G(w)$">|; 

$key = q/displaystyleb^*_{js}({mbox{boldmatho}}^r_t)=prod_{kneqs}b_{jk}({mbox{boldmatho}}^r_{kt});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img319.png"
 ALT="$\displaystyle b^*_{js}({\mbox{\boldmath$o$}}^r_t) = \prod_{k \neq s} b_{jk}({\mbox{\boldmath$o$}}^r_{kt})
$">|; 

$key = q/c_i=lambdaw_i+(1-lambda)bar{w}_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="149" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img453.png"
 ALT="$ c_i = \lambda w_i + (1 - \lambda) \bar{w}_i$">|; 

$key = q/fbox{texttt{Load}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img681.png"
 ALT="\fbox{\texttt{Load}}">|; 

$key = q/displaystyleU^{(q)r}_j(t)=left{{array}{cl}alpha^{(q)r}_1(t)a^{(q)}_{1j}&mbox{ift1}_{i=2}alpha^{(q)r}_i(t-1)a^{(q)}_{ij}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="445" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img330.png"
 ALT="$\displaystyle U^{(q)r}_j(t) = \left\{ \begin{array}{cl}
\alpha^{(q)r}_1(t) a^{(...
...{i=2} \alpha^{(q)r}_i(t-1)
a^{(q)}_{ij} &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/-4089;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img828.png"
 ALT="$ -4089$">|; 

$key = q/j^{th};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img396.png"
 ALT="$ j^{th}$">|; 

$key = q/c_{jm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img227.png"
 ALT="$ c_{jm}$">|; 

$key = q/alpha=1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img168.png"
 ALT="$ \alpha=1.0$">|; 

$key = q/+15051;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img963.png"
 ALT="$ +15051$">|; 

$key = q/{mbox{boldmathSigma}}_{m_r}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img409.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}_{m_r}^{-1}$">|; 

$key = q/0.1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img175.png"
 ALT="$ 0.1$">|; 

$key = q/62.5mus;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img206.png"
 ALT="$ 62.5\mu s$">|; 

$key = q/.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img550.png"
 ALT="$ .$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_j=frac{1}{T}sum_{t=1}^{T}{mbox{boldmatho}}_t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img54.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_j = \frac{1}{T} \sum_{t=1}^{T} {\mbox{\boldmath$o$}}_t$">|; 

$key = q/+7230;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img920.png"
 ALT="$ +7230$">|; 

$key = q/-2330;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img778.png"
 ALT="$ -2330$">|; 

$key = q/+7021;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img899.png"
 ALT="$ +7021$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img195.png"
 ALT="$ B$">|; 

$key = q/(w_1,;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img601.png"
 ALT="$ (w_1,$">|; 

$key = q/+6571;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img896.png"
 ALT="$ +6571$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.model.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="355" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img213.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.model.eps}
\end{center} }$">|; 

$key = q/displaystylem=frac{c_1}{sum_{a=1}^{A}a,.,c_a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img582.png"
 ALT="$\displaystyle m = \frac{c_1}{\sum_{a=1}^{A}a . c_a}$">|; 

$key = q/i-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img151.png"
 ALT="$ i-1$">|; 

$key = q/pm3230;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img817.png"
 ALT="$ \pm 3230$">|; 

$key = q/{mbox{boldmathO}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img4.png"
 ALT="$ {\mbox{\boldmath $O$}}$">|; 

$key = q/displaystyled_t=c_{t+1}-c_t,;;;t<Theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="159" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img181.png"
 ALT="$\displaystyle d_t = c_{t+1} - c_t,\;\;\; t&lt;\Theta$">|; 

$key = q/d_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img563.png"
 ALT="$ d_a$">|; 

$key = q/+7350;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img934.png"
 ALT="$ +7350$">|; 

$key = q/displaystylep(i)=left{{array}{ll}N(i)slashN&mbox{ifN(i)>u}uslashN&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="228" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img648.png"
 ALT="$\displaystyle p(i) = \left\{
\begin{array}{ll}
N(i)/N &amp; \mbox{if $N(i) &gt; u$} \\\\
u/N &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/displaystyle{mbox{boldmathc}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}psi^r_{jsm}(t)}{sum_{r=1}^Rsum_{t=1}^{T_r}sum_{l=1}^{M_s}psi^r_{jsl}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img280.png"
 ALT="$\displaystyle {\mbox{\boldmath$c$}}_{jsm} = \frac{
\sum_{r=1}^R \sum_{t=1}^{T_r...
...r_{jsm}(t)
}{
\sum_{r=1}^R \sum_{t=1}^{T_r} \sum_{l=1}^{M_s} \psi^r_{jsl}(t)
}
$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img16.png"
 ALT="$ j$">|; 

$key = q/L';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img231.png"
 ALT="$ L'$">|; 

$key = q/+15430;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img986.png"
 ALT="$ +15430$">|; 

$key = q/+2226;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img769.png"
 ALT="$ +2226$">|; 

$key = q/U^r_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img329.png"
 ALT="$ U^r_j(t)$">|; 

$key = q/+2120;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img751.png"
 ALT="$ +2120$">|; 

$key = q/-7173;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img918.png"
 ALT="$ -7173$">|; 

$key = q/+5020;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img829.png"
 ALT="$ +5020$">|; 

$key = q/w_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img615.png"
 ALT="$ w_1$">|; 

$key = q/%latex2htmlidmarker48716textstyleparbox{85mm}{center{setlength{epsfxsize}{85mm}e{Fig.thechapter.arabic{figctr}FormofMasterMacroFiles}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="297" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img112.png"
 ALT="% latex2html id marker 48716
$\textstyle \parbox{85mm}{ \begin{center}\setlength...
...ter.\arabic{figctr}  Form of Master Macro Files}
\end{center}\end{center} }$">|; 

$key = q/-17053;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1011.png"
 ALT="$ -17053$">|; 

$key = q/+15341;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img980.png"
 ALT="$ +15341$">|; 

$key = q/+6323;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img874.png"
 ALT="$ +6323$">|; 

$key = q/{a_j^{(i-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img150.png"
 ALT="$ \{a_j^{(i-1)} \}$">|; 

$key = q/displaystylec_n=-a_n-frac{1}{n}sum_{i=1}^{n-1}(n-i)a_ic_{n-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="220" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img160.png"
 ALT="$\displaystyle c_n = -a_n - \frac{1}{n} \sum_{i=1}^{n-1} (n-i) a_i c_{n-i}$">|; 

$key = q/%latex2htmlidmarker49599textstyleparbox{70mm}{center{setlength{epsfxsize}{70mm}etbf{Fig.thechapter.arabic{figctr}SimpleLeft-RightHMM}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="321" HEIGHT="299" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img214.png"
 ALT="% latex2html id marker 49599
$\textstyle \parbox{70mm}{ \begin{center}\setlength...
...echapter.\arabic{figctr}  Simple Left-Right HMM}
\end{center}\end{center} }$">|; 

$key = q/N(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="51" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img461.png"
 ALT="$ N(i,j)$">|; 

$key = q/%latex2htmlidmarker48701textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmctr}center{textbf{Fig.thechapter.arabic{figctr}Step2}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="351" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img107.png"
 ALT="% latex2html id marker 48701
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 2}
\end{center}\end{center} }$">|; 

$key = q/displaystylec_1+c_2*256+c_3*256^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="170" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img617.png"
 ALT="$\displaystyle c_1 + c_2*256 + c_3*256^2
$">|; 

$key = q/displaystyleb_{j}({mbox{boldmatho}}_t)=prod_{s=1}^Sleft[sum_{m=1}^{M_s}c_{jsm}{cmbox{boldmathmu}}_{jsm},{mbox{boldmathSigma}}_{jsm})right]^{gamma_s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="319" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img39.png"
 ALT="$\displaystyle b_{j}({\mbox{\boldmath$o$}}_t) = \prod_{s=1}^S \left[ \sum_{m=1}^...
...ox{\boldmath$\mu$}}_{jsm}, {\mbox{\boldmath$\Sigma$}}_{jsm}) \right]^{\gamma_s}$">|; 

$key = q/fbox{texttt{<--}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img688.png"
 ALT="\fbox{\texttt{&lt;-}}">|; 

$key = q/P({mbox{boldmathO}}|w_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img13.png"
 ALT="$ P({\mbox{\boldmath $O$}}\vert w_i) $">|; 

$key = q/+7172;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img917.png"
 ALT="$ +7172$">|; 

$key = q/%latex2htmlidmarker49224textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmechapter.arabic{figctr}ValidParameterKindConversions}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="269" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img207.png"
 ALT="% latex2html id marker 49224
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...rabic{figctr}  Valid Parameter Kind Conversions}
\end{center}\end{center} }$">|; 

$key = q/c_{t-Theta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img178.png"
 ALT="$ c_{t-\Theta}$">|; 

$key = q/-2637;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img798.png"
 ALT="$ -2637$">|; 

$key = q/c_a=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img573.png"
 ALT="$ c_a = 0$">|; 

$key = q/+5051;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img833.png"
 ALT="$ +5051$">|; 

$key = q/-2428;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img788.png"
 ALT="$ -2428$">|; 

$key = q/displaystylehat{H}=-frac{1}{m}log_2P(w_1,w_2,ldots,w_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img596.png"
 ALT="$\displaystyle \hat{H} = - \frac{1}{m} \log_2 P(w_1, w_2, \ldots, w_m)$">|; 

$key = q/%latex2htmlidmarker51245textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmter.arabic{figctr}MonophoneExpansionofBit-ButNetwork}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="218" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img470.png"
 ALT="% latex2html id marker 51245
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...figctr}  Monophone Expansion of Bit-But Network}
\end{center}\end{center} }$">|; 

$key = q/-2289;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img772.png"
 ALT="$ -2289$">|; 

$key = q/displaystylehat{{mbox{boldmatho}}}={mbox{boldmathW}}{mbox{boldmathzeta}},;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img352.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$o$}}} = {\mbox{\boldmath$W$}}{\mbox{\boldmath$\zeta$}},$">|; 

$key = q/{mbox{boldmathW}}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img397.png"
 ALT="$ {\mbox{\boldmath $W$}}_r$">|; 

$key = q/{mbox{boldmathH}}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img413.png"
 ALT="$ {\mbox{\boldmath $H$}}_r$">|; 

$key = q/%latex2htmlidmarker49535textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}ec{figctr}DatabaseHierarchy:Data[Left];Labels[Right].}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="297" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img211.png"
 ALT="% latex2html id marker 49535
$\textstyle \parbox{80mm}{ \begin{center}\setlength...
... Database Hierarchy: Data [Left]; Labels [Right].}
\end{center}\end{center} }$">|; 

$key = q/+5171;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img839.png"
 ALT="$ +5171$">|; 

$key = q/C(G(w_x),G(w_y));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="124" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img534.png"
 ALT="$ C(G(w_x), G(w_y))$">|; 

$key = q/-6071;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img853.png"
 ALT="$ -6071$">|; 

$key = q/displaystyleP=alpha_N(T)=beta_1(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="142" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img310.png"
 ALT="$\displaystyle P = \alpha_N(T) = \beta_1(1)
$">|; 

$key = q/+??14;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img722.png"
 ALT="$ +??14$">|; 

$key = q/+2530;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img790.png"
 ALT="$ +2530$">|; 

$key = q/(|mathbb{G}|-1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img556.png"
 ALT="$ (\vert\mathbb{G}\vert-1)$">|; 

$key = q/displaystyle{calQ}({calM},{hat{calM}})=K-frac{1}{2}sum_{r=1}^Rsum_{j=1}^d{left({x{boldmathw}}}^T_{rj}-2{{mbox{boldmathw}}}_{rj}{bfk}^{(j)T}_rright)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img394.png"
 ALT="$\displaystyle {\cal Q}({\cal M},{\hat{\cal M}}) = K
- \frac{1}{2}
\sum_{r=1}^R
...
...\boldmath$w$}}}^T_{rj} -2 {{\mbox{\boldmath$w$}}}_{rj}{\bf k}^{(j)T}_r
\right)}$">|; 

$key = q/%latex2htmlidmarker51198textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmhapter.arabic{figctr}OverviewoftheRecognitionProcess}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="587" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img456.png"
 ALT="% latex2html id marker 51198
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...ic{figctr}  Overview of the Recognition Process}
\end{center}\end{center} }$">|; 

$key = q/+2321;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img774.png"
 ALT="$ +2321$">|; 

$key = q/mathcalE(x)=mathcalE(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img500.png"
 ALT="$ \mathcal E(x) = \mathcal E(y)$">|; 

$key = q/{a_{ij}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img33.png"
 ALT="$ \{a_{ij}\}$">|; 

$key = q/%latex2htmlidmarker53351textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmxtbf{Fig.thechapter.arabic{figctr}HSLabdisplaywindow}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="471" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img680.png"
 ALT="% latex2html id marker 53351
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...hechapter.\arabic{figctr}  HSLab display window}
\end{center}\end{center} }$">|; 

$key = q/pm8622;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img953.png"
 ALT="$ \pm 8622$">|; 

$key = q/n>1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img558.png"
 ALT="$ n&gt;1$">|; 

$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img524.png"
 ALT="$ (x, y)$">|; 

$key = q/{mbox{boldmathA}}^{(i)}{mbox{boldmatho}}+{mbox{boldmathb}}^{(i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img366.png"
 ALT="$ {\mbox{\boldmath $A$}}^{(i)}{\mbox{\boldmath $o$}}+{\mbox{\boldmath $b$}}^{(i)}$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img41.png"
 ALT="$ s$">|; 

$key = q/framebox[100mm]{minipage{{100mm}program{mbox{{sim{textsf{o}}>mbox{{<{textsf{VecS00.00.40.6>>0.00.00.00.0mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="599" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img233.png"
 ALT="\framebox[100mm]{
\begin{minipage}{100mm}
\begin{program}
\mbox{$\sim$\textsf{...
... 0.0 0.0 0.0 0.0 \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/fbox{texttt{-->}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img689.png"
 ALT="\fbox{\texttt{-&gt;}}">|; 

$key = q/pm1430;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img738.png"
 ALT="$ \pm 1430$">|; 

$key = q/{mbox{boldmathSigma}}^{-1}=LL';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img230.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}^{-1} = LL'$">|; 

$key = q/+2650;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img803.png"
 ALT="$ +2650$">|; 

$key = q/+2129;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img760.png"
 ALT="$ +2129$">|; 

$key = q/[1..n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img714.png"
 ALT="$ [1..n]$">|; 

$key = q/displaystyle{c^{prime}}_n=left(1+frac{L}{2}sinfrac{pin}{L}right)c_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="175" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img162.png"
 ALT="$\displaystyle {c^{\prime}}_n = \left( 1 + \frac{L}{2} sin \frac{\pi n}{L} \right) c_n$">|; 

$key = q/+6070;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img852.png"
 ALT="$ +6070$">|; 

$key = q/+17050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1007.png"
 ALT="$ +17050$">|; 

$key = q/-16645;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1003.png"
 ALT="$ -16645$">|; 

$key = q/mathcal{M};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img375.png"
 ALT="$ \mathcal{M}$">|; 

$key = q/%latex2htmlidmarker49054textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}etextbf{Fig.thechapter.arabic{figctr}FrequencyWarping}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="340" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img167.png"
 ALT="% latex2html id marker 49054
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
... \thechapter.\arabic{figctr}  Frequency Warping}
\end{center}\end{center} }$">|; 

$key = q/8621;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img952.png"
 ALT="$ 8621$">|; 

$key = q/w_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="28" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img616.png"
 ALT="$ w_N$">|; 

$key = q/8691;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img960.png"
 ALT="$ 8691$">|; 

$key = q/displaystylehat{a}^{(q)}_{1N_q}=frac{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r-1}alpeta^{(q)r}_i(t)+alpha^{(q)r}_{1}(t)a^{(q)}_{1N_q}beta^{(q+1)r}_1(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="439" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img328.png"
 ALT="$\displaystyle \hat{a}^{(q)}_{1N_q} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=...
...i(t)\beta^{(q)r}_i(t)
+ \alpha^{(q)r}_{1}(t)a^{(q)}_{1N_q}\beta^{(q+1)r}_1(t)}
$">|; 

$key = q/%latex2htmlidmarker49887textstyleparbox{70mm}{noindentfbox{parbox{70mm}{hspace{7center{textbf{Fig.thechapter.arabic{figctr}HMMListwithTying}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="342" HEIGHT="242" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img240.png"
 ALT="% latex2html id marker 49887
$\textstyle \parbox{70mm}{\noindent
\fbox{ \parbox...
...bf{ Fig. \thechapter.\arabic{figctr}  HMM List with Tying}
\end{center}
}$">|; 

$key = q/+16650;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1004.png"
 ALT="$ +16650$">|; 

$key = q/displaystyle{{{mbox{boldmathw}}}}_{ri}={bfk}_r^{(i)}{bfG}_r^{(i)-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="121" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img401.png"
 ALT="$\displaystyle {{{\mbox{\boldmath$w$}}}}_{ri} = {\bf k}_r^{(i)}{\bf G}_r^{(i)-1}$">|; 

$key = q/displaystylea_j^{(i)}=a_j^{(i-1)}-k_i^{(i)}a_{i-j}^{(i-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="172" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img156.png"
 ALT="$\displaystyle a_j^{(i)} = a_j^{(i-1)} - k_i^{(i)} a_{i-j}^{(i-1)}$">|; 

$key = q/i=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img159.png"
 ALT="$ i=p$">|; 

$key = q/+15155;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img976.png"
 ALT="$ +15155$">|; 

$key = q/r_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img148.png"
 ALT="$ r_0$">|; 

$key = q/F_{mathrm{M}_mathrm{C}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img552.png"
 ALT="$ F_{\mathrm{M}_\mathrm{C}}$">|; 

$key = q/beta_i(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img285.png"
 ALT="$ \beta_i(t)$">|; 

$key = q/w_{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img511.png"
 ALT="$ w_{i-1}$">|; 

$key = q/backslash;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img655.png"
 ALT="$ \backslash$">|; 

$key = q/-2225;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img768.png"
 ALT="$ -2225$">|; 

$key = q/+2628;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img792.png"
 ALT="$ +2628$">|; 

$key = q/+??06;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img718.png"
 ALT="$ +??06$">|; 

$key = q/+6151;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img855.png"
 ALT="$ +6151$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}={mbox{boldmathW}}{mbox{boldmathxi}},;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="30" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img332.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}} = {\mbox{\boldmath$W$}}{\mbox{\boldmath$\xi$}},$">|; 

$key = q/T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img183.png"
 ALT="$ T$">|; 

$key = q/-3389;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img826.png"
 ALT="$ -3389$">|; 

$key = q/displaystylefrac{C(w_i)}{C(G(w_i))}timesfrac{Cleft(G(w_i),G(w_{i-1})right)}{C(G(w_{i-1}))};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="232" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img529.png"
 ALT="$\displaystyle \frac{C(w_i)}{C(G(w_i))}
\times
\frac{C\left(G(w_i), G(w_{i-1})\right)}
{C(G(w_{i-1}))}$">|; 

$key = q/d_{js}[v];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img244.png"
 ALT="$ d_{js}[v]$">|; 

$key = q/displaystyled(i,j)=frac{1}{S}sum_{s=1}^Sleft[frac{1}{V_s}sum_{k=1}^{V_s}frac{({mboldmathsigma}}_{isk}{mbox{boldmathsigma}}_{jsk}}right]^{frac{1}{2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="289" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img633.png"
 ALT="$\displaystyle d(i,j) = \frac{1}{S} \sum_{s=1}^S \left[ \frac{1}{V_s} \sum_{k=1}...
...boldmath$\sigma$}}_{isk}{\mbox{\boldmath$\sigma$}}_{jsk}} \right]^{\frac{1}{2}}$">|; 

$key = q/G(w_y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img535.png"
 ALT="$ G(w_y)$">|; 

$key = q/+7036;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img907.png"
 ALT="$ +7036$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img547.png"
 ALT="$ h$">|; 

$key = q/+6271;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img870.png"
 ALT="$ +6271$">|; 

$key = q/times8;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img701.png"
 ALT="$ \times 8$">|; 

$key = q/|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img210.png"
 ALT="$ \vert$">|; 

$key = q/{mbox{boldmathSigma}}_{jsm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img267.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}_{jsm}$">|; 

$key = q/displaystyleqquadqquadtimes;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="80" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img516.png"
 ALT="$\displaystyle \qquad\qquad\times$">|; 

$key = q/k=5;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="42" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img577.png"
 ALT="$ k=5$">|; 

$key = q/+15445;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img988.png"
 ALT="$ +15445$">|; 

$key = q/t+deltat;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img65.png"
 ALT="$ t+\delta t$">|; 

$key = q/displaystylea_i^{(i)}=-k_i^{(i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="85" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img157.png"
 ALT="$\displaystyle a_i^{(i)} = - k_i^{(i)}$">|; 

$key = q/-17051;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1008.png"
 ALT="$ -17051$">|; 

$key = q/b>a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img575.png"
 ALT="$ b&gt;a$">|; 

$key = q/+6321;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img872.png"
 ALT="$ +6321$">|; 

$key = q/displaystyle{mbox{boldmathH}}_r=frac{sum_{m_r=1}^{M_r}{mbox{boldmathC}}_{m_r}^Tlbox{boldmathmu}}}_{m_r})^Tright]{mbox{boldmathC}}_{m_r}}{L_{m_r}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="426" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img412.png"
 ALT="$\displaystyle {\mbox{\boldmath$H$}}_r = \frac{ \sum_{m_r=1}^{M_r}{\mbox{\boldma...
...\boldmath$\mu$}}}_{m_r})^T
\right]
{\mbox{\boldmath$C$}}_{m_r}
}
{ L_{m_r}(t)}
$">|; 

$key = q/displaystylealpha^{(q)}_{j}(1)=a^{(q)}_{1j}b^{(q)}_j({mbox{boldmatho}}_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="151" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img294.png"
 ALT="$\displaystyle \alpha^{(q)}_{j}(1) = a^{(q)}_{1j} b^{(q)}_j({\mbox{\boldmath$o$}}_1)
$">|; 

$key = q/+7050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img909.png"
 ALT="$ +7050$">|; 

$key = q/log_2N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img662.png"
 ALT="$ \log_2 N$">|; 

$key = q/displaystyleH_e=frac{sum_kP(S_k)}{sum_k|S_k|};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="119" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img674.png"
 ALT="$\displaystyle H_e = \frac{\sum_k P(S_k)}{\sum_k \vert S_k\vert}$">|; 

$key = q/%latex2htmlidmarker50446textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}echapter.arabic{figctr}FileProcessingintextsc{HERest}}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="438" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img260.png"
 ALT="% latex2html id marker 50446
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...bic{figctr}  File Processing in \textsc{HERest}}
\end{center}\end{center} }$">|; 

$key = q/{calN}(cdot;{mbox{boldmathmu}},{mbox{boldmathSigma}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="75" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img44.png"
 ALT="$ {\cal N}(\cdot; {\mbox{\boldmath $\mu$}}, {\mbox{\boldmath $\Sigma$}})$">|; 

$key = q/displaystyleb_{j}({mbox{boldmatho}}_t)=prod_{s=1}^Sleft[sum_{m=1}^{M_{js}}c_{jsmmbox{boldmathmu}}_{jsm},{mbox{boldmathSigma}}_{jsm})right]^{gamma_s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="322" HEIGHT="74" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img215.png"
 ALT="$\displaystyle b_{j}({\mbox{\boldmath$o$}}_t) = \prod_{s=1}^S \left[ \sum_{m=1}^...
...ox{\boldmath$\mu$}}_{jsm}, {\mbox{\boldmath$\Sigma$}}_{jsm}) \right]^{\gamma_s}$">|; 

$key = q/+3332;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img824.png"
 ALT="$ +3332$">|; 

$key = q/+7170;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img915.png"
 ALT="$ +7170$">|; 

$key = q/+15250;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img977.png"
 ALT="$ +15250$">|; 

$key = q/w_i^{(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img450.png"
 ALT="$ w_i^{(n)}$">|; 

$key = q/fbox{texttt{Mark}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img686.png"
 ALT="\fbox{\texttt{Mark}}">|; 

$key = q/displaystylesum_{x,yinmathbb{W}}C(x,y).logleft(frac{C(x)}{C(G(x))}right);+;sum_{x,yinmathbb{W}}C(x,y).logleft(frac{C(G(x),G(y))}{C(G(y))}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="485" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img540.png"
 ALT="$\displaystyle \sum_{x,y \in \mathbb{W}} C(x,y) . \log
\left(\frac{C(x)}{C(G(x))...
...sum_{x,y \in \mathbb{W}} C(x,y)
. \log\left(\frac{C(G(x),G(y))}{C(G(y))}\right)$">|; 

$key = q/d_a,.,c_a,.,a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="69" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img571.png"
 ALT="$ d_a . c_a . a)$">|; 

$key = q/+7326;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img931.png"
 ALT="$ +7326$">|; 

$key = q/%latex2htmlidmarker48463textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}ebf{Fig.thechapter.arabic{figctr}RepresentingaMixture}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="285" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img50.png"
 ALT="% latex2html id marker 48463
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Representing a Mixture}
\end{center}\end{center} }$">|; 

$key = q/+7220;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img919.png"
 ALT="$ +7220$">|; 

$key = q/displaystylebar{{mbox{boldmathmu}}}_{jm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jm}(t){mbox{boldmatho}}^r_{t}}{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img373.png"
 ALT="$\displaystyle \bar{{\mbox{\boldmath$\mu$}}}_{jm} = \frac{
\sum_{r=1}^R \sum_{t=...
...}(t)
{\mbox{\boldmath$o$}}^r_{t}}{
\sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{jm}(t)
}
$">|; 

$key = q/displaystyleP(w_i;|;G(w_i),G(w_{i-1}),w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img515.png"
 ALT="$\displaystyle P(w_i \;\vert\; G(w_i), G(w_{i-1}), w_{i-1} )$">|; 

$key = q/+15058;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img970.png"
 ALT="$ +15058$">|; 

$key = q/displaystylefrac{1}{P}alpha_j(t)beta_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img83.png"
 ALT="$\displaystyle \frac{1}{P} \alpha_j(t) \beta_j(t)$">|; 

$key = q/+6352;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img880.png"
 ALT="$ +6352$">|; 

$key = q/t_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img670.png"
 ALT="$ t_m$">|; 

$key = q/displaystylelogP_mathrm{class}(mathcal{W});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="93" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img537.png"
 ALT="$\displaystyle \log P_\mathrm{class}(\mathcal{W})$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{js(t){mbox{boldmatho}}^r_{st}}{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jsm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="222" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img322.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{jsm} = \frac{
\sum_{r=1}^R \sum_{t...
...t)
{\mbox{\boldmath$o$}}^r_{st}}{
\sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{jsm}(t)
}
$">|; 

$key = q/displaystyle=;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img80.png"
 ALT="$\displaystyle =$">|; 

$key = q/-log(;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img660.png"
 ALT="$ -\log ($">|; 

$key = q/{bfa}_{ri};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img421.png"
 ALT="$ {\bf a}_{ri}$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img202.png"
 ALT="$ I$">|; 

$key = q/pm8570;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img950.png"
 ALT="$ \pm 8570$">|; 

$key = q/+15420;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img983.png"
 ALT="$ +15420$">|; 

$key = q/{mbox{boldmathSigma}}_{m}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img345.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}_{m}^{-1}$">|; 

$key = q/{mbox{boldmathw}}_{ri};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img435.png"
 ALT="$ {\mbox{\boldmath $w$}}_{ri}$">|; 

$key = q/+2634;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img796.png"
 ALT="$ +2634$">|; 

$key = q/N(i)=sum_{j=1}^LN(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="147" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img642.png"
 ALT="$ N(i) = \sum_{j=1}^L N(i,j)$">|; 

$key = q/+2425;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img786.png"
 ALT="$ +2425$">|; 

$key = q/pm3131;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img813.png"
 ALT="$ \pm 3131$">|; 

$key = q/q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img136.png"
 ALT="$ q$">|; 

$key = q/%latex2htmlidmarker51142textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmhechapter.arabic{figctr}Decisiontree-basedstatetying}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="580" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img445.png"
 ALT="% latex2html id marker 51142
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...arabic{figctr}  Decision tree-based state tying}
\end{center}\end{center} }$">|; 

$key = q/p_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img480.png"
 ALT="$ p_i$">|; 

$key = q/star;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img630.png"
 ALT="$ \star$">|; 

$key = q/+15540;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img996.png"
 ALT="$ +15540$">|; 

$key = q/+7251;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img923.png"
 ALT="$ +7251$">|; 

$key = q/displaystylesum_{g,hinmathbb{G}}C(g,h).logC(g,h);-;2sum_{ginmathbb{G}}C(g).logC(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="333" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img554.png"
 ALT="$\displaystyle \sum_{g,h \in \mathbb{G}} C(g,h) . \log C(g,h)
\;-\; 2 \sum_{g \in \mathbb{G}} C(g) . \log C(g)$">|; 

$key = q/displaystyleA*x_{float}-B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="103" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img193.png"
 ALT="$\displaystyle A*x_{float}-B$">|; 

$key = q/displaystyled_a=left{{array}{c@{quad:quad}l}frac{(a+1)frac{c_{a+1}}{a,.,c_a};-;({c_{k+1}}{c_1}}{1-(k+1)frac{c_{k+1}}{c_1}}&1lealek1&a>k{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="323" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img578.png"
 ALT="$\displaystyle d_a = \left\{ \begin{array}{c@{\quad:\quad}l} \frac{(a+1) \frac{c...
...1}} {1 - (k+1)\frac{c_{k+1}}{c_1}} &amp; 1 \le a \le k 1 &amp; a&gt;k \end{array}\right.$">|; 

$key = q/-1589;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img743.png"
 ALT="$ -1589$">|; 

$key = q/%latex2htmlidmarker48737textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmtr}center{textbf{Fig.thechapter.arabic{figctr}Step11}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="233" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img120.png"
 ALT="% latex2html id marker 48737
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...xtbf{ Fig. \thechapter.\arabic{figctr}  Step 11}
\end{center}\end{center} }$">|; 

$key = q/+6174;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img860.png"
 ALT="$ +6174$">|; 

$key = q/+2127;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img758.png"
 ALT="$ +2127$">|; 

$key = q/+2021;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img745.png"
 ALT="$ +2021$">|; 

$key = q/alpha_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img59.png"
 ALT="$ \alpha_j(t)$">|; 

$key = q/L^r_{jsm}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img321.png"
 ALT="$ L^r_{jsm}(t)$">|; 

$key = q/-7333;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img933.png"
 ALT="$ -7333$">|; 

$key = q/-15460;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img992.png"
 ALT="$ -15460$">|; 

$key = q/1timesn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img424.png"
 ALT="$ 1\times n$">|; 

$key = q/C_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img110.png"
 ALT="$ C_0$">|; 

$key = q/{mbox{boldmatho}}_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img20.png"
 ALT="$ {\mbox{\boldmath $o$}}_1$">|; 

$key = q/+8150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img936.png"
 ALT="$ +8150$">|; 

$key = q/fbox{texttt{Delete}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img706.png"
 ALT="\fbox{\texttt{Delete}}">|; 

$key = q/1-sum_{i=1}^pa_iz^{-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="34" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img138.png"
 ALT="$ 1 - \sum_{i=1}^p a_i z^{-i}$">|; 

$key = q/i^{th};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="18" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img422.png"
 ALT="$ i^{th}$">|; 

$key = q/p(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img643.png"
 ALT="$ p(i,j)$">|; 

$key = q/+15153;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img974.png"
 ALT="$ +15153$">|; 

$key = q/%latex2htmlidmarker48364textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}etbf{Fig.thechapter.arabic{figctr}IsolatedWordProblem}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="258" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img15.png"
 ALT="% latex2html id marker 48364
$\textstyle \parbox{50mm}{ \begin{center}\setlength...
...echapter.\arabic{figctr}  Isolated Word Problem}
\end{center}\end{center} }$">|; 

$key = q/1<tleqT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img282.png"
 ALT="$ 1&lt;t \leq T$">|; 

$key = q/c_b>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img574.png"
 ALT="$ c_b &gt; 0$">|; 

$key = q/w_2,;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="27" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img602.png"
 ALT="$ w_2,$">|; 

$key = q/%latex2htmlidmarker48725textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}eFig.thechapter.arabic{figctr}TyingTransitionMatrices}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="424" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img117.png"
 ALT="% latex2html id marker 48725
$\textstyle \parbox{80mm}{ \begin{center}\setlength...
...pter.\arabic{figctr}  Tying Transition Matrices}
\end{center}\end{center} }$">|; 

$key = q/{mbox{boldmathB}}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img344.png"
 ALT="$ {\mbox{\boldmath $B$}}_m$">|; 

$key = q/%latex2htmlidmarker48424textstyleparbox{84mm}{center{setlength{epsfxsize}{84mm}eer.arabic{figctr}UsingHMMsforIsolatedWordRecognition}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="573" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img35.png"
 ALT="% latex2html id marker 48424
$\textstyle \parbox{84mm}{ \begin{center}\setlength...
...gctr}  Using HMMs for Isolated Word Recognition}
\end{center}\end{center} }$">|; 

$key = q/displaystyle;+;sum_{g,hinmathbb{G}}C(g,h).logC(g,h);-;sum_{ginmathbb{G}}C(g).logC(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="346" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img543.png"
 ALT="$\displaystyle \;+\; \sum_{g,h \in \mathbb{G}} C(g,h) . \log C(g,h)
\;-\; \sum_{g \in \mathbb{G}} C(g) . \log C(g)$">|; 

$key = q/(m-1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="58" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img626.png"
 ALT="$ (m-1)$">|; 

$key = q/{mbox{boldmathmu}}_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img52.png"
 ALT="$ {\mbox{\boldmath $\mu$}}_j$">|; 

$key = q/>;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img226.png"
 ALT="$ &gt;$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img404.png"
 ALT="$ R$">|; 

$key = q/-3420;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img827.png"
 ALT="$ -3420$">|; 

$key = q/{mbox{boldmathO}}_T=left{{mbox{boldmatho}}(1),dots,{mbox{boldmatho}}(T)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="163" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img392.png"
 ALT="$ {\mbox{\boldmath $O$}}_T = \left\{{\mbox{\boldmath $o$}}(1),\dots,{\mbox{\boldmath $o$}}(T)\right\}$">|; 

$key = q/+5072;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img836.png"
 ALT="$ +5072$">|; 

$key = q/%latex2htmlidmarker51312textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}e{textbf{Fig.thechapter.arabic{figctr}ForcedAlignment}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="240" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img485.png"
 ALT="% latex2html id marker 51312
$\textstyle \parbox{55mm}{ \begin{center}\setlength...
.... \thechapter.\arabic{figctr}  Forced Alignment}
\end{center}\end{center} }$">|; 

$key = q/P(G(w_i);|;G(w_{i-1}),w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img519.png"
 ALT="$ P(G(w_i) \;\vert\; G(w_{i-1}), w_{i-1})$">|; 

$key = q/displaystyleL^r_{jsm}(t)=L^r_{j}(t)=frac{1}{P_r}alpha_j(t)beta_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="225" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img320.png"
 ALT="$\displaystyle L^r_{jsm}(t) = L^r_{j}(t) = \frac{1}{P_r} \alpha_j(t) \beta_j(t)
$">|; 

$key = q/displaystyle{mbox{boldmathc}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jsm}(t)}{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{j}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="198" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img324.png"
 ALT="$\displaystyle {\mbox{\boldmath$c$}}_{jsm} = \frac{
\sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{jsm}(t)
}{
\sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{j}(t)
}
$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img645.png"
 ALT="$ f$">|; 

$key = q/displaystylepsi_j(t)=max_ileft{psi_i(t-1)+log(a_{ij})right}+log(b_j({mbox{boldmatho}}_t)).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="340" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img92.png"
 ALT="$\displaystyle \psi_j(t) = \max_i \left\{ \psi_i(t-1) + log(a_{ij}) \right\} + log(b_j({\mbox{\boldmath$o$}}_t)).$">|; 

$key = q/+6375;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img886.png"
 ALT="$ +6375$">|; 

$key = q/+2328;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img777.png"
 ALT="$ +2328$">|; 

$key = q/{mbox{boldmathxi}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img382.png"
 ALT="$ {\mbox{\boldmath $\xi$}}_{m_r}$">|; 

$key = q/%latex2htmlidmarker51246textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmfigctr}WordInternalTriphoneExpansionofBit-ButNetwork}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="234" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img471.png"
 ALT="% latex2html id marker 51246
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...rd Internal Triphone Expansion of Bit-But Network}
\end{center}\end{center} }$">|; 

$key = q/+2640;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img801.png"
 ALT="$ +2640$">|; 

$key = q/N_{jm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img374.png"
 ALT="$ N_{jm}$">|; 

$key = q/displaystyle=frac{1}{10T}(p_1+p_2+ldots+p_N+ap_{N+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="256" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img483.png"
 ALT="$\displaystyle = \frac{1}{10T}(p_1 + p_2 + \ldots + p_N + a p_{N+1})$">|; 

$key = q/+2222;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img766.png"
 ALT="$ +2222$">|; 

$key = q/mathcal{E};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img504.png"
 ALT="$ \mathcal{E}$">|; 

$key = q/+8231;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img941.png"
 ALT="$ +8231$">|; 

$key = q/fbox{texttt{Save}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img682.png"
 ALT="\fbox{\texttt{Save}}">|; 

$key = q/-16635;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1001.png"
 ALT="$ -16635$">|; 

$key = q/displaystylealpha_1(1)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img66.png"
 ALT="$\displaystyle \alpha_1(1) = 1$">|; 

$key = q/-7325;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img930.png"
 ALT="$ -7325$">|; 

$key = q/alpha<1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img166.png"
 ALT="$ \alpha &lt; 1.0$">|; 

$key = q/%latex2htmlidmarker51126textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}ebf{Fig.thechapter.arabic{figctr}ItemListConstruction}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="337" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img443.png"
 ALT="% latex2html id marker 51126
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Item List Construction}
\end{center}\end{center} }$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}_j=frac{sum_{t=1}^{T}L_j(t)({mbox{boldmath_j)({mbox{boldmatho}}_t-{mbox{boldmathmu}}_j)'}{sum_{t=1}^{T}L_j(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="260" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img58.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}}_j = \frac{ \sum_{t=1}^{T} L_j(t)...
...{\mbox{\boldmath$o$}}_t - {\mbox{\boldmath$\mu$}}_j)' } {\sum_{t=1}^{T} L_j(t)}$">|; 

$key = q/1<j<N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img68.png"
 ALT="$ 1&lt;j&lt;N$">|; 

$key = q/p(c_i,|c_{i-n+1},ldots,c_{n-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="161" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img606.png"
 ALT="$ p(c_i, \vert c_{i-n+1},
\ldots, c_{n-1})$">|; 

$key = q/mathbb{W};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img496.png"
 ALT="$ \mathbb{W}$">|; 

$key = q/{mbox{boldmatho}}_{st};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img219.png"
 ALT="$ {\mbox{\boldmath $o$}}_{st}$">|; 

$key = q/-6351;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img879.png"
 ALT="$ -6351$">|; 

$key = q/%latex2htmlidmarker48713textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmctr}center{textbf{Fig.thechapter.arabic{figctr}Step5}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="308" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img111.png"
 ALT="% latex2html id marker 48713
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 5}
\end{center}\end{center} }$">|; 

$key = q/pm1451;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img741.png"
 ALT="$ \pm 1451$">|; 

$key = q/+6230;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img863.png"
 ALT="$ +6230$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img9.png"
 ALT="$ w_i$">|; 

$key = q/+6021;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img851.png"
 ALT="$ +6021$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.decode.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="302" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img473.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.decode.eps}
\end{center} }$">|; 

$key = q/displaystyle{s^{prime}}_n=s_n+qRND();MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="142" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img133.png"
 ALT="$\displaystyle {s^{\prime}}_n = s_n + q RND()$">|; 

$key = q/P(w_i;|;G(w_i),G(w_{i-1}),w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="205" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img518.png"
 ALT="$ P(w_i
\;\vert\; G(w_i), G(w_{i-1}), w_{i-1})$">|; 

$key = q/o_{st};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="23" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img37.png"
 ALT="$ o_{st}$">|; 

$key = q/-2424;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img785.png"
 ALT="$ -2424$">|; 

$key = q/2^{24}-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img611.png"
 ALT="$ 2^{24} -
1$">|; 

$key = q/+15056;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img968.png"
 ALT="$ +15056$">|; 

$key = q/bar{{mbox{boldmathmu}}}_{jm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img372.png"
 ALT="$ \bar{{\mbox{\boldmath $\mu$}}}_{jm}$">|; 

$key = q/+6350;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img878.png"
 ALT="$ +6350$">|; 

$key = q/times;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img124.png"
 ALT="$ \times$">|; 

$key = q/|mathbb{W}|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img557.png"
 ALT="$ \vert\mathbb{W}\vert$">|; 

$key = q/ginmathbb{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img506.png"
 ALT="$ g \in \mathbb{G}$">|; 

$key = q/+8521;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img948.png"
 ALT="$ +8521$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{jsm}=frac{sum_{i=1}^Isum_{r=1}^{R^i}sum_{t=thb}}^{(i)})}{sum_{i=1}^Isum_{r=1}^{R^i}sum_{t=1}^{T_r}L^r_{jsm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="347" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img367.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{jsm} = \frac{
\sum_{i=1}^I\sum_{r=...
...ath$b$}}^{(i)})}{
\sum_{i=1}^I\sum_{r=1}^{R^i} \sum_{t=1}^{T_r} L^r_{jsm}(t)
}
$">|; 

$key = q/textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}epsfbox{HTKFigsslashslashTool.spio.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="277" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img125.png"
 ALT="$\textstyle \parbox{60mm}{ \begin{center}\setlength{\epsfxsize}{60mm}
\epsfbox{HTKFigs//Tool.spio.eps}
\end{center} }$">|; 

$key = q/L_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img56.png"
 ALT="$ L_j(t)$">|; 

$key = q/G(w_x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img536.png"
 ALT="$ G(w_x)$">|; 

$key = q/{{mbox{boldmathA}}}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img429.png"
 ALT="$ {{\mbox{\boldmath $A$}}}_r$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img671.png"
 ALT="$ G$">|; 

$key = q/1lealek;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img580.png"
 ALT="$ 1 \le a \le k$">|; 

$key = q/fbox{texttt{Z.Out}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img691.png"
 ALT="\fbox{\texttt{Z.Out}}">|; 

$key = q/+2632;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img795.png"
 ALT="$ +2632$">|; 

$key = q/+16930;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1006.png"
 ALT="$ +16930$">|; 

$key = q/+2423;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img784.png"
 ALT="$ +2423$">|; 

$key = q/displaystylePP=hat{P}(w_1,w_2,ldots,w_m)^{-frac{1}{m}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="202" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img599.png"
 ALT="$\displaystyle PP = \hat{P}(w_1, w_2, \ldots, w_m)^{-\frac{1}{m}}$">|; 

$key = q/+??10;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img719.png"
 ALT="$ +??10$">|; 

$key = q/pm6553;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img893.png"
 ALT="$ \pm 6553$">|; 

$key = q/displaystylealpha^{(q)}_{N_q}(1)=sum_{i=2}^{N_q-1}alpha^{(q)}_{i}(1)a^{(q)}_{iN_q};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="186" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img295.png"
 ALT="$\displaystyle \alpha^{(q)}_{N_q}(1) =
\sum_{i=2}^{N_q-1} \alpha^{(q)}_{i}(1) a^{(q)}_{iN_q}
$">|; 

$key = q/+6520;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img888.png"
 ALT="$ +6520$">|; 

$key = q/psi^r_{jsm}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img276.png"
 ALT="$ \psi^r_{jsm}(t)$">|; 

$key = q/pm7332;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img932.png"
 ALT="$ \pm 7332$">|; 

$key = q/0.8-1.2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img169.png"
 ALT="$ 0.8 - 1.2$">|; 

$key = q/hatP(w_1,w_2,ldots,w_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="132" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img487.png"
 ALT="$ \hat
P(w_1, w_2, \ldots, w_m)$">|; 

$key = q/+6172;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img858.png"
 ALT="$ +6172$">|; 

$key = q/+2125;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img756.png"
 ALT="$ +2125$">|; 

$key = q/fbox{texttt{About}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img683.png"
 ALT="\fbox{\texttt{About}}">|; 

$key = q/fbox{texttt{Labelas}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img705.png"
 ALT="\fbox{\texttt{Labelas}}">|; 

$key = q/w_1,w_2,ldots,w_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img622.png"
 ALT="$ w_1, w_2, \ldots,
w_n$">|; 

$key = q/k.c_k>c_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="67" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img579.png"
 ALT="$ k.c_k &gt; c_1$">|; 

$key = q/displaystyleP(G(w_i);|;G(w_{i-1}),w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img517.png"
 ALT="$\displaystyle P(G(w_i) \;\vert\; G(w_{i-1}), w_{i-1})$">|; 

$key = q/+6328;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img877.png"
 ALT="$ +6328$">|; 

$key = q/psi_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img94.png"
 ALT="$ \psi_j(t)$">|; 

$key = q/-1289;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img734.png"
 ALT="$ -1289$">|; 

$key = q/t-Deltat;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img289.png"
 ALT="$ t-\Delta t$">|; 

$key = q/displaystyle{mbox{boldmathzeta}}=left[mbox{}wmbox{}o_1mbox{}o_2mbox{}dotsmbox{}o_nmbox{}right]^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img354.png"
 ALT="$\displaystyle {\mbox{\boldmath$\zeta$}} = \left[\mbox{ }w\mbox{ }o_1\mbox{ }o_2\mbox{ }\dots\mbox{ }o_n\mbox{ }\right]^T
$">|; 

$key = q/ntimesn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img340.png"
 ALT="$ n \times n$">|; 

$key = q/{bfG}^{(i)}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img427.png"
 ALT="$ {\bf G}^{(i)}_r$">|; 

$key = q/+3233;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img820.png"
 ALT="$ +3233$">|; 

$key = q/hat{beta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img585.png"
 ALT="$ \hat{\beta}$">|; 

$key = q/+7071;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img912.png"
 ALT="$ +7071$">|; 

$key = q/+6551;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img891.png"
 ALT="$ +6551$">|; 

$key = q/+15151;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img972.png"
 ALT="$ +15151$">|; 

$key = q/displaystyle{bfA}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img419.png"
 ALT="$\displaystyle {\bf A}_r$">|; 

$key = q/phi_j(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img86.png"
 ALT="$ \phi_j(t)$">|; 

$key = q/fbox{texttt{Adjust}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img709.png"
 ALT="\fbox{\texttt{Adjust}}">|; 

$key = q/i=1ldotsN+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img481.png"
 ALT="$ i = 1 \ldots N+1$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_j=frac{sum_{t=1}^{T}L_j(t){mbox{boldmatho}}_t}{sum_{t=1}^{T}L_j(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="139" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img57.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_j = \frac{ \sum_{t=1}^{T} L_j(t) {\mbox{\boldmath$o$}}_t} {\sum_{t=1}^{T} L_j(t)}$">|; 

$key = q/+2050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img749.png"
 ALT="$ +2050$">|; 

$key = q/-1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img189.png"
 ALT="$ -1.0$">|; 

$key = q/displaystylealpha_j(t)=left[sum_{i=2}^{N-1}alpha_i(t-1)a_{ij}right]b_j({mbox{boldmatho}}_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="233" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img283.png"
 ALT="$\displaystyle \alpha_j(t) = \left[ \sum_{i=2}^{N-1} \alpha_i(t-1) a_{ij} \right]
b_j({\mbox{\boldmath$o$}}_t)
$">|; 

$key = q/+6253;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img867.png"
 ALT="$ +6253$">|; 

$key = q/b_{j}({mbox{boldmatho}}_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img38.png"
 ALT="$ b_{j}({\mbox{\boldmath $o$}}_t)$">|; 

$key = q/<;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img225.png"
 ALT="$ &lt;$">|; 

$key = q/{figure}preform{<verbatim_mark>verbatim560#preform{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="309" HEIGHT="89" BORDER="0"
 SRC="|."$dir".q|img355.png"
 ALT="\begin{figure}\begin{verbatim}&nbsp;b \lq\lq global''
&lt;MMFIDMASK&gt; CUED_WSJ*
&lt;PARAMET...
...SE
&lt;NUMCLASSES&gt; 1
&lt;CLASS&gt; 1 {*.state[2-4].mix[1-12]}\end{verbatim}\end{figure}">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img268.png"
 ALT="$ \lambda$">|; 

$key = q/displaystyle(x_{max}+x_{min})*Islash(x_{max}-x_{min});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="237" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img199.png"
 ALT="$\displaystyle (x_{max}+x_{min})*I/(x_{max}-x_{min})$">|; 

$key = q/displaystyle=frac{N-D-S-I}{N}times100%;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="187" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img479.png"
 ALT="$\displaystyle = \frac{N-D-S-I}{N} \times 100\%$">|; 

$key = q/displaystyleN_{jm}=sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jm}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="154" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img370.png"
 ALT="$\displaystyle N_{jm} =
\sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{jm}(t)
$">|; 

$key = q/+2170;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img762.png"
 ALT="$ +2170$">|; 

$key = q/1leqtleqT;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img265.png"
 ALT="$ 1 \leq t \leq T $">|; 

$key = q/+1232;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img733.png"
 ALT="$ +1232$">|; 

$key = q/+5070;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img834.png"
 ALT="$ +5070$">|; 

$key = q/M_s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img40.png"
 ALT="$ M_s$">|; 

$key = q/displaystyle{mbox{boldmathSigma}}_{m_r}^{-1}={mbox{boldmathC}}_{m_r}{mbox{boldmathC}}_{m_r}^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="122" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img410.png"
 ALT="$\displaystyle {\mbox{\boldmath$\Sigma$}}_{m_r}^{-1} = {\mbox{\boldmath$C$}}_{m_r}{\mbox{\boldmath$C$}}_{m_r}^T
$">|; 

$key = q/+??19;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img725.png"
 ALT="$ +??19$">|; 

$key = q/d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img377.png"
 ALT="$ d$">|; 

$key = q/psi_i(t-1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="65" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img93.png"
 ALT="$ \psi_i(t-1)$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img281.png"
 ALT="$ \psi$">|; 

$key = q/+6373;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img884.png"
 ALT="$ +6373$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img191.png"
 ALT="$ x$">|; 

$key = q/+2220;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img764.png"
 ALT="$ +2220$">|; 

$key = q/times4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img700.png"
 ALT="$ \times 4$">|; 

$key = q/c_{t+Theta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img179.png"
 ALT="$ c_{t+\Theta}$">|; 

$key = q/displaystylealpha^{(q)}_j(t)=left[alpha^{(q)}_1(t)a^{(q)}_{1j}+sum_{i=2}^{N_q-1}alpha^{(q)}_{i}(t-1)a^{(q)}_{ij}right]b^{(q)}_j({mbox{boldmatho}}_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="369" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img298.png"
 ALT="$\displaystyle \alpha^{(q)}_j(t) =
\left[
\alpha^{(q)}_1(t) a^{(q)}_{1j} +
\s...
... \alpha^{(q)}_{i}(t-1) a^{(q)}_{ij}
\right]
b^{(q)}_j({\mbox{\boldmath$o$}}_t)
$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}={mbox{boldmathH}}{mbox{boldmathSigma}}{mbox{boldmathH}},;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img348.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}} = {\mbox{\boldmath$H$}}{\mbox{\boldmath$\Sigma$}}{\mbox{\boldmath$H$}},$">|; 

$key = q/displaystyle{calQ}({calM},{hat{calM}})=-frac{1}{2}sum_{r=1}^Rsum_{m_r=1}^{M_r}su}^{-1}({{mbox{boldmatho}}}(t)-{hat{{mbox{boldmathmu}}}}_{m_r})right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="639" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img388.png"
 ALT="$\displaystyle {\cal Q}({\cal M},{\hat{\cal M}}) =
- \frac{1}{2}
\sum_{r=1}^R
\s...
...^{-1}({{\mbox{\boldmath$o$}}}(t)-{\hat{{\mbox{\boldmath$\mu$}}}}_{m_r})
\right]$">|; 

$key = q/65,000,^4simeq1.8times10^{19};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="151" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img497.png"
 ALT="$ 65,000 ^4
\simeq 1.8\times10^{19}$">|; 

$key = q/fbox{texttt{Edit}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img707.png"
 ALT="\fbox{\texttt{Edit}}">|; 

$key = q/fbox{texttt{Set[?]}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="40" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img710.png"
 ALT="\fbox{\texttt{Set [?]}}">|; 

$key = q/left{m_1,m_2,dots,m_{M_r}right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="138" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img387.png"
 ALT="$ \left\{m_1, m_2, \dots, m_{M_r}\right\}$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}psi^box{boldmathmu}}}_{jsm})'}{sum_{r=1}^Rsum_{t=1}^{T_r}psi^r_{jsm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="383" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img279.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}}_{jsm} = \frac{
\sum_{r=1}^R \sum...
...x{\boldmath$\mu$}}}_{jsm})'
}{
\sum_{r=1}^R \sum_{t=1}^{T_r} \psi^r_{jsm}(t)
}
$">|; 

$key = q/displaystyle{mbox{boldmathSigma}}_{m}^{-1}={mbox{boldmathC}}_m{mbox{boldmathC}}_m^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="108" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img346.png"
 ALT="$\displaystyle {\mbox{\boldmath$\Sigma$}}_{m}^{-1} = {\mbox{\boldmath$C$}}_m{\mbox{\boldmath$C$}}_m^T
$">|; 

$key = q/-15450;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img989.png"
 ALT="$ -15450$">|; 

$key = q/+2028;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img746.png"
 ALT="$ +2028$">|; 

$key = q/c(w_1,ldots,w_{i})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="123" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img590.png"
 ALT="$ c(w_1,\ldots,w_{i})=0$">|; 

$key = q/C(.);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img494.png"
 ALT="$ C(.)$">|; 

$key = q/sum_{j=1}^Lp(i,j)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img646.png"
 ALT="$ \sum_{j=1}^L p(i,j) = 1$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{jm}=frac{N_{jm}}{N_{jm}+tau}bar{{mbox{boldmathmu}}}_{jm}+frac{tau}{N_{jm}+tau}{mbox{boldmathmu}}_{jm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img368.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{jm} = \frac{ N_{jm} } { N_{jm} + \...
...ath$\mu$}}}_{jm} + \frac{ \tau } { N_{jm} + \tau } {\mbox{\boldmath$\mu$}}_{jm}$">|; 

$key = q/m_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img381.png"
 ALT="$ m_r$">|; 

$key = q/%latex2htmlidmarker50465textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}ehapter.arabic{figctr}textsc{HERest}ParallelOperation}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="386" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img262.png"
 ALT="% latex2html id marker 50465
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...bic{figctr}  \textsc{HERest} Parallel Operation}
\end{center}\end{center} }$">|; 

$key = q/a>k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img576.png"
 ALT="$ a &gt; k$">|; 

$key = q/x(T+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="64" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img29.png"
 ALT="$ x(T+1)$">|; 

$key = q/sum_ka_{ik}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="81" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img631.png"
 ALT="$ \sum_k a_{ik} = 1 $">|; 

$key = q/displaystyle{mbox{boldmathO}}={mbox{boldmatho}}_1,{mbox{boldmatho}}_2,ldots,{mbox{boldmatho}}_T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="134" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img5.png"
 ALT="$\displaystyle {\mbox{\boldmath$O$}} = {\mbox{\boldmath$o$}}_1, {\mbox{\boldmath$o$}}_2, \ldots, {\mbox{\boldmath$o$}}_T$">|; 

$key = q/X=x(1),x(2),x(3),ldots,x(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="210" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img26.png"
 ALT="$ X = x(1), x(2), x(3), \ldots, x(T)$">|; 

$key = q/pm1031;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img728.png"
 ALT="$ \pm 1031$">|; 

$key = q/displaystyleP(w_i;|;w_1,w_2,ldots,w_{i-1})=P(w_i;|;mathcalE(w_1,w_2,ldots,w_{i-1}));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="382" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img503.png"
 ALT="$\displaystyle P(w_i \;\vert\; w_1, w_2, \ldots, w_{i-1}) = P(w_i \;\vert\; \mathcal E(w_1, w_2, \ldots, w_{i-1}))$">|; 

$key = q/displaystyler_i=sum_{j=1}^{N-i}s_js_{j+i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="109" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img144.png"
 ALT="$\displaystyle r_i = \sum_{j=1}^{N-i} s_j s_{j+i}$">|; 

$key = q/Theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img180.png"
 ALT="$ \Theta$">|; 

$key = q/displaystylebeta_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img417.png"
 ALT="$\displaystyle \beta_r$">|; 

$key = q/N(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img462.png"
 ALT="$ N(i)$">|; 

$key = q/framebox[85mm]{minipage{{85mm}program{parmbox{{sim{textsf{h``htm''}}mbox{{<{texttextsf{TransP}{>{}4>>...mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="217" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img246.png"
 ALT="\framebox[85mm]{
\begin{minipage}{85mm}
\begin{program}
\par
\mbox{$\sim$\text...
...4 \\\\
\&gt; \&gt; ... \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.train.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="321" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img253.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.train.eps}
\end{center} }$">|; 

$key = q/+7322;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img927.png"
 ALT="$ +7322$">|; 

$key = q/-2631;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img794.png"
 ALT="$ -2631$">|; 

$key = q/-2389;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img780.png"
 ALT="$ -2389$">|; 

$key = q/-5322;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img849.png"
 ALT="$ -5322$">|; 

$key = q/displaystyle{bfk}^{(i)}=sum_{c=1}^{C}{bfk}^{(i)}_{R_c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="102" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img406.png"
 ALT="$\displaystyle {\bf k}^{(i)} = \sum_{c=1}^{C} {\bf k}^{(i)}_{R_c}
$">|; 

$key = q/%latex2htmlidmarker48951textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}ef{Fig.thechapter.arabic{figctr}SpeechEncodingProcess}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="384" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img127.png"
 ALT="% latex2html id marker 48951
$\textstyle \parbox{50mm}{ \begin{center}\setlength...
...hapter.\arabic{figctr}  Speech Encoding Process}
\end{center}\end{center} }$">|; 

$key = q/displaystyleb_j({mbox{boldmatho}}_t)=frac{1}{sqrt{(2pi)^n|{mbox{boldmathSigma_j}ox{boldmathSigma}}_j^{-1}({mbox{boldmatho}}_t-{mbox{boldmathmu}}_j)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="317" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img51.png"
 ALT="$\displaystyle b_j({\mbox{\boldmath$o$}}_t) = \frac{1}{\sqrt{(2 \pi)^n \vert {\m...
...boldmath$\Sigma$}}_j^{-1}({\mbox{\boldmath$o$}}_t - {\mbox{\boldmath$\mu$}}_j)}$">|; 

$key = q/+15054;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img966.png"
 ALT="$ +15054$">|; 

$key = q/i=0,p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="54" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img145.png"
 ALT="$ i = 0,p$">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img288.png"
 ALT="$ \beta$">|; 

$key = q/P=;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img308.png"
 ALT="$ P =$">|; 

$key = q/%latex2htmlidmarker51162textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}eter{textbf{Fig.thechapter.arabic{figctr}VQProcessing}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="428" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img449.png"
 ALT="% latex2html id marker 51162
$\textstyle \parbox{55mm}{ \begin{center}\setlength...
...Fig. \thechapter.\arabic{figctr}  VQ Processing}
\end{center}\end{center} }$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="13" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img63.png"
 ALT="$ 1$">|; 

$key = q/+5271;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img846.png"
 ALT="$ +5271$">|; 

$key = q/displaystyled(i,j)=frac{1}{S}sum_{s=1}^Sleft[frac{1}{M_s}sum_{m=1}^{M_s}(c_{ism}-c_{jsm})^2right]^{frac{1}{2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="293" HEIGHT="77" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img635.png"
 ALT="$\displaystyle d(i,j) = \frac{1}{S} \sum_{s=1}^S \left[ \frac{1}{M_s} \sum_{m=1}^{M_s} (c_{ism} - c_{jsm})^2 \right]^{\frac{1}{2}}$">|; 

$key = q/pm2663;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img809.png"
 ALT="$ \pm 2663$">|; 

$key = q/{mbox{boldmatho}}_t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img6.png"
 ALT="$ {\mbox{\boldmath $o$}}_t$">|; 

$key = q/textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}epsfbox{HTKFigsslashslashTool.hedit.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="254" HEIGHT="424" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img442.png"
 ALT="$\textstyle \parbox{55mm}{ \begin{center}\setlength{\epsfxsize}{55mm}
\epsfbox{HTKFigs//Tool.hedit.eps}
\end{center} }$">|; 

$key = q/+7024;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img901.png"
 ALT="$ +7024$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img147.png"
 ALT="$ E$">|; 

$key = q/%latex2htmlidmarker51252textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}eig.thechapter.arabic{figctr}RecognitionNetworkLevels}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="360" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img474.png"
 ALT="% latex2html id marker 51252
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...ter.\arabic{figctr}  Recognition Network Levels}
\end{center}\end{center} }$">|; 

$key = q/displaystylesum_{x,yinmathbb{W}}C(x,y).logleft(frac{C(x)}{C(G(x))}timesfrac{C(G(x),G(y))}{C(G(y))}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="333" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img539.png"
 ALT="$\displaystyle \sum_{x,y \in \mathbb{W}} C(x,y) . \log\left(
\frac{C(x)}{C(G(x))} \times \frac{C(G(x),G(y))}{C(G(y))}
\right)$">|; 

$key = q/+2421;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img782.png"
 ALT="$ +2421$">|; 

$key = q/+5321;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img848.png"
 ALT="$ +5321$">|; 

$key = q/%latex2htmlidmarker48634textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmfigctr}TheViterbiAlgorithmforIsolatedWordRecognition}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="410" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img95.png"
 ALT="% latex2html id marker 48634
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...e Viterbi Algorithm for Isolated
Word Recognition}
\end{center}\end{center} }$">|; 

$key = q/displaystyleF_{mathrm{M}_mathrm{C}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img553.png"
 ALT="$\displaystyle F_{\mathrm{M}_\mathrm{C}}$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img43.png"
 ALT="$ m$">|; 

$key = q/{bfW}_3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img360.png"
 ALT="$ {\bf W}_3$">|; 

$key = q/%latex2htmlidmarker49121textstyleparbox{130mm}{center{setlength{epsfxsize}{130mm.arabic{figctr}ParameterVectorLayoutinHTKFormatFiles}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="591" HEIGHT="313" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img185.png"
 ALT="% latex2html id marker 49121
$\textstyle \parbox{130mm}{ \begin{center}\setlengt...
...}  Parameter Vector Layout in HTK Format Files}
\end{center}\end{center} }$">|; 

$key = q/({mbox{boldmathO}}|lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img309.png"
 ALT="$ ({\mbox{\boldmath $O$}} \vert \lambda)$">|; 

$key = q/p+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="39" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img143.png"
 ALT="$ p+1$">|; 

$key = q/>>;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img657.png"
 ALT="$ &#187;$">|; 

$key = q/{figure}preform{<verbatim_mark>verbatim562#preform{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="266" HEIGHT="164" BORDER="0"
 SRC="|."$dir".q|img363.png"
 ALT="\begin{figure}\begin{verbatim}&nbsp;r ''regtree_4.tree''
&lt;BASECLASS&gt;&nbsp;b ''baseclas...
...DE&gt; 4 1 1
&lt;TNODE&gt; 5 1 2
&lt;TNODE&gt; 6 1 3
&lt;TNODE&gt; 7 1 4\end{verbatim}\end{figure}">|; 

$key = q/C(G(w))=sum_{x:G(x)=G(w)}C(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="218" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img533.png"
 ALT="$ C(G(w))=\sum_{x:
G(x)=G(w)}C(x)$">|; 

$key = q/+6170;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img856.png"
 ALT="$ +6170$">|; 

$key = q/+16630;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1000.png"
 ALT="$ +16630$">|; 

$key = q/+2123;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img754.png"
 ALT="$ +2123$">|; 

$key = q/p(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img647.png"
 ALT="$ p(i)$">|; 

$key = q/displaystylehat{{mbox{boldmatho}}}_r(t)={mbox{boldmathA}}_r{mbox{boldmatho}}(t)+{mbox{boldmathb}}_r={mbox{boldmathW}}_r{mbox{boldmathzeta}}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="218" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img431.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath $o$}}}_r(t) = {\mbox{\boldmath $A$}}_r{\mbo...
...\mbox{\boldmath $b$}}_r = {\mbox{\boldmath $W$}}_r{\mbox{\boldmath $\zeta$}}(t)$">|; 

$key = q/fbox{texttt{Label}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img704.png"
 ALT="\fbox{\texttt{Label}}">|; 

$key = q/displaystylebeta_j(t)=P({mbox{boldmatho}}_{t+1},ldots,{mbox{boldmatho}}_T|x(t)=j,M).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="258" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img73.png"
 ALT="$\displaystyle \beta_j(t) = P({\mbox{\boldmath$o$}}_{t+1},\ldots,{\mbox{\boldmath$o$}}_T \vert x(t)=j , M).$">|; 

$key = q/framebox[70mm]{minipage{{70mm}program{mbox{{sim{textsf{j``lintran.mat''}}mbox{{<f{Xform}{>{}25>>>1.00.10.20.10.4>>>0.21.00.10.10.1program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="321" HEIGHT="236" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img247.png"
 ALT="\framebox[70mm]{
\begin{minipage}{70mm}
\begin{program}
\mbox{$\sim$\textsf{j ...
...0 0.1 0.2 0.1 0.4\\\\
\&gt; \&gt; \&gt; 0.2 1.0 0.1 0.1 0.1
\end{program} \end{minipage} }">|; 

$key = q/{mbox{boldmathW}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img333.png"
 ALT="$ {\mbox{\boldmath $W$}}$">|; 

$key = q/%latex2htmlidmarker48680textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}ebf{Fig.thechapter.arabic{figctr}TrainingSub-wordHMMs}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="599" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img102.png"
 ALT="% latex2html id marker 48680
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Training Sub-word HMMs}
\end{center}\end{center} }$">|; 

$key = q/displaystylesum_{xinmathbb{W}}C(x).logleft(frac{C(x)}{C(G(x))}right);+;sum_{g,hinmathbb{G}}C(g,h).logleft(frac{C(g,h)}{C(h)}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="406" HEIGHT="54" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img541.png"
 ALT="$\displaystyle \sum_{x \in \mathbb{W}} C(x) . \log \left(\frac{C(x)}{C(G(x))}\right)
\;+\; \sum_{g,h \in \mathbb{G}} C(g,h) . \log\left(\frac{C(g,h)}{C(h)}\right)$">|; 

$key = q/+6220;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img861.png"
 ALT="$ +6220$">|; 

$key = q/+2661;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img807.png"
 ALT="$ +2661$">|; 

$key = q/displaystyleH=-lim_{mtoinfty}frac{1}{m}sum_{w_1,w_2,ldots,w_m}left(P(w_1,w_2,ldots,w_m);log_2P(w_1,w_2,ldots,w_m)right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="496" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img594.png"
 ALT="$\displaystyle H = - \lim_{m \to \infty} \frac{1}{m} \sum_{w_1, w_2, \ldots, w_m}\left( P(w_1, w_2, \ldots, w_m)\; \log_2 P(w_1, w_2, \ldots, w_m) \right)$">|; 

$key = q/pm7032;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img905.png"
 ALT="$ \pm 7032$">|; 

$key = q/fbox{texttt{Command}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img685.png"
 ALT="\fbox{\texttt{Command}}">|; 

$key = q/log(C(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img551.png"
 ALT="$ \log(C(x))$">|; 

$key = q/|mathbb{W}|^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img495.png"
 ALT="$ \vert\mathbb{W}\vert^n$">|; 

$key = q/+8252;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img945.png"
 ALT="$ +8252$">|; 

$key = q/displaystylemathcalE_{textrm{class-{itn}-gram}}(w_1,ldots,w_{i})=mathcalE(G(w_{i-n+1}),ldots,G(w_{i}));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="362" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img509.png"
 ALT="$\displaystyle \mathcal E_{\textrm{class-{\it n}-gram}}(w_1, \ldots, w_{i}) = \mathcal E(G(w_{i-n+1}), \ldots, G(w_{i}))$">|; 

$key = q/8632;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img958.png"
 ALT="$ 8632$">|; 

$key = q/ngeqslant1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="43" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img491.png"
 ALT="$ n \geqslant 1$">|; 

$key = q/G(.);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="33" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img510.png"
 ALT="$ G(.)$">|; 

$key = q/G(w_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img512.png"
 ALT="$ G(w_i)$">|; 

$key = q/displaystylebeta_i(T)=a_{iN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="89" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img75.png"
 ALT="$\displaystyle \beta_i(T) = a_{iN}$">|; 

$key = q/pm2655;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img805.png"
 ALT="$ \pm 2655$">|; 

$key = q/displaystylealpha_j(t)=left[sum_{i=2}^{N-1}alpha_i(t-1)a_{ij}right]b_j({mbox{boldmatho}}_t).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="238" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img62.png"
 ALT="$\displaystyle \alpha_j(t) = \left[ \sum_{i=2}^{N-1} \alpha_i(t-1) a_{ij} \right] b_j({\mbox{\boldmath$o$}}_t).$">|; 

$key = q/P(w|x)=P(w|y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="125" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img502.png"
 ALT="$ P(w \vert x) = P(w \vert y)$">|; 

$key = q/displaystyle=frac{N-D-S}{N}times100%;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="159" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img478.png"
 ALT="$\displaystyle = \frac{N-D-S}{N} \times 100\%$">|; 

$key = q/framebox[100mm]{minipage{{100mm}program{mbox{{sim{textsf{o}}mbox{{<{textsf{VecSi00.00.60.4>>0.00.00.00.0mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="580" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img232.png"
 ALT="\framebox[100mm]{
\begin{minipage}{100mm}
\begin{program}
\mbox{$\sim$\textsf{...
... 0.0 0.0 0.0 0.0 \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/displaystyleP(x(t)=j|{mbox{boldmathO}},M);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="127" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img81.png"
 ALT="$\displaystyle P(x(t)=j\vert{\mbox{\boldmath$O$}},M)$">|; 

$key = q/displaystyle{mbox{boldmathw}}_{ri}=left(alpha{{bfp}_{ri}}+{bfk}^{(i)}_rright){bfG}^{(i)-1}_r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="194" HEIGHT="44" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img440.png"
 ALT="$\displaystyle {\mbox{\boldmath$w$}}_{ri} = \left(\alpha{{\bf p}_{ri}} + {\bf k}^{(i)}_r\right){\bf G}^{(i)-1}_r$">|; 

$key = q/%latex2htmlidmarker50444textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}e{Fig.thechapter.arabic{figctr}textsc{HRest}Operation}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="483" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img259.png"
 ALT="% latex2html id marker 50444
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...apter.\arabic{figctr}  \textsc{HRest} Operation}
\end{center}\end{center} }$">|; 

$key = q/%latex2htmlidmarker48705textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step4}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="426" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img109.png"
 ALT="% latex2html id marker 48705
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 4}
\end{center}\end{center} }$">|; 

$key = q/+6251;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img865.png"
 ALT="$ +6251$">|; 

$key = q/0.00001;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img663.png"
 ALT="$ 0.00001$">|; 

$key = q/+16920;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1005.png"
 ALT="$ +16920$">|; 

$key = q/:;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="8" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img659.png"
 ALT="$ \:$">|; 

$key = q/b_j({mbox{boldmatho}}_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img17.png"
 ALT="$ b_j({\mbox{\boldmath $o$}}_t)$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashHLMoperation.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="409" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img486.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//HLMoperation.eps}
\end{center} }$">|; 

$key = q/+??00;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img715.png"
 ALT="$ +??00$">|; 

$key = q/a'=d_a,.,a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="72" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img564.png"
 ALT="$ a' = d_a  .  a$">|; 

$key = q/displaystyleB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img198.png"
 ALT="$\displaystyle B$">|; 

$key = q/(a,b,c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img984.png"
 ALT="$ (a,b,c)$">|; 

$key = q/displaystyled_t=frac{sum_{theta=1}^Thetatheta(c_{t+theta}-c_{t-theta})}{2sum_{theta=1}^Thetatheta^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="61" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img176.png"
 ALT="$\displaystyle d_t = \frac{ \sum_{\theta =1}^\Theta \theta(c_{t+\theta} - c_{t-\theta}) }{ 2 \sum_{\theta = 1}^\Theta \theta^2 }$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img60.png"
 ALT="$ N$">|; 

$key = q/+5174;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img842.png"
 ALT="$ +5174$">|; 

$key = q/displaystylec_i=sqrt{frac{2}{N}}sum_{j=1}^Nm_jcosleft(frac{pii}{N}(j-0.5)right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="249" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img171.png"
 ALT="$\displaystyle c_i = \sqrt{\frac{2}{N}} \sum_{j=1}^N m_j \cos \left( \frac{\pi i}{N}(j-0.5) \right)$">|; 

$key = q/+1230;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img731.png"
 ALT="$ +1230$">|; 

$key = q/+7030;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img903.png"
 ALT="$ +7030$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="11" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img609.png"
 ALT="$ b$">|; 

$key = q/+6371;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img882.png"
 ALT="$ +6371$">|; 

$key = q/fbox{texttt{Restore}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="73" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img692.png"
 ALT="\fbox{\texttt{Restore}}">|; 

$key = q/displaystyled(i,j)=-frac{1}{S}sum_{s=1}^Sfrac{1}{M_s}sum_{m=1}^{M_s}log[b_{js}({mbox{boldmathmu}}_{ism})]+log[b_{is}({mbox{boldmathmu}}_{jsm})];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="390" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img636.png"
 ALT="$\displaystyle d(i,j) = - \frac{1}{S} \sum_{s=1}^S \frac{1}{M_s} \sum_{m=1}^{M_s...
...}({\mbox{\boldmath$\mu$}}_{ism})] + \log[b_{is}({\mbox{\boldmath$\mu$}}_{jsm})]$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img221.png"
 ALT="$ v$">|; 

$key = q/times2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img699.png"
 ALT="$ \times 2$">|; 

$key = q/phi_N(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="49" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img275.png"
 ALT="$ \phi_N(T)$">|; 

$key = q/pm3030;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img810.png"
 ALT="$ \pm 3030$">|; 

$key = q/10^{11};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img498.png"
 ALT="$ 10^{11}$">|; 

$key = q/+7150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img914.png"
 ALT="$ +7150$">|; 

$key = q/fbox{texttt{Z.In}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img690.png"
 ALT="\fbox{\texttt{Z.In}}">|; 

$key = q/%latex2htmlidmarker50436textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}ebf{Fig.thechapter.arabic{figctr}IsolatedWordTraining}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="495" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img254.png"
 ALT="% latex2html id marker 50436
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Isolated Word Training}
\end{center}\end{center} }$">|; 

$key = q/{mbox{boldmathSigma}}_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img53.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}_j$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{mbox{boldmathmu}}}_{jsm})'}{sum_{r=1}^Rsum_{t=1}^{T_r}L^r_{jsm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="384" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img323.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}}_{jsm} = \frac{ \sum_{r=1}^R \sum...
...\mbox{\boldmath$\mu$}}}_{jsm})' }{ \sum_{r=1}^R \sum_{t=1}^{T_r} L^r_{jsm}(t) }$">|; 

$key = q/{mbox{boldmathzeta}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img353.png"
 ALT="$ {\mbox{\boldmath $\zeta$}}$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.netdict.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="353" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img455.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.netdict.eps}
\end{center} }$">|; 

$key = q/DeltaDeltaDelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img103.png"
 ALT="$ \Delta\Delta\Delta$">|; 

$key = q/framebox[80mm]{minipage{{80mm}program{mbox{{sim{textsf{o}}mbox{{<{textsf{DISCRET00.00.60.4>>0.00.00.00.0mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="484" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img249.png"
 ALT="\framebox[80mm]{
\begin{minipage}{80mm}
\begin{program}
\mbox{$\sim$\textsf{o}...
...0.0 0.0 0.0 0.0 \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/displaystylealpha^{(q)}_{1}(1)=left{{array}{cl}1&mbox{ifq=1}alpha^{(q-1)}_1(1)a^{(q-1)}_{1N_{q-1}}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="295" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img293.png"
 ALT="$\displaystyle \alpha^{(q)}_{1}(1) =
\left\{ \begin{array}{cl}
1 &amp; \mbox{if $q=...
...\alpha^{(q-1)}_1(1) a^{(q-1)}_{1N_{q-1}} &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/displaystyleargmax_ileft{P(w_i|{mbox{boldmathO}})right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="137" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img8.png"
 ALT="$\displaystyle \arg\max_i \left\{ P(w_i \vert {\mbox{\boldmath$O$}}) \right\}$">|; 

$key = q/sim;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img224.png"
 ALT="$ \sim$">|; 

$key = q/{figure}preform{<verbatim_mark>verbatim564#preform{{{{figure};FSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="308" HEIGHT="757" BORDER="0"
 SRC="|."$dir".q|img365.png"
 ALT="\begin{figure}\begin{verbatim}&nbsp;a \lq\lq mjfg''
&lt;ADAPTKIND&gt; TREE
&lt;BASECLASSES&gt;&nbsp;b...
...
&lt;CLASSXFORM&gt; 2 1
&lt;CLASSXFORM&gt; 3 1
&lt;CLASSXFORM&gt; 4 2\end{verbatim}\end{figure}">|; 

$key = q/pm2630;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img793.png"
 ALT="$ \pm 2630$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img121.png"
 ALT="$ \mu$">|; 

$key = q/pm3150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img815.png"
 ALT="$ \pm 3150$">|; 

$key = q/8624;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img955.png"
 ALT="$ 8624$">|; 

$key = q/+7270;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img924.png"
 ALT="$ +7270$">|; 

$key = q/+15350;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img982.png"
 ALT="$ +15350$">|; 

$key = q/{k_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img146.png"
 ALT="$ \{k_i\}$">|; 

$key = q/{mbox{boldmatho}}_6;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img21.png"
 ALT="$ {\mbox{\boldmath $o$}}_6$">|; 

$key = q/fbox{texttt{Quit}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img684.png"
 ALT="\fbox{\texttt{Quit}}">|; 

$key = q/displaystyled_a=(a+1)frac{c_{a+1}}{a,.,c_a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="43" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img572.png"
 ALT="$\displaystyle d_a = (a+1) \frac{c_{a+1}}{a . c_a}$">|; 

$key = q/+6870;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img961.png"
 ALT="$ +6870$">|; 

$key = q/displaystyle{calQ}({calM},{hat{calM}})=K+frac{1}{2}sum_{r=1}^Rbeta_rlog({bfc}_{r_{ri}^T)-sum_{j=1}^d{left({bfa}_{rj}{bfG}^{(j)}_r{bfa}^T_{rj}right)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="401" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img416.png"
 ALT="$\displaystyle {\cal Q}({\cal M},{\hat{\cal M}}) = K +
\frac{1}{2}
\sum_{r=1}^R
...
...{ri}^T)-
\sum_{j=1}^d{
\left({\bf a}_{rj}{\bf G}^{(j)}_r{\bf a}^T_{rj}
\right)}$">|; 

$key = q/b_j(cdot);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img272.png"
 ALT="$ b_j(\cdot)$">|; 

$key = q/+15052;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img964.png"
 ALT="$ +15052$">|; 

$key = q/%latex2htmlidmarker48697textstyleparbox{25mm}{center{setlength{epsfxsize}{25mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step1}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="117" HEIGHT="285" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img106.png"
 ALT="% latex2html id marker 48697
$\textstyle \parbox{25mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 1}
\end{center}\end{center} }$">|; 

$key = q/%latex2htmlidmarker48728textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}etr}center{textbf{Fig.thechapter.arabic{figctr}Step10}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="441" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img119.png"
 ALT="% latex2html id marker 48728
$\textstyle \parbox{50mm}{ \begin{center}\setlength...
...xtbf{ Fig. \thechapter.\arabic{figctr}  Step 10}
\end{center}\end{center} }$">|; 

$key = q/0.2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img632.png"
 ALT="$ 0.2$">|; 

$key = q/+1328;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img735.png"
 ALT="$ +1328$">|; 

$key = q/P_{js}[v];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img220.png"
 ALT="$ P_{js}[v]$">|; 

$key = q/+15520;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img993.png"
 ALT="$ +15520$">|; 

$key = q/+7231;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img921.png"
 ALT="$ +7231$">|; 

$key = q/-2331;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img779.png"
 ALT="$ -2331$">|; 

$key = q/displaystyle{mbox{boldmathW}}_r=left[{array}{cc}-{mbox{boldmathA}}_rtilde{{mbox{ht]=left[{array}{cc}{mbox{boldmathb}}&{mbox{boldmathA}}{array}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="268" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img434.png"
 ALT="$\displaystyle {\mbox{\boldmath$W$}}_r = \left[\begin{array}{c c}
-{\mbox{\boldm...
...in{array}{c c}
{\mbox{\boldmath$b$}} &amp; {\mbox{\boldmath$A$}} \end{array}\right]$">|; 

$key = q/+6572;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img897.png"
 ALT="$ +6572$">|; 

$key = q/-2089;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img750.png"
 ALT="$ -2089$">|; 

$key = q/displaystyle{calN}({mbox{boldmatho}};{mbox{boldmathmu}},{mbox{boldmathSigma}})=f})'{mbox{boldmathSigma}}^{-1}({mbox{boldmatho}}-{mbox{boldmathmu}})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="322" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img47.png"
 ALT="$\displaystyle {\cal N}({\mbox{\boldmath$o$}}; {\mbox{\boldmath$\mu$}}, {\mbox{\...
...{\mbox{\boldmath$\Sigma$}}^{-1}({\mbox{\boldmath$o$}}-{\mbox{\boldmath$\mu$}})}$">|; 

$key = q/-32767;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img190.png"
 ALT="$ -32767$">|; 

$key = q/displaystylehat{a}_{ij}=frac{A_{ij}}{sum_{k=2}^{N}A_{ik}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img274.png"
 ALT="$\displaystyle \hat{a}_{ij} = \frac{A_{ij}}{\sum_{k=2}^{N}A_{ik}}
$">|; 

$key = q/pm3231;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img818.png"
 ALT="$ \pm 3231$">|; 

$key = q/%latex2htmlidmarker51247textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmc{figctr}Cross-WordTriphoneExpansionofBit-ButNetwork}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="286" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img472.png"
 ALT="% latex2html id marker 51247
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
... Cross-Word Triphone Expansion of Bit-But Network}
\end{center}\end{center} }$">|; 

$key = q/fbox{texttt{Undo}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img712.png"
 ALT="\fbox{\texttt{Undo}}">|; 

$key = q/displaystylebeta_i(t)=sum_{j=2}^{N-1}a_{ij}b_j({mbox{boldmatho}}_{t+1})beta_j(t+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="226" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img74.png"
 ALT="$\displaystyle \beta_i(t) = \sum_{j=2}^{N-1} a_{ij} b_j({\mbox{\boldmath$o$}}_{t+1}) \beta_j(t+1)$">|; 

$key = q/displaystylebeta_1(1)=sum_{j=2}^{N-1}a_{1j}b_j({mbox{boldmatho}}_1)beta_j(1).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="196" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img77.png"
 ALT="$\displaystyle \beta_1(1) = \sum_{j=2}^{N-1} a_{1j} b_j({\mbox{\boldmath$o$}}_1) \beta_j(1).$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img130.png"
 ALT="$ k$">|; 

$key = q/%latex2htmlidmarker48679textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmtbf{Fig.thechapter.arabic{figctr}HTKProcessingStages}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="455" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img101.png"
 ALT="% latex2html id marker 48679
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...chapter.\arabic{figctr}  HTK Processing Stages}
\end{center}\end{center} }$">|; 

$key = q/ge10T-0.5;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="88" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img482.png"
 ALT="$ \ge 10T - 0.5$">|; 

$key = q/tau;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img369.png"
 ALT="$ \tau$">|; 

$key = q/%latex2htmlidmarker48674textstyleparbox{75mm}{center{setlength{epsfxsize}{75mm}ebf{Fig.thechapter.arabic{figctr}SoftwareArchitecture}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="344" HEIGHT="370" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img100.png"
 ALT="% latex2html id marker 48674
$\textstyle \parbox{75mm}{ \begin{center}\setlength...
...echapter.\arabic{figctr}  Software Architecture}
\end{center}\end{center} }$">|; 

$key = q/+2121;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img752.png"
 ALT="$ +2121$">|; 

$key = q/(g,h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img546.png"
 ALT="$ (g,h)$">|; 

$key = q/+5021;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img830.png"
 ALT="$ +5021$">|; 

$key = q/%latex2htmlidmarker50439textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}e{Fig.thechapter.arabic{figctr}textsc{HInit}Operation}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="625" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img256.png"
 ALT="% latex2html id marker 50439
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...apter.\arabic{figctr}  \textsc{HInit} Operation}
\end{center}\end{center} }$">|; 

$key = q/displaystylek_j^{(i)}=k_j^{(i-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img152.png"
 ALT="$\displaystyle k_j^{(i)} = k_j^{(i-1)}$">|; 

$key = q/displaystyled_a=frac{a-m}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="87" HEIGHT="47" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img581.png"
 ALT="$\displaystyle d_a = \frac{a-m}{a}$">|; 

$key = q/displaystyleP({mbox{boldmathO}}|M)=sum_Xa_{x(0)x(1)}prod_{t=1}^Tb_{x(t)}({mbox{boldmatho}}_t)a_{x(t)x(t+1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="320" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img27.png"
 ALT="$\displaystyle P({\mbox{\boldmath$O$}}\vert M) = \sum_X a_{x(0)x(1)} \prod_{t=1}^T b_{x(t)}({\mbox{\boldmath$o$}}_t) a_{x(t)x(t+1)}$">|; 

$key = q/displaystylehat{a}^{(q)}_{iN_q}=frac{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r-1}alp{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r}alpha^{(q)r}_i(t)beta^{(q)r}_i(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="306" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img327.png"
 ALT="$\displaystyle \hat{a}^{(q)}_{iN_q} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=...
...m_{r=1}^R \frac{1}{P_r}
\sum_{t=1}^{T_r}
\alpha^{(q)r}_i(t)\beta^{(q)r}_i(t)
}
$">|; 

$key = q/+6324;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img875.png"
 ALT="$ +6324$">|; 

$key = q/{mbox{boldmatho}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="13" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img48.png"
 ALT="$ {\mbox{\boldmath $o$}}$">|; 

$key = q/%latex2htmlidmarker51138textstyleparbox{100mm}{center{setlength{epsfxsize}{100mmf{Fig.thechapter.arabic{figctr}Data-drivenstatetying}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="477" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img444.png"
 ALT="% latex2html id marker 51138
$\textstyle \parbox{100mm}{ \begin{center}\setlengt...
...hapter.\arabic{figctr}  Data-driven state tying}
\end{center}\end{center} }$">|; 

$key = q/P({mbox{boldmatho}}_1,{mbox{boldmatho}}_2,ldots|w_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="118" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img14.png"
 ALT="$ P({\mbox{\boldmath $o$}}_1,{\mbox{\boldmath $o$}}_2,\ldots \vert w_i)$">|; 

$key = q/%latex2htmlidmarker51210textstyleparbox{120mm}{center{setlength{epsfxsize}{120mmhapter.arabic{figctr}ExampleDigitRecognitionNetworks}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="383" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img459.png"
 ALT="% latex2html id marker 51210
$\textstyle \parbox{120mm}{ \begin{center}\setlengt...
...bic{figctr}  Example Digit Recognition Networks}
\end{center}\end{center} }$">|; 

$key = q/+8250;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img943.png"
 ALT="$ +8250$">|; 

$key = q/displaystylebeta^{(q)}_{N_q}(t)=left{{array}{cl}0&mbox{ifq=Q}beta^{(q+1)}_1(t+1)(q+1)}_{N_{q+1}}(t)a^{(q+1)}_{1N_{q+1}}&mbox{otherwise}{array}right.;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="397" HEIGHT="63" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img305.png"
 ALT="$\displaystyle \beta^{(q)}_{N_q}(t) =
\left\{ \begin{array}{cl}
0 &amp; \mbox{if $q...
...+1)}_{N_{q+1}} (t)
a^{(q+1)}_{1N_{q+1}} &amp; \mbox{otherwise}
\end{array}\right.
$">|; 

$key = q/S_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="22" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img675.png"
 ALT="$ S_k$">|; 

$key = q/-8630;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img956.png"
 ALT="$ -8630$">|; 

$key = q/displaystyleb_{j}({mbox{boldmatho}}_t)=prod_{s=1}^Sleft[sum_{m=1}^{M_s}c_{jsm}{c;{mbox{boldmathmu}}_{sm},{mbox{boldmathSigma}}_{sm})right]^{gamma_s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="307" HEIGHT="68" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img242.png"
 ALT="$\displaystyle b_{j}({\mbox{\boldmath$o$}}_t) = \prod_{s=1}^S \left[ \sum_{m=1}^...
...mbox{\boldmath$\mu$}}_{sm}, {\mbox{\boldmath$\Sigma$}}_{sm}) \right]^{\gamma_s}$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{m_r}={mbox{boldmathmu}}_{m_r},::::hat{{mbox={mbox{boldmathB}}_{m_r}^T{mbox{boldmathH}}_r{mbox{boldmathB}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="238" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img408.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{m_r} = {\mbox{\boldmath$\mu$}}_{m_...
...\mbox{\boldmath$B$}}_{m_r}^T{\mbox{\boldmath$H$}}_r{\mbox{\boldmath$B$}}_{m_r}
$">|; 

$key = q/displaystylesum_{m_r=1}^{M_r}sum_{t=1}^TL_{m_r}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="114" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img418.png"
 ALT="$\displaystyle \sum_{m_r=1}^{M_r}\sum_{t=1}^TL_{m_r}(t)$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img212.png"
 ALT="$ n-1$">|; 

$key = q/{mbox{boldmathSigma}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img46.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}$">|; 

$key = q/hat{P}(cal{W});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img489.png"
 ALT="$ \hat{P}(\cal{W})$">|; 

$key = q/textstyleparbox{120mm}{center{setlength{epsfxsize}{120mm}epsfbox{HTKFigsslashslashLTool.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="547" HEIGHT="297" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img610.png"
 ALT="$\textstyle \parbox{120mm}{ \begin{center}\setlength{\epsfxsize}{120mm}
\epsfbox{HTKFigs//LTool.eps}
\end{center} }$">|; 

$key = q/%latex2htmlidmarker49219textstyleparbox{65mm}{center{setlength{epsfxsize}{65mm}eFig.thechapter.arabic{figctr}UsingVectorQuantisation}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="298" HEIGHT="681" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img205.png"
 ALT="% latex2html id marker 49219
$\textstyle \parbox{65mm}{ \begin{center}\setlength...
...pter.\arabic{figctr}  Using Vector Quantisation}
\end{center}\end{center} }$">|; 

$key = q/%latex2htmlidmarker49752textstyleparbox{60mm}{noindentfbox{parbox{60mm}{hspace{6extbf{Fig.thechapter.arabic{figctr}SimpleMixtureGaussianHMM}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="735" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img229.png"
 ALT="% latex2html id marker 49752
$\textstyle \parbox{60mm}{\noindent
\fbox{ \parbox...
... \thechapter.\arabic{figctr}  Simple Mixture Gaussian HMM}
\end{center}
}$">|; 

$key = q/p(w_k|c_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="63" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img607.png"
 ALT="$ p(w_k\vert c_k)$">|; 

$key = q/-2638;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img799.png"
 ALT="$ -2638$">|; 

$key = q/displaystyleb(i)=frac{1-sum_{jinB}p(i,j)}{1-sum_{jinB}p(j)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="167" HEIGHT="58" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img653.png"
 ALT="$\displaystyle b(i) = \frac{1 - \sum_{j \in B} p(i,j)}{1 - \sum_{j \in B} p(j)}
$">|; 

$key = q/%latex2htmlidmarker48722textstyleparbox{85mm}{center{setlength{epsfxsize}{85mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step8}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="389" HEIGHT="291" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img116.png"
 ALT="% latex2html id marker 48722
$\textstyle \parbox{85mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 8}
\end{center}\end{center} }$">|; 

$key = q/w_1,w_2,ldots,w_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="120" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img621.png"
 ALT="$ w_1, w_2, \ldots, w_{n-1}$">|; 

$key = q/{C_4,C_5,C_6,C_7};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="115" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img358.png"
 ALT="$ \{C_4, C_5, C_6,
C_7\}$">|; 

$key = q/{mbox{boldmathW_r}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img385.png"
 ALT="$ {\mbox{\boldmath $W_r$}}$">|; 

$key = q/t_s<t_m<t_e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="90" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img669.png"
 ALT="$ t_s &lt; t_m &lt; t_e$">|; 

$key = q/+5172;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img840.png"
 ALT="$ +5172$">|; 

$key = q/displaystylephi_N(T)=max_ileft{phi_i(T)a_{iN}right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="183" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img91.png"
 ALT="$\displaystyle \phi_N(T) = \max_i \left\{ \phi_i(T) a_{iN} \right\}$">|; 

$key = q/displaystyle{mbox{boldmathB}}_{m_r}={mbox{boldmathC}}_{m_r}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="91" HEIGHT="36" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img411.png"
 ALT="$\displaystyle {\mbox{\boldmath$B$}}_{m_r} = {\mbox{\boldmath$C$}}_{m_r}^{-1}
$">|; 

$key = q/displaystylelogP_mathrm{class}(mathcal{W});=;sum_{x,yinmathbb{W}}C(x,y).logP_mathrm{class}(x;|;y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="323" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img527.png"
 ALT="$\displaystyle \log P_\mathrm{class}(\mathcal{W}) \;=\; \sum_{x, y \in \mathbb{W}} C(x, y) . \log P_\mathrm{class}(x \;\vert\; y)$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img161.png"
 ALT="$ L$">|; 

$key = q/2^{16}-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img612.png"
 ALT="$ 2^{16}-1$">|; 

$key = q/+??15;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img723.png"
 ALT="$ +??15$">|; 

$key = q/32767;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="44" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img188.png"
 ALT="$ 32767$">|; 

$key = q/{k_j^{(i-1)}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="60" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img149.png"
 ALT="$ \{k_j^{(i-1)} \}$">|; 

$key = q/{mbox{boldmathzeta}}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="31" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img379.png"
 ALT="$ {\mbox{\boldmath $\zeta$}}(t)$">|; 

$key = q/+2531;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img791.png"
 ALT="$ +2531$">|; 

$key = q/+16620;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img998.png"
 ALT="$ +16620$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img7.png"
 ALT="$ t$">|; 

$key = q/%latex2htmlidmarker48943textstyleparbox{62mm}{center{setlength{epsfxsize}{62mm}ebf{Fig.thechapter.arabic{figctr}SpeechInputSubsystem}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="285" HEIGHT="368" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img126.png"
 ALT="% latex2html id marker 48943
$\textstyle \parbox{62mm}{ \begin{center}\setlength...
...chapter.\arabic{figctr}  Speech Input Subsystem}
\end{center}\end{center} }$">|; 

$key = q/pm1431;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img739.png"
 ALT="$ \pm 1431$">|; 

$key = q/{mbox{boldmatho}}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img378.png"
 ALT="$ {\mbox{\boldmath $o$}}(t)$">|; 

$key = q/+2651;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img804.png"
 ALT="$ +2651$">|; 

$key = q/a_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img18.png"
 ALT="$ a_{ij}$">|; 

$key = q/displaystyle=frac{H}{N}times100%;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="96" HEIGHT="51" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img665.png"
 ALT="$\displaystyle = \frac{H}{N} \times 100\%$">|; 

$key = q/+17051;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img1009.png"
 ALT="$ +17051$">|; 

$key = q/H_e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img673.png"
 ALT="$ H_e$">|; 

$key = q/displaystylealpha_j(t)=P({mbox{boldmatho}}_1,ldots,{mbox{boldmatho}}_t,x(t)=j|M).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="241" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img61.png"
 ALT="$\displaystyle \alpha_j(t) = P({\mbox{\boldmath$o$}}_1,\ldots,{\mbox{\boldmath$o$}}_t, x(t)=j \vert M).$">|; 

$key = q/textstyleparbox{50mm}{center{setlength{epsfxsize}{50mm}epsfbox{HTKFigsslashslashtoolkit.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="231" HEIGHT="238" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img99.png"
 ALT="$\textstyle \parbox{50mm}{ \begin{center}\setlength{\epsfxsize}{50mm}
\epsfbox{HTKFigs//toolkit.eps}
\end{center} }$">|; 

$key = q/+8571;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img951.png"
 ALT="$ +8571$">|; 

$key = q/displaystylebeta^{(q)}_{1}(t)=sum_{j=2}^{N_q-1}a^{(q)}_{1j}b^{(q)}_j({mbox{boldmatho}}_t)beta^{(q)}_{j}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="227" HEIGHT="69" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img307.png"
 ALT="$\displaystyle \beta^{(q)}_{1}(t) =
\sum_{j=2}^{N_q-1} a^{(q)}_{1j} b^{(q)}_j({\mbox{\boldmath$o$}}_t)
\beta^{(q)}_{j}(t)
$">|; 

$key = q/fbox{texttt{Pause}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="57" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img695.png"
 ALT="\fbox{\texttt{Pause}}">|; 

$key = q/displaystyle{calN}({mbox{boldmatho}};{mbox{boldmathmu}},{mbox{boldmathH}}{mbox{bldmatho}};{mbox{boldmathA}}{mbox{boldmathmu}},{mbox{boldmathSigma}});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="470" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img350.png"
 ALT="$\displaystyle {\cal N}({\mbox{\boldmath$o$}};{\mbox{\boldmath$\mu$}},{\mbox{\bo...
...h$o$}};{\mbox{\boldmath$A$}}{\mbox{\boldmath$\mu$}},{\mbox{\boldmath$\Sigma$}})$">|; 

$key = q/textstyleparbox{80mm}{center{setlength{epsfxsize}{80mm}epsfbox{HTKFigsslashslashTool.shell.eps}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="352" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img122.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//Tool.shell.eps}
\end{center} }$">|; 

$key = q/displaystylehat{{mbox{boldmathSigma}}}_j=frac{1}{T}sum_{t=1}^{T}({mbox{boldmatho}_t-{mbox{boldmathmu}}_j)({mbox{boldmatho}}_t-{mbox{boldmathmu}}_j)';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="220" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img55.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\Sigma$}}}_j = \frac{1}{T} \sum_{t=1}^{T} (...
...mbox{\boldmath$\mu$}}_j) ({\mbox{\boldmath$o$}}_t - {\mbox{\boldmath$\mu$}}_j)'$">|; 

$key = q/P(mathcal{A})=frac{a}{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="76" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img561.png"
 ALT="$ P(\mathcal{A}) = \frac{a}{A}$">|; 

$key = q/a=10T-N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="95" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img484.png"
 ALT="$ a = 10T - N$">|; 

$key = q/+15050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img962.png"
 ALT="$ +15050$">|; 

$key = q/{mbox{boldmatho}}^r_{st};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="25" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img277.png"
 ALT="$ {\mbox{\boldmath $o$}}^r_{st}$">|; 

$key = q/t_e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="17" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img668.png"
 ALT="$ t_e$">|; 

$key = q/0.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="24" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img137.png"
 ALT="$ 0.0$">|; 

$key = q/%latex2htmlidmarker49653textstyleparbox{60mm}{noindentfbox{parbox{60mm}{hspace{6xtbf{Fig.thechapter.arabic{figctr}DefinitionforSimpleL-RHMM}center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="296" HEIGHT="620" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img223.png"
 ALT="% latex2html id marker 49653
$\textstyle \parbox{60mm}{\noindent
\fbox{ \parbox...
...thechapter.\arabic{figctr}  Definition for Simple L-R HMM}
\end{center}
}$">|; 

$key = q/2^{16};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="16" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img613.png"
 ALT="$ 2^{16}$">|; 

$key = q/+7020;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img898.png"
 ALT="$ +7020$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="16" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img194.png"
 ALT="$ A$">|; 

$key = q/+6570;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img895.png"
 ALT="$ +6570$">|; 

$key = q/ldots,w_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="62" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img603.png"
 ALT="$ \ldots, w_m)$">|; 

$key = q/gamma_s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="19" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img49.png"
 ALT="$ \gamma_s$">|; 

$key = q/displaystyle2*Islash(x_{max}-x_{min});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="144" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img197.png"
 ALT="$\displaystyle 2*I/(x_{max}-x_{min})$">|; 

$key = q/t+Deltat;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img290.png"
 ALT="$ t+\Delta t$">|; 

$key = q/M_{js};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img216.png"
 ALT="$ M_{js}$">|; 

$key = q/+7037;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img908.png"
 ALT="$ +7037$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="10" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img10.png"
 ALT="$ i$">|; 

$key = q/p_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img566.png"
 ALT="$ p_a$">|; 

$key = q/displaystyle{bfk}^{(i)}_{r}=sum_{m_r=1}^{M_r}sumlimits_{t=1}^TL_{m_r}(t)frac{1}{sigma^{2}_{m_ri}}o_i(t){{mbox{boldmathxi}}}^{T}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="254" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img399.png"
 ALT="$\displaystyle {\bf k}^{(i)}_{r} = \sum_{m_r=1}^{M_r}\sum\limits_{t=1}^T
L_{m_r}(t)\frac{1}{\sigma^{2}_{m_ri}}
o_i(t){{\mbox{\boldmath$\xi$}}}^{T}_{m_r}$">|; 

$key = q/displaystylehat{a}^{(q)}_{ij}=frac{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r-1}alpha{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r}alpha^{(q)r}_i(t)beta^{(q)r}_i(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="391" HEIGHT="67" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img325.png"
 ALT="$\displaystyle \hat{a}^{(q)}_{ij} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=1}...
...m_{r=1}^R \frac{1}{P_r}
\sum_{t=1}^{T_r}
\alpha^{(q)r}_i(t)\beta^{(q)r}_i(t)
}
$">|; 

$key = q/d_t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="18" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img177.png"
 ALT="$ d_t$">|; 

$key = q/1slash(N+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="74" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img466.png"
 ALT="$ 1/(N+1)$">|; 

$key = q/{mbox{boldmathmu}}_{m_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img380.png"
 ALT="$ {\mbox{\boldmath $\mu$}}_{m_r}$">|; 

$key = q/displaystyled_t=frac{(c_{t+Theta}-c_{t-Theta})}{2Theta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="139" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img184.png"
 ALT="$\displaystyle d_t = \frac{ (c_{t+\Theta} - c_{t-\Theta}) }{ 2 \Theta}$">|; 

$key = q/+6322;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img873.png"
 ALT="$ +6322$">|; 

$key = q/+15340;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img979.png"
 ALT="$ +15340$">|; 

$key = q/-1389;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img737.png"
 ALT="$ -1389$">|; 

$key = q/0leqk<1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img131.png"
 ALT="$ 0 \leq k &lt; 1$">|; 

$key = q/P(w_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="46" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img12.png"
 ALT="$ P(w_i)$">|; 

$key = q/+2030;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img747.png"
 ALT="$ +2030$">|; 

$key = q/x_{min};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img201.png"
 ALT="$ x_{min}$">|; 

$key = q/framebox[80mm]{minipage{{80mm}program{mbox{{sim{textsf{h``htm''}}mbox{{<{textsf{00.00.60.4>>0.00.00.00.0mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="366" HEIGHT="274" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img248.png"
 ALT="\framebox[80mm]{
\begin{minipage}{80mm}
\begin{program}
\mbox{$\sim$\textsf{h ...
...0.0 0.0 0.0 0.0 \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/displaystyleP({mbox{boldmathO}}|M)=alpha_N(T).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="136" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img70.png"
 ALT="$\displaystyle P({\mbox{\boldmath$O$}}\vert M) = \alpha_N(T).$">|; 

$key = q/+7171;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img916.png"
 ALT="$ +7171$">|; 

$key = q/{mbox{boldmathO}}^r,;;1leqrleqR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img269.png"
 ALT="$ {\mbox{\boldmath $O$}}^r, \;\; 1 \leq r \leq R$">|; 

$key = q/displaystyle{bfG}^{(i)}_r=sum_{m_r=1}^{M_r}frac{1}{sigma_{m_ri}^{2}}{{mbox{boldmathxi}}}_{m_r}{{mbox{boldmathxi}}}^{T}_{m_r}sum_{t=1}^TL_{m_r}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="257" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img398.png"
 ALT="$\displaystyle {\bf G}^{(i)}_r=\sum_{m_r=1}^{M_r}
\frac{1}{\sigma_{m_ri}^{2}}{{\...
...oldmath$\xi$}}}_{m_r}{{\mbox{\boldmath$\xi$}}}^{T}_{m_r}
\sum_{t=1}^TL_{m_r}(t)$">|; 

$key = q/-E_{min}..1.0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="82" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img173.png"
 ALT="$ -E_{min}..1.0$">|; 

$key = q/C(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="53" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img526.png"
 ALT="$ C(x, y)$">|; 

$key = q/|S_k|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="30" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img676.png"
 ALT="$ \vert S_k\vert$">|; 

$key = q/hat{P}(w_1,w_2,ldots,w_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="132" HEIGHT="37" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img600.png"
 ALT="$ \hat{P}(w_1, w_2, \ldots, w_m)$">|; 

$key = q/{bfp}_{ri};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="26" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img436.png"
 ALT="$ {\bf p}_{ri}$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{jsm}=frac{sum_{r=1}^Rsum_{t=1}^{T_r}psi^r_{){mbox{boldmatho}}^r_{st}}{sum_{r=1}^Rsum_{t=1}^{T_r}psi^r_{jsm}(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="222" HEIGHT="64" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img278.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{jsm} = \frac{
\sum_{r=1}^R \sum_{t...
...{\mbox{\boldmath$o$}}^r_{st}}{
\sum_{r=1}^R \sum_{t=1}^{T_r} \psi^r_{jsm}(t)
}
$">|; 

$key = q/+2150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img761.png"
 ALT="$ +2150$">|; 

$key = q/-2427;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img787.png"
 ALT="$ -2427$">|; 

$key = q/+5050;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img832.png"
 ALT="$ +5050$">|; 

$key = q/displaystylesum_{xinmathbb{W}}C(x).logC(x);+;sum_{g,hinmathbb{G}}C(g,h).logC(g,h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="327" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img544.png"
 ALT="$\displaystyle \sum_{x \in \mathbb{W}} C(x) . \log C(x)
\;+\; \sum_{g,h \in \mathbb{G}} C(g,h) . \log C(g,h)$">|; 

$key = q/framebox[100mm]{minipage{{100mm}program{parmbox{{sim{textsf{o}}>>mbox{{<{textsf{>0.00.00.60.40.0>>0.00.00.00.70.3>>0.00.00.00.00.0program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="465" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img237.png"
 ALT="\framebox[100mm]{
\begin{minipage}{100mm}
\begin{program}
\par
\mbox{$\sim$\te...
...0.0 0.0 0.0 0.7 0.3 \\\\
\&gt;\&gt; 0.0 0.0 0.0 0.0 0.0
\end{program} \end{minipage} }">|; 

$key = q/log_{10};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="38" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img618.png"
 ALT="$ \log_{10}$">|; 

$key = q/fbox{texttt{Play}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img693.png"
 ALT="\fbox{\texttt{Play}}">|; 

$key = q/displaystyleH(z)=frac{1}{sum_{i=0}^pa_iz^{-i}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="142" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img139.png"
 ALT="$\displaystyle H(z) = \frac{1}{\sum_{i=0}^p a_i z^{-i}}$">|; 

$key = q/G(w)=g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img507.png"
 ALT="$ G(w)
= g$">|; 

$key = q/displaystylehat{a}_{ij}=frac{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r-1}alpha^r_i(tt+1)}{sum_{r=1}^Rfrac{1}{P_r}sum_{t=1}^{T_r}alpha^r_i(t)beta^r_i(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="337" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img311.png"
 ALT="$\displaystyle \hat{a}_{ij} = \frac{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=1}^{T_r-...
...+1)
}{
\sum_{r=1}^R \frac{1}{P_r}
\sum_{t=1}^{T_r}
\alpha^r_i(t)\beta^r_i(t)
}
$">|; 

$key = q/{s_n,n=1,N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img129.png"
 ALT="$ \{s_n, n=1,N \}$">|; 

$key = q/displaystylehat{P}(w_i;|;w_{i-n+1},ldots,w_{i-1})=qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{}qquad{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="193" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img586.png"
 ALT="$\displaystyle \hat{P}(w_i \;\vert\; w_{i-n+1},\ldots,w_{i-1}) =\qquad{}\qquad{}...
...qquad{}\qquad{}\qquad{}\qquad{}\qquad{}\qquad{}\qquad{}\qquad{}\qquad{}\qquad{}$">|; 

$key = q/framebox[100mm]{minipage{{100mm}program{mbox{{sim{textsf{h``ha''}}mbox{{<{textsf{{sim{textsf{t``tran''}}mbox{{<{textsf{EndHMM}{>{}program{minipage{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="457" HEIGHT="695" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img238.png"
 ALT="\framebox[100mm]{
\begin{minipage}{100mm}
\begin{program}\mbox{$\sim$\textsf{h...
...tsf{t \lq\lq tran''}} \\\\
\mbox{$&lt;$\textsf{EndHMM}$&gt;$}
\end{program} \end{minipage} }">|; 

$key = q/P(w_j|w_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="68" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img464.png"
 ALT="$ P(w_j\vert w_i)$">|; 

$key = q/+5170;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img838.png"
 ALT="$ +5170$">|; 

$key = q/J;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="14" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img638.png"
 ALT="$ J$">|; 

$key = q/%latex2htmlidmarker48750textstyleparbox{60mm}{center{setlength{epsfxsize}{60mm}e{Fig.thechapter.arabic{figctr}DefiningaConfiguration}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="276" HEIGHT="297" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img123.png"
 ALT="% latex2html id marker 48750
$\textstyle \parbox{60mm}{ \begin{center}\setlength...
...apter.\arabic{figctr}  Defining a Configuration}
\end{center}\end{center} }$">|; 

$key = q/displaystyle{bfk}^{(i)}_r=sum_{m_r=1}^{M_r}frac{mu_{m_ri}}{sigma_{m_ri}^{2}}sum_{t=1}^TL_{m_r}(t){{mbox{boldmathzeta}}}^{T}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="236" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img439.png"
 ALT="$\displaystyle {\bf k}^{(i)}_r=\sum_{m_r=1}^{M_r}
\frac{\mu_{m_ri}}{\sigma_{m_ri}^{2}}
\sum_{t=1}^TL_{m_r}(t){{\mbox{\boldmath$\zeta$}}}^{T}(t)$">|; 

$key = q/%latex2htmlidmarker49046textstyleparbox{110mm}{center{setlength{epsfxsize}{110mmtbf{Fig.thechapter.arabic{figctr}Mel-ScaleFilterBank}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="502" HEIGHT="288" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img164.png"
 ALT="% latex2html id marker 49046
$\textstyle \parbox{110mm}{ \begin{center}\setlengt...
...echapter.\arabic{figctr}  Mel-Scale Filter Bank}
\end{center}\end{center} }$">|; 

$key = q/+2635;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img797.png"
 ALT="$ +2635$">|; 

$key = q/fbox{texttt{New}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img711.png"
 ALT="\fbox{\texttt{New}}">|; 

$key = q/+??13;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img721.png"
 ALT="$ +??13$">|; 

$key = q/+2320;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img773.png"
 ALT="$ +2320$">|; 

$key = q/+5220;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img844.png"
 ALT="$ +5220$">|; 

$key = q/pm3132;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img814.png"
 ALT="$ \pm 3132$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img314.png"
 ALT="$ r$">|; 

$key = q/%latex2htmlidmarker48726textstyleparbox{55mm}{center{setlength{epsfxsize}{55mm}ectr}center{textbf{Fig.thechapter.arabic{figctr}Step9}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="253" HEIGHT="441" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img118.png"
 ALT="% latex2html id marker 48726
$\textstyle \parbox{55mm}{ \begin{center}\setlength...
...extbf{ Fig. \thechapter.\arabic{figctr}  Step 9}
\end{center}\end{center} }$">|; 

$key = q/displaystyle{s^{prime}}_n=s_n-k,s_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="125" HEIGHT="33" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img128.png"
 ALT="$\displaystyle {s^{\prime}}_n = s_n - k s_{n-1}$">|; 

$key = q/displaystyleE^{(i)}=(1-k_i^{(i)}k_i^{(i)})E^{(i-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="185" HEIGHT="41" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img155.png"
 ALT="$\displaystyle E^{(i)} = (1 - k_i^{(i)} k_i^{(i)} ) E^{(i-1)}$">|; 

$key = q/x(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img28.png"
 ALT="$ x(0)$">|; 

$key = q/+2128;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img759.png"
 ALT="$ +2128$">|; 

$key = q/|mathbb{G}|<|mathbb{W}|;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="71" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img508.png"
 ALT="$ \vert\mathbb{G}\vert &lt; \vert\mathbb{W}\vert$">|; 

$key = q/displaystyle{bfH}_r^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="35" HEIGHT="35" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img420.png"
 ALT="$\displaystyle {\bf H}_r^{-1}$">|; 

$key = q/displaystylealpha_j(1)=a_{1j}b_j({mbox{boldmatho}}_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="126" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img67.png"
 ALT="$\displaystyle \alpha_j(1) = a_{1j} b_j({\mbox{\boldmath$o$}}_1)$">|; 

$key = q/displaystylehatP(w_1,w_2,ldots,w_m)=prod_{i=1}^{m}hatP(w_i;|;w_1,ldots,w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="318" HEIGHT="60" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img488.png"
 ALT="$\displaystyle \hat P(w_1, w_2, \ldots, w_m) = \prod_{i=1}^{m} \hat P(w_i \;\vert\; w_1, \ldots, w_{i-1})$">|; 

$key = q/displaystyleL^r_{jsm}(t)=frac{1}{P_r}U^r_j(t)c_{jsm}b_{jsm}({mbox{boldmatho}}^r_{st})beta^r_j(t)b^*_{js}({mbox{boldmatho}}^r_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="315" HEIGHT="49" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img317.png"
 ALT="$\displaystyle L^r_{jsm}(t) = \frac{1}{P_r} U^r_j(t) c_{jsm} b_{jsm}({\mbox{\boldmath$o$}}^r_{st})
\beta^r_j(t) b^*_{js}({\mbox{\boldmath$o$}}^r_t)
$">|; 

$key = q/8690;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="36" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img959.png"
 ALT="$ 8690$">|; 

$key = q/displaystyle{bfG}^{(i)}_r=sum_{m_r=1}^{M_r}frac{1}{sigma_{m_ri}^{2}}sum_{t=1}^TL_{m_r}(t){{mbox{boldmathzeta}}}(t){{mbox{boldmathzeta}}}^{T}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="268" HEIGHT="66" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img438.png"
 ALT="$\displaystyle {\bf G}^{(i)}_r=\sum_{m_r=1}^{M_r}
\frac{1}{\sigma_{m_ri}^{2}}
\s...
...=1}^TL_{m_r}(t){{\mbox{\boldmath$\zeta$}}}(t){{\mbox{\boldmath$\zeta$}}}^{T}(t)$">|; 

$key = q/{mbox{boldmathb}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img341.png"
 ALT="$ {\mbox{\boldmath $b$}}$">|; 

$key = q/displaystyle{mbox{boldmathxi}}=left[mbox{}wmbox{}mu_1mbox{}mu_2mbox{}dotsmbox{}mu_nmbox{}right]^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="172" HEIGHT="39" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img336.png"
 ALT="$\displaystyle {\mbox{\boldmath$\xi$}} = \left[\mbox{ }w\mbox{ }\mu_1\mbox{ }\mu_2\mbox{ }\dots\mbox{ }\mu_n\mbox{ }\right]^T
$">|; 

$key = q/+2039;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img748.png"
 ALT="$ +2039$">|; 

$key = q/infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="20" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img628.png"
 ALT="$ \infty$">|; 

$key = q/displaystylex_{short};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="45" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img192.png"
 ALT="$\displaystyle x_{short}$">|; 

$key = q/displaystyle{bfG}^{(i)}=sum_{c=1}^{C}{bfG}^{(i)}_{R_c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="111" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img407.png"
 ALT="$\displaystyle {\bf G}^{(i)} = \sum_{c=1}^{C} {\bf G}^{(i)}_{R_c}
$">|; 

$key = q/displaystyleP_mathrm{class}(w_i;|;w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="116" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img528.png"
 ALT="$\displaystyle P_\mathrm{class}(w_i \;\vert\; w_{i-1})$">|; 

$key = q/+6554;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img894.png"
 ALT="$ +6554$">|; 

$key = q/log[a_{ij}]+log[b_j({mbox{boldmatho}}(t)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="150" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img96.png"
 ALT="$ log[a_{ij}]+log[b_j({\mbox{\boldmath $o$}}(t)]$">|; 

$key = q/+15154;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="56" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img975.png"
 ALT="$ +15154$">|; 

$key = q/%latex2htmlidmarker50440textstyleparbox{90mm}{center{setlength{epsfxsize}{90mm}eechapter.arabic{figctr}FileProcessingintextsc{HInit}}center{center{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="412" HEIGHT="443" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img257.png"
 ALT="% latex2html id marker 50440
$\textstyle \parbox{90mm}{ \begin{center}\setlength...
...abic{figctr}  File Processing in \textsc{HInit}}
\end{center}\end{center} }$">|; 

$key = q/<>;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img658.png"
 ALT="$ &lt;&gt;$">|; 

$key = q/displaystylealpha(w_{i-n+1},ldots,w_{i-1})=frac{hat{beta}(w_{i-n+1},ldots,w_{i-1m_{w_i:c(w_{i-n+1},ldots,w_i)=0}hat{P}(w_i|w_{i-n+2},ldots,w_{i-1})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="482" HEIGHT="59" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img589.png"
 ALT="$\displaystyle \alpha(w_{i-n+1}, \ldots, w_{i-1}) = \frac{\hat{\beta}(w_{i-n+1},...
...w_i: c(w_{i-n+1}, \ldots, w_i)=0}\hat{P}(w_i \vert w_{i-n+2}, \ldots, w_{i-1})}$">|; 

$key = q/displaystylePP=2^{hat{H}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="70" HEIGHT="42" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img598.png"
 ALT="$\displaystyle PP = 2^{\hat{H}}$">|; 

$key = q/mathcal{W};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="21" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img521.png"
 ALT="$ \mathcal{W}$">|; 

$key = q/b(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="29" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img652.png"
 ALT="$ b(i)$">|; 

$key = q/+??05;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="47" HEIGHT="29" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img717.png"
 ALT="$ +??05$">|; 

$key = q/L(G);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="40" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img672.png"
 ALT="$ L(G)$">|; 

$key = q/+6150;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img854.png"
 ALT="$ +6150$">|; 

$key = q/pm3333;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img825.png"
 ALT="$ \pm 3333$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="15" HEIGHT="15" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img36.png"
 ALT="$ S$">|; 

$key = q/displaystylehatP(w_1,w_2,ldots,w_m)simeqprod_{i=1}^{m}hatP(w_i;|;w_{i-n+1},ldots,w_{i-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="350" HEIGHT="60" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img490.png"
 ALT="$\displaystyle \hat P(w_1, w_2, \ldots, w_m) \simeq \prod_{i=1}^{m} \hat P(w_i \;\vert\; w_{i-n+1}, \ldots, w_{i-1})$">|; 

$key = q/fbox{texttt{x1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="32" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img697.png"
 ALT="\fbox{\texttt{x1}}">|; 

$key = q/displaystyleP({mbox{boldmathO}}|w_i)=P({mbox{boldmathO}}|M_i).;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="153" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img32.png"
 ALT="$\displaystyle P({\mbox{\boldmath$O$}}\vert w_i) = P({\mbox{\boldmath$O$}}\vert M_i).$">|; 

$key = q/+5073;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img837.png"
 ALT="$ +5073$">|; 

$key = q/+7035;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img906.png"
 ALT="$ +7035$">|; 

$key = q/displaystylehat{a}_{1j}=frac{1}{R}sum_{r=1}^Rfrac{1}{P_r}alpha^r_j(1)beta^r_j(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="187" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img315.png"
 ALT="$\displaystyle \hat{a}_{1j} = \frac{1}{R}
\sum_{r=1}^R \frac{1}{P_r}
\alpha^r_j(1) \beta^r_j(1)
$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="12" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img548.png"
 ALT="$ g$">|; 

$key = q/+6376;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img887.png"
 ALT="$ +6376$">|; 

$key = q/+2641;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img802.png"
 ALT="$ +2641$">|; 

$key = q/+6270;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img869.png"
 ALT="$ +6270$">|; 

$key = q/10^{-5};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="37" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img447.png"
 ALT="$ 10^{-5}$">|; 

$key = q/+2223;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img767.png"
 ALT="$ +2223$">|; 

$key = q/+8232;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img942.png"
 ALT="$ +8232$">|; 

$key = q/K^{(m)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="41" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img389.png"
 ALT="$ K^{(m)}$">|; 

$key = q/displaystylehat{{mbox{boldmathmu}}}_{m_r}={mbox{boldmathmu}}_{m_r},::::hat{{mboxx{boldmathH}}}_r{{mbox{boldmathSigma}}}_{m_r}{{mbox{boldmathH}}}_r^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="242" HEIGHT="38" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img415.png"
 ALT="$\displaystyle \hat{{\mbox{\boldmath$\mu$}}}_{m_r} = {\mbox{\boldmath$\mu$}}_{m_...
...{\boldmath$H$}}}_r{{\mbox{\boldmath$\Sigma$}}}_{m_r}{{\mbox{\boldmath$H$}}}_r^T$">|; 

$key = q/displaystyleE=logsum_{n=1}^Ns_n^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="104" HEIGHT="65" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img172.png"
 ALT="$\displaystyle E = log \sum_{n=1}^N s_n^2$">|; 

$key = q/+6320;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img871.png"
 ALT="$ +6320$">|; 

$key = q/displaystyleleft{{array}{ccl}{bfW}_2&rightarrow&left{C_5right}{bfW}_3&rightarrow&left{C_6,C_7right}{bfW}_4&rightarrow&left{C_4right}{array}right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="182" HEIGHT="73" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img362.png"
 ALT="$\displaystyle \left\{
\begin{array}{ccl}
{\bf W}_2 &amp; \rightarrow &amp; \left\{C_5\r...
...7\right\} \\\\
{\bf W}_4 &amp; \rightarrow &amp; \left\{C_4\right\}
\end{array}\right\}
$">|; 

$key = q/c_{rij}={mbox{cof}}({bfA}_{rij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="113" HEIGHT="31" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img426.png"
 ALT="$ c_{rij}={\mbox{cof}}({\bf A}_{rij})$">|; 

$key = q/displaystyleP_mathrm{class}(mathcal{W});=;prod_{x,yinmathbb{W}}P_mathrm{class}(x;|;y)^{C(x,y)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="259" HEIGHT="53" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img523.png"
 ALT="$\displaystyle P_\mathrm{class}(\mathcal{W}) \;=\; \prod_{x, y \in \mathbb{W}} P_\mathrm{class}(x \;\vert\; y)^{C(x,y)}$">|; 

$key = q/+3228;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img816.png"
 ALT="$ +3228$">|; 

$key = q/+3331;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img823.png"
 ALT="$ +3331$">|; 

$key = q/pm1452;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img742.png"
 ALT="$ \pm 1452$">|; 

$key = q/-1089;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="48" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="|."$dir".q|img730.png"
 ALT="$ -1089$">|; 

$key = q/{mbox{boldmathSigma}}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 WIDTH="34" HEIGHT="17" ALIGN="BOTTOM" BORDER="0"
 SRC="|."$dir".q|img228.png"
 ALT="$ {\mbox{\boldmath $\Sigma$}}^{-1}$">|; 

1;

