# LaTeX2HTML 2002 (1.62)
# Associate internals original text with physical files.


$key = q/fig:regtree/;
$ref_files{$key} = "$dir".q|node133_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGCopy/;
$ref_files{$key} = "$dir".q|node376_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LSubset-Use/;
$ref_files{$key} = "$dir".q|node410_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:recnetlev/;
$ref_files{$key} = "$dir".q|node168_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mllr/;
$ref_files{$key} = "$dir".q|node126_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classnorm_ml/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slfsyntax/;
$ref_files{$key} = "$dir".q|node421_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMbinarylmformat/;
$ref_files{$key} = "$dir".q|node216_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.shell/;
$ref_files{$key} = "$dir".q|node43_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:wsandcs/;
$ref_files{$key} = "$dir".q|node207_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HDMan-Function/;
$ref_files{$key} = "$dir".q|node258_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:openvars/;
$ref_files{$key} = "$dir".q|node54_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:wordngrams/;
$ref_files{$key} = "$dir".q|node178_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:egsils/;
$ref_files{$key} = "$dir".q|node33_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HCompV/;
$ref_files{$key} = "$dir".q|node249_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:exampsys/;
$ref_files{$key} = "$dir".q|node24_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.decode/;
$ref_files{$key} = "$dir".q|node167_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HBuild-Function/;
$ref_files{$key} = "$dir".q|node246_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:Eupdate/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclustering/;
$ref_files{$key} = "$dir".q|node181_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:itemtree/;
$ref_files{$key} = "$dir".q|node146_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:Training/;
$ref_files{$key} = "$dir".q|node112_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:egdataprep/;
$ref_files{$key} = "$dir".q|node25_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HRest/;
$ref_files{$key} = "$dir".q|node340_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HInit-Function/;
$ref_files{$key} = "$dir".q|node305_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HCopy/;
$ref_files{$key} = "$dir".q|node253_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm3def/;
$ref_files{$key} = "$dir".q|node103_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HMMparm/;
$ref_files{$key} = "$dir".q|node102_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HQuant-Use/;
$ref_files{$key} = "$dir".q|node338_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:labstruct/;
$ref_files{$key} = "$dir".q|node88_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGCopy-Use/;
$ref_files{$key} = "$dir".q|node378_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:genio/;
$ref_files{$key} = "$dir".q|node56_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:errsum/;
$ref_files{$key} = "$dir".q|node417_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LAdapt/;
$ref_files{$key} = "$dir".q|node364_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Config/;
$ref_files{$key} = "$dir".q|node46_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMdatabaseprep/;
$ref_files{$key} = "$dir".q|node193_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:toolkit/;
$ref_files{$key} = "$dir".q|node14_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:mac6def/;
$ref_files{$key} = "$dir".q|node105_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LFoF/;
$ref_files{$key} = "$dir".q|node372_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse/;
$ref_files{$key} = "$dir".q|node329_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMperpproblems/;
$ref_files{$key} = "$dir".q|node205_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSGen-Tracing/;
$ref_files{$key} = "$dir".q|node351_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:ClassLM/;
$ref_files{$key} = "$dir".q|node191_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:openvcparmsLM/;
$ref_files{$key} = "$dir".q|node230_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:bwformulae/;
$ref_files{$key} = "$dir".q|node120_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mkCDHMMs/;
$ref_files{$key} = "$dir".q|node145_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMinterpmax/;
$ref_files{$key} = "$dir".q|node237_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HVite-Use/;
$ref_files{$key} = "$dir".q|node362_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:decorg/;
$ref_files{$key} = "$dir".q|node169_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMarpamitlm/;
$ref_files{$key} = "$dir".q|node214_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:tstrats/;
$ref_files{$key} = "$dir".q|node113_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGCopy-Function/;
$ref_files{$key} = "$dir".q|node377_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:mononet/;
$ref_files{$key} = "$dir".q|node165_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:aupdate1/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:aupdate2/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hlisteg/;
$ref_files{$key} = "$dir".q|node105_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:UseHCopy/;
$ref_files{$key} = "$dir".q|node84_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclassngram/;
$ref_files{$key} = "$dir".q|node180_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMdiscounts/;
$ref_files{$key} = "$dir".q|node184_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:VQUse/;
$ref_files{$key} = "$dir".q|node82_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMcompact/;
$ref_files{$key} = "$dir".q|node234_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.model/;
$ref_files{$key} = "$dir".q|node101_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMlanmodgen/;
$ref_files{$key} = "$dir".q|node195_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:Cluster/;
$ref_files{$key} = "$dir".q|node241_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:usehdman/;
$ref_files{$key} = "$dir".q|node164_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNorm-Use/;
$ref_files{$key} = "$dir".q|node402_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLStats-Tracing/;
$ref_files{$key} = "$dir".q|node328_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HHEd/;
$ref_files{$key} = "$dir".q|node265_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMlmfileformats/;
$ref_files{$key} = "$dir".q|node213_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:WordLM/;
$ref_files{$key} = "$dir".q|node190_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HVite-Tracing/;
$ref_files{$key} = "$dir".q|node363_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:plp/;
$ref_files{$key} = "$dir".q|node62_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse-Use/;
$ref_files{$key} = "$dir".q|node333_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/equiv_cond_prob_model/;
$ref_files{$key} = "$dir".q|node179_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMCopy-Tracing/;
$ref_files{$key} = "$dir".q|node319_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:labels/;
$ref_files{$key} = "$dir".q|node87_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPlex/;
$ref_files{$key} = "$dir".q|node404_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LAdapt-Tracing/;
$ref_files{$key} = "$dir".q|node367_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:autoco/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLRescore/;
$ref_files{$key} = "$dir".q|node320_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGList-Tracing/;
$ref_files{$key} = "$dir".q|node383_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclassprobslmformat/;
$ref_files{$key} = "$dir".q|node219_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSmooth-Use/;
$ref_files{$key} = "$dir".q|node358_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:dither/;
$ref_files{$key} = "$dir".q|node57_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LCMapTrace/;
$ref_files{$key} = "$dir".q|node223_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:subword/;
$ref_files{$key} = "$dir".q|node113_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:mtrans/;
$ref_files{$key} = "$dir".q|node128_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:HLMoperation/;
$ref_files{$key} = "$dir".q|node176_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/discounting_and_other_fun_things/;
$ref_files{$key} = "$dir".q|node184_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:nbest/;
$ref_files{$key} = "$dir".q|node174_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LFoF-Tracing/;
$ref_files{$key} = "$dir".q|node375_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMintegcheck/;
$ref_files{$key} = "$dir".q|node238_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:Adapt/;
$ref_files{$key} = "$dir".q|node125_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:othernets/;
$ref_files{$key} = "$dir".q|node166_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HCompV-Tracing/;
$ref_files{$key} = "$dir".q|node252_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:adapttrain/;
$ref_files{$key} = "$dir".q|node137_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:fileform/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMlibtracing/;
$ref_files{$key} = "$dir".q|node222_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:vtrellis/;
$ref_files{$key} = "$dir".q|node8_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:whatismllr/;
$ref_files{$key} = "$dir".q|node127_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HRest-Use/;
$ref_files{$key} = "$dir".q|node342_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMexchangealg/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:globbase/;
$ref_files{$key} = "$dir".q|node132_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LModelTrace/;
$ref_files{$key} = "$dir".q|node225_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:wdnet/;
$ref_files{$key} = "$dir".q|node159_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSLab/;
$ref_files{$key} = "$dir".q|node352_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclassModels/;
$ref_files{$key} = "$dir".q|node199_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HMMmac/;
$ref_files{$key} = "$dir".q|node104_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LSubset-Function/;
$ref_files{$key} = "$dir".q|node409_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSmooth-Tracing/;
$ref_files{$key} = "$dir".q|node359_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:waveform/;
$ref_files{$key} = "$dir".q|node68_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:recandvit/;
$ref_files{$key} = "$dir".q|node8_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HVite/;
$ref_files{$key} = "$dir".q|node360_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMuseintid/;
$ref_files{$key} = "$dir".q|node231_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LBuild-Function/;
$ref_files{$key} = "$dir".q|node369_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLStats-Function/;
$ref_files{$key} = "$dir".q|node325_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/ngramcountdiv/;
$ref_files{$key} = "$dir".q|node178_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPlex-Function/;
$ref_files{$key} = "$dir".q|node405_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPlex-Use/;
$ref_files{$key} = "$dir".q|node406_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse-Function/;
$ref_files{$key} = "$dir".q|node330_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cmllr/;
$ref_files{$key} = "$dir".q|node130_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:reg_classes/;
$ref_files{$key} = "$dir".q|node133_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:fbankanal/;
$ref_files{$key} = "$dir".q|node59_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:HTKFormat/;
$ref_files{$key} = "$dir".q|node66_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm4def/;
$ref_files{$key} = "$dir".q|node103_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGList/;
$ref_files{$key} = "$dir".q|node380_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSLab-Use/;
$ref_files{$key} = "$dir".q|node354_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSGen-Function/;
$ref_files{$key} = "$dir".q|node349_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:decop/;
$ref_files{$key} = "$dir".q|node168_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:dct/;
$ref_files{$key} = "$dir".q|node61_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:singlepass/;
$ref_files{$key} = "$dir".q|node118_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LMerge-Use/;
$ref_files{$key} = "$dir".q|node394_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:coninlib/;
$ref_files{$key} = "$dir".q|node413_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:tmfs/;
$ref_files{$key} = "$dir".q|node134_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:dischmm/;
$ref_files{$key} = "$dir".q|node110_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:consprec/;
$ref_files{$key} = "$dir".q|node9_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tiedstate/;
$ref_files{$key} = "$dir".q|node147_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMmemoryproblems/;
$ref_files{$key} = "$dir".q|node204_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HInit-Tracing/;
$ref_files{$key} = "$dir".q|node307_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/Csteptwo/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LBuild/;
$ref_files{$key} = "$dir".q|node368_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMCopy/;
$ref_files{$key} = "$dir".q|node316_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMgeneratingcount/;
$ref_files{$key} = "$dir".q|node197_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:vmllr/;
$ref_files{$key} = "$dir".q|node129_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:useforiso/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:delta/;
$ref_files{$key} = "$dir".q|node64_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HCompV-Use/;
$ref_files{$key} = "$dir".q|node251_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/chap2equalToOne/;
$ref_files{$key} = "$dir".q|node180_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclasscountslmformat/;
$ref_files{$key} = "$dir".q|node218_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMsmoothingprobs/;
$ref_files{$key} = "$dir".q|node187_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMwordmapproblems/;
$ref_files{$key} = "$dir".q|node203_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LMerge-Tracing/;
$ref_files{$key} = "$dir".q|node395_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSmooth/;
$ref_files{$key} = "$dir".q|node356_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPCalcTrace/;
$ref_files{$key} = "$dir".q|node226_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMfileproblems/;
$ref_files{$key} = "$dir".q|node201_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:Refine/;
$ref_files{$key} = "$dir".q|node143_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGCopy-Tracing/;
$ref_files{$key} = "$dir".q|node379_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HList/;
$ref_files{$key} = "$dir".q|node312_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:egreclive/;
$ref_files{$key} = "$dir".q|node40_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slfeg/;
$ref_files{$key} = "$dir".q|node423_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:alpha_quad/;
$ref_files{$key} = "$dir".q|node142_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mtransest/;
$ref_files{$key} = "$dir".q|node140_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cmaps/;
$ref_files{$key} = "$dir".q|node210_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:base_classes/;
$ref_files{$key} = "$dir".q|node132_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMconfigParms/;
$ref_files{$key} = "$dir".q|node230_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:ceplifter/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclasslmfileformats/;
$ref_files{$key} = "$dir".q|node217_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cmdline/;
$ref_files{$key} = "$dir".q|node44_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:decflow/;
$ref_files{$key} = "$dir".q|node169_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HVite-Function/;
$ref_files{$key} = "$dir".q|node361_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:dfhdrs/;
$ref_files{$key} = "$dir".q|node208_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLStats/;
$ref_files{$key} = "$dir".q|node324_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:parmstore/;
$ref_files{$key} = "$dir".q|node65_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LSubset-Tracing/;
$ref_files{$key} = "$dir".q|node411_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HERest-Function/;
$ref_files{$key} = "$dir".q|node262_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:mllrmeansol/;
$ref_files{$key} = "$dir".q|node140_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HBuild-Use/;
$ref_files{$key} = "$dir".q|node247_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse-Network_Definition/;
$ref_files{$key} = "$dir".q|node331_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:logenergy/;
$ref_files{$key} = "$dir".q|node63_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mllrformulae/;
$ref_files{$key} = "$dir".q|node139_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HList-Function/;
$ref_files{$key} = "$dir".q|node313_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:toolkit/;
$ref_files{$key} = "$dir".q|node11_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:mtrans2/;
$ref_files{$key} = "$dir".q|node130_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMmapoov/;
$ref_files{$key} = "$dir".q|node194_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hinitdp/;
$ref_files{$key} = "$dir".q|node114_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:cdpdf/;
$ref_files{$key} = "$dir".q|node102_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:netforcsr/;
$ref_files{$key} = "$dir".q|node9_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tmixeg/;
$ref_files{$key} = "$dir".q|node106_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:lpcanal/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:OneHMM/;
$ref_files{$key} = "$dir".q|node103_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:qstree/;
$ref_files{$key} = "$dir".q|node148_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:byteswap/;
$ref_files{$key} = "$dir".q|node53_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLRescore-Function/;
$ref_files{$key} = "$dir".q|node321_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:UseHList/;
$ref_files{$key} = "$dir".q|node83_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:htkslf/;
$ref_files{$key} = "$dir".q|node418_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:egrectest/;
$ref_files{$key} = "$dir".q|node38_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/ngram_model/;
$ref_files{$key} = "$dir".q|node178_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HERest-Tracing/;
$ref_files{$key} = "$dir".q|node264_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMprobshort/;
$ref_files{$key} = "$dir".q|node235_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:xwrdnet/;
$ref_files{$key} = "$dir".q|node165_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HQuant-Tracing/;
$ref_files{$key} = "$dir".q|node339_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:tmix/;
$ref_files{$key} = "$dir".q|node106_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:conintools/;
$ref_files{$key} = "$dir".q|node414_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMrobustestimates/;
$ref_files{$key} = "$dir".q|node183_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HBuild/;
$ref_files{$key} = "$dir".q|node245_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:speechio/;
$ref_files{$key} = "$dir".q|node55_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:hlmtutor/;
$ref_files{$key} = "$dir".q|node192_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cmllrest/;
$ref_files{$key} = "$dir".q|node142_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slffields/;
$ref_files{$key} = "$dir".q|node422_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mkngoview/;
$ref_files{$key} = "$dir".q|node190_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/fig:hiermllr/;
$ref_files{$key} = "$dir".q|node135_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMlmidshort/;
$ref_files{$key} = "$dir".q|node233_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:digitnets/;
$ref_files{$key} = "$dir".q|node160_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LLink/;
$ref_files{$key} = "$dir".q|node388_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMmodelinterp/;
$ref_files{$key} = "$dir".q|node198_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/2-3/;
$ref_files{$key} = "$dir".q|node189_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/2-4/;
$ref_files{$key} = "$dir".q|node189_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:deltas/;
$ref_files{$key} = "$dir".q|node64_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:tbclust/;
$ref_files{$key} = "$dir".q|node148_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LMerge/;
$ref_files{$key} = "$dir".q|node392_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLStats-Use/;
$ref_files{$key} = "$dir".q|node327_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:markovgen/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:flatstart/;
$ref_files{$key} = "$dir".q|node115_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:lpcepstra/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGPrep-Function/;
$ref_files{$key} = "$dir".q|node385_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LUtilTrace/;
$ref_files{$key} = "$dir".q|node228_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGPrep/;
$ref_files{$key} = "$dir".q|node384_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HQuant-VQ_Codebook_Format/;
$ref_files{$key} = "$dir".q|node337_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:urjt/;
$ref_files{$key} = "$dir".q|node123_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:vquant/;
$ref_files{$key} = "$dir".q|node82_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step1/;
$ref_files{$key} = "$dir".q|node26_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step2/;
$ref_files{$key} = "$dir".q|node27_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step3/;
$ref_files{$key} = "$dir".q|node29_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:confvars/;
$ref_files{$key} = "$dir".q|node412_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step4/;
$ref_files{$key} = "$dir".q|node29_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HQuant-Function/;
$ref_files{$key} = "$dir".q|node336_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPMergeTrace/;
$ref_files{$key} = "$dir".q|node227_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step5/;
$ref_files{$key} = "$dir".q|node30_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step6/;
$ref_files{$key} = "$dir".q|node32_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step7/;
$ref_files{$key} = "$dir".q|node33_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step8/;
$ref_files{$key} = "$dir".q|node34_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HQuant/;
$ref_files{$key} = "$dir".q|node335_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step9/;
$ref_files{$key} = "$dir".q|node36_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HResults-Tracing/;
$ref_files{$key} = "$dir".q|node347_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cllmoview/;
$ref_files{$key} = "$dir".q|node191_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm5def/;
$ref_files{$key} = "$dir".q|node104_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:restloop/;
$ref_files{$key} = "$dir".q|node116_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/KN-clustering/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:openvsum/;
$ref_files{$key} = "$dir".q|node54_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.netdict/;
$ref_files{$key} = "$dir".q|node157_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HDMan/;
$ref_files{$key} = "$dir".q|node257_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:netexpand/;
$ref_files{$key} = "$dir".q|node165_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:sigjsm/;
$ref_files{$key} = "$dir".q|node123_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGPrep-Use/;
$ref_files{$key} = "$dir".q|node386_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:speechvq/;
$ref_files{$key} = "$dir".q|node154_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMperplexity/;
$ref_files{$key} = "$dir".q|node189_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:labelcparms/;
$ref_files{$key} = "$dir".q|node100_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:errors/;
$ref_files{$key} = "$dir".q|node415_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:whattransform/;
$ref_files{$key} = "$dir".q|node131_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:allpole/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:toolref/;
$ref_files{$key} = "$dir".q|node240_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSLab-Tracing/;
$ref_files{$key} = "$dir".q|node355_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classnorm_logprob/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNorm-Function/;
$ref_files{$key} = "$dir".q|node401_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMproblemSolving/;
$ref_files{$key} = "$dir".q|node200_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/Cstepone/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HBuild-Tracing/;
$ref_files{$key} = "$dir".q|node248_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNewMap-Tracing/;
$ref_files{$key} = "$dir".q|node399_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:wdnet1/;
$ref_files{$key} = "$dir".q|node159_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/normclass/;
$ref_files{$key} = "$dir".q|node180_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.disc/;
$ref_files{$key} = "$dir".q|node152_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HERest/;
$ref_files{$key} = "$dir".q|node261_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HInit-Use/;
$ref_files{$key} = "$dir".q|node306_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HDMan-Use/;
$ref_files{$key} = "$dir".q|node259_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mapadapt/;
$ref_files{$key} = "$dir".q|node138_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:exsyssum/;
$ref_files{$key} = "$dir".q|node41_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:egtranstie/;
$ref_files{$key} = "$dir".q|node36_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNorm/;
$ref_files{$key} = "$dir".q|node400_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:HMMDefs/;
$ref_files{$key} = "$dir".q|node101_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LFoF-Function/;
$ref_files{$key} = "$dir".q|node373_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:spk_adapt/;
$ref_files{$key} = "$dir".q|node10_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Spmods/;
$ref_files{$key} = "$dir".q|node56_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:decode/;
$ref_files{$key} = "$dir".q|node167_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:vtransest/;
$ref_files{$key} = "$dir".q|node141_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:isowrdrec/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:vtln/;
$ref_files{$key} = "$dir".q|node60_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLRescore-Tracing/;
$ref_files{$key} = "$dir".q|node323_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LLink-Tracing/;
$ref_files{$key} = "$dir".q|node391_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:messencode/;
$ref_files{$key} = "$dir".q|node4_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/fg_stats/;
$ref_files{$key} = "$dir".q|node215_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:hlmfiles/;
$ref_files{$key} = "$dir".q|node206_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HRest-Tracing/;
$ref_files{$key} = "$dir".q|node343_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LAdapt-Use/;
$ref_files{$key} = "$dir".q|node366_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:labform/;
$ref_files{$key} = "$dir".q|node89_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:upmix/;
$ref_files{$key} = "$dir".q|node149_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLEd/;
$ref_files{$key} = "$dir".q|node308_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:regtree1/;
$ref_files{$key} = "$dir".q|node133_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:memman/;
$ref_files{$key} = "$dir".q|node51_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HResults-Use/;
$ref_files{$key} = "$dir".q|node346_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:lintran/;
$ref_files{$key} = "$dir".q|node108_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/robust_estimation/;
$ref_files{$key} = "$dir".q|node183_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclasslmformat/;
$ref_files{$key} = "$dir".q|node220_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HList-Use/;
$ref_files{$key} = "$dir".q|node314_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:streams/;
$ref_files{$key} = "$dir".q|node81_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classngram-description/;
$ref_files{$key} = "$dir".q|node180_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:isoprob/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:usehsgen/;
$ref_files{$key} = "$dir".q|node163_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:softarch/;
$ref_files{$key} = "$dir".q|node12_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:ham/;
$ref_files{$key} = "$dir".q|node57_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:teemods/;
$ref_files{$key} = "$dir".q|node109_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:falign/;
$ref_files{$key} = "$dir".q|node172_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slffiles/;
$ref_files{$key} = "$dir".q|node419_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:v1spcompat/;
$ref_files{$key} = "$dir".q|node85_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HERest-Use/;
$ref_files{$key} = "$dir".q|node263_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:FoFs/;
$ref_files{$key} = "$dir".q|node212_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMctconfigParms/;
$ref_files{$key} = "$dir".q|node232_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classnorm_breakdown/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:MMFeg/;
$ref_files{$key} = "$dir".q|node32_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:dialnet/;
$ref_files{$key} = "$dir".q|node26_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HCompV-Function/;
$ref_files{$key} = "$dir".q|node250_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Blocking/;
$ref_files{$key} = "$dir".q|node57_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:covtrans/;
$ref_files{$key} = "$dir".q|node129_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:bobig/;
$ref_files{$key} = "$dir".q|node162_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:twomodel/;
$ref_files{$key} = "$dir".q|node119_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:vqtohmm/;
$ref_files{$key} = "$dir".q|node154_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tmixhmm/;
$ref_files{$key} = "$dir".q|node109_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:recipe/;
$ref_files{$key} = "$dir".q|node24_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:decompmtrans2/;
$ref_files{$key} = "$dir".q|node130_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMCopy-Use/;
$ref_files{$key} = "$dir".q|node318_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:usehparse/;
$ref_files{$key} = "$dir".q|node160_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:Cluster-Function/;
$ref_files{$key} = "$dir".q|node242_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:dbhier/;
$ref_files{$key} = "$dir".q|node98_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:whatsnew/;
$ref_files{$key} = "$dir".q|node19_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm6def/;
$ref_files{$key} = "$dir".q|node105_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:10/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:11/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:12/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:13/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:14/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:15/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:audioio/;
$ref_files{$key} = "$dir".q|node80_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:16/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:resthmm/;
$ref_files{$key} = "$dir".q|node116_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:labegs/;
$ref_files{$key} = "$dir".q|node95_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMCopy-Function/;
$ref_files{$key} = "$dir".q|node317_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSGen-Use/;
$ref_files{$key} = "$dir".q|node350_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:receval/;
$ref_files{$key} = "$dir".q|node171_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMmodifiedarpamitlm/;
$ref_files{$key} = "$dir".q|node215_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNewMap/;
$ref_files{$key} = "$dir".q|node396_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/cond_prob_model/;
$ref_files{$key} = "$dir".q|node177_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:sigproc/;
$ref_files{$key} = "$dir".q|node57_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/smoothing_probs/;
$ref_files{$key} = "$dir".q|node187_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:labelsum/;
$ref_files{$key} = "$dir".q|node100_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:wmaps/;
$ref_files{$key} = "$dir".q|node209_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:hmmsets/;
$ref_files{$key} = "$dir".q|node105_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:ddpdf/;
$ref_files{$key} = "$dir".q|node102_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:openviron/;
$ref_files{$key} = "$dir".q|node43_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:gi_mllr/;
$ref_files{$key} = "$dir".q|node140_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGPrep-Tracing/;
$ref_files{$key} = "$dir".q|node387_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:21/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:25/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:27/;
$ref_files{$key} = "$dir".q|node8_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLStats-Bigram_Generation/;
$ref_files{$key} = "$dir".q|node326_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:generr/;
$ref_files{$key} = "$dir".q|node416_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm1def/;
$ref_files{$key} = "$dir".q|node103_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:meanmap/;
$ref_files{$key} = "$dir".q|node138_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:gi_mllr2/;
$ref_files{$key} = "$dir".q|node141_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/pp_sum_eqn/;
$ref_files{$key} = "$dir".q|node189_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:gi_mllr3/;
$ref_files{$key} = "$dir".q|node142_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:gi_mllr4/;
$ref_files{$key} = "$dir".q|node142_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slfintro/;
$ref_files{$key} = "$dir".q|node159_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:genprops/;
$ref_files{$key} = "$dir".q|node13_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HDMan-Tracing/;
$ref_files{$key} = "$dir".q|node260_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:recaudio/;
$ref_files{$key} = "$dir".q|node173_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.labs/;
$ref_files{$key} = "$dir".q|node87_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:gramfs/;
$ref_files{$key} = "$dir".q|node211_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:30/;
$ref_files{$key} = "$dir".q|node8_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.spio/;
$ref_files{$key} = "$dir".q|node55_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:subsmixrep/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:egcreattri/;
$ref_files{$key} = "$dir".q|node35_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/clustering_section/;
$ref_files{$key} = "$dir".q|node181_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSLab-Function/;
$ref_files{$key} = "$dir".q|node353_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:netuse/;
$ref_files{$key} = "$dir".q|node158_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classnorm_Fml/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:stdopts/;
$ref_files{$key} = "$dir".q|node47_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:isoword/;
$ref_files{$key} = "$dir".q|node113_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:tiedmix/;
$ref_files{$key} = "$dir".q|node155_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:spiosum/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:egcreatmono/;
$ref_files{$key} = "$dir".q|node31_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:dpscale/;
$ref_files{$key} = "$dir".q|node107_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:ddclust/;
$ref_files{$key} = "$dir".q|node147_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:config/;
$ref_files{$key} = "$dir".q|node46_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HRest-Function/;
$ref_files{$key} = "$dir".q|node341_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:biglms/;
$ref_files{$key} = "$dir".q|node161_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMtestingpp/;
$ref_files{$key} = "$dir".q|node196_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:decinet/;
$ref_files{$key} = "$dir".q|node162_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:2/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:energy/;
$ref_files{$key} = "$dir".q|node63_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:3/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:4/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:5/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:6/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:7/;
$ref_files{$key} = "$dir".q|node5_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:slfformat/;
$ref_files{$key} = "$dir".q|node420_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:binsave/;
$ref_files{$key} = "$dir".q|node110_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:8/;
$ref_files{$key} = "$dir".q|node6_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hierarch/;
$ref_files{$key} = "$dir".q|node104_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.hedit/;
$ref_files{$key} = "$dir".q|node143_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:inithmm/;
$ref_files{$key} = "$dir".q|node114_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:edlab/;
$ref_files{$key} = "$dir".q|node99_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step10/;
$ref_files{$key} = "$dir".q|node37_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:Tool.train/;
$ref_files{$key} = "$dir".q|node112_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:step11/;
$ref_files{$key} = "$dir".q|node39_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:fundaments/;
$ref_files{$key} = "$dir".q|node3_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:wlroper/;
$ref_files{$key} = "$dir".q|node9_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse-Compatibility_Mode/;
$ref_files{$key} = "$dir".q|node332_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:melscale/;
$ref_files{$key} = "$dir".q|node59_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:vtlnpiecewise/;
$ref_files{$key} = "$dir".q|node60_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LFoF-Use/;
$ref_files{$key} = "$dir".q|node374_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:overview/;
$ref_files{$key} = "$dir".q|node3_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:Function/;
$ref_files{$key} = "$dir".q|node397_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tmixpool/;
$ref_files{$key} = "$dir".q|node106_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:discseq/;
$ref_files{$key} = "$dir".q|node153_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:lintran/;
$ref_files{$key} = "$dir".q|node108_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:genpHMM/;
$ref_files{$key} = "$dir".q|node4_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLEd-Tracing/;
$ref_files{$key} = "$dir".q|node311_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSmooth-Function/;
$ref_files{$key} = "$dir".q|node357_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hsetdef/;
$ref_files{$key} = "$dir".q|node105_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HResults/;
$ref_files{$key} = "$dir".q|node344_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:bwrest/;
$ref_files{$key} = "$dir".q|node7_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:eresthmm/;
$ref_files{$key} = "$dir".q|node117_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mlfs/;
$ref_files{$key} = "$dir".q|node94_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:streams/;
$ref_files{$key} = "$dir".q|node81_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:ngramLMs/;
$ref_files{$key} = "$dir".q|node177_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:preemp/;
$ref_files{$key} = "$dir".q|node57_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGList-Function/;
$ref_files{$key} = "$dir".q|node381_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:parher/;
$ref_files{$key} = "$dir".q|node117_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:script/;
$ref_files{$key} = "$dir".q|node45_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:Cluster-Tracing/;
$ref_files{$key} = "$dir".q|node244_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLEd-Use/;
$ref_files{$key} = "$dir".q|node310_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:erep/;
$ref_files{$key} = "$dir".q|node48_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:misedit/;
$ref_files{$key} = "$dir".q|node151_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:herestdp/;
$ref_files{$key} = "$dir".q|node117_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:outprobspec/;
$ref_files{$key} = "$dir".q|node6_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LWMapTrace/;
$ref_files{$key} = "$dir".q|node229_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:qualifiers/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:validcons/;
$ref_files{$key} = "$dir".q|node255_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:wintnet/;
$ref_files{$key} = "$dir".q|node165_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:stdopts/;
$ref_files{$key} = "$dir".q|node54_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HResults-Function/;
$ref_files{$key} = "$dir".q|node345_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:macregtreedef/;
$ref_files{$key} = "$dir".q|node110_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:parmkinds/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LLink-Use/;
$ref_files{$key} = "$dir".q|node390_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm2def/;
$ref_files{$key} = "$dir".q|node103_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:openvcparms/;
$ref_files{$key} = "$dir".q|node54_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGList-Use/;
$ref_files{$key} = "$dir".q|node382_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:netdict/;
$ref_files{$key} = "$dir".q|node157_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HInit/;
$ref_files{$key} = "$dir".q|node304_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:iopipes/;
$ref_files{$key} = "$dir".q|node52_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMequivalenceclasses/;
$ref_files{$key} = "$dir".q|node179_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HSGen/;
$ref_files{$key} = "$dir".q|node348_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:hvrec/;
$ref_files{$key} = "$dir".q|node170_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMclassSinglmformat/;
$ref_files{$key} = "$dir".q|node221_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/equiv_classes/;
$ref_files{$key} = "$dir".q|node180_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:streamadapt/;
$ref_files{$key} = "$dir".q|node136_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:nistfom/;
$ref_files{$key} = "$dir".q|node171_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:mac5def/;
$ref_files{$key} = "$dir".q|node104_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:hlmfund/;
$ref_files{$key} = "$dir".q|node176_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:cepstrum/;
$ref_files{$key} = "$dir".q|node61_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:mumllr/;
$ref_files{$key} = "$dir".q|node128_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:hieradapt/;
$ref_files{$key} = "$dir".q|node135_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:parmtying/;
$ref_files{$key} = "$dir".q|node146_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LSubset/;
$ref_files{$key} = "$dir".q|node408_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:/;
$ref_files{$key} = "$dir".q|node20_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:usingHHEd/;
$ref_files{$key} = "$dir".q|node144_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:Cluster-Use/;
$ref_files{$key} = "$dir".q|node243_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:simdiffs/;
$ref_files{$key} = "$dir".q|node64_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LMerge-Function/;
$ref_files{$key} = "$dir".q|node393_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/classnorm_totprob/;
$ref_files{$key} = "$dir".q|node182_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:htkstrings/;
$ref_files{$key} = "$dir".q|node49_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:gnorm/;
$ref_files{$key} = "$dir".q|node102_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:recsys/;
$ref_files{$key} = "$dir".q|node158_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LBuild-Tracing/;
$ref_files{$key} = "$dir".q|node371_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HParse-Tracing/;
$ref_files{$key} = "$dir".q|node334_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:decompmtrans/;
$ref_files{$key} = "$dir".q|node128_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:usehbuild/;
$ref_files{$key} = "$dir".q|node162_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:coercions/;
$ref_files{$key} = "$dir".q|node84_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:LTool/;
$ref_files{$key} = "$dir".q|node206_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:spiocparms1/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/t:spiocparms2/;
$ref_files{$key} = "$dir".q|node86_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:discmods/;
$ref_files{$key} = "$dir".q|node152_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:headapt/;
$ref_files{$key} = "$dir".q|node125_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tsubword/;
$ref_files{$key} = "$dir".q|node16_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLMsyntaxproblems/;
$ref_files{$key} = "$dir".q|node202_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:kupdate1/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:kupdate2/;
$ref_files{$key} = "$dir".q|node58_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/c:htkoview/;
$ref_files{$key} = "$dir".q|node11_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:flatst/;
$ref_files{$key} = "$dir".q|node115_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:tmixpdf/;
$ref_files{$key} = "$dir".q|node106_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:tmixhmm2/;
$ref_files{$key} = "$dir".q|node107_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:hhedregtree/;
$ref_files{$key} = "$dir".q|node150_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:endpointer/;
$ref_files{$key} = "$dir".q|node80_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:hmmdef/;
$ref_files{$key} = "$dir".q|node111_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hmm1/;
$ref_files{$key} = "$dir".q|node102_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:melfbank/;
$ref_files{$key} = "$dir".q|node59_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNorm-Tracing/;
$ref_files{$key} = "$dir".q|node403_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hslab/;
$ref_files{$key} = "$dir".q|node353_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LNewMap-Use/;
$ref_files{$key} = "$dir".q|node398_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:dmaker/;
$ref_files{$key} = "$dir".q|node164_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:softarch/;
$ref_files{$key} = "$dir".q|node12_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLEd-Function/;
$ref_files{$key} = "$dir".q|node309_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:vitloop/;
$ref_files{$key} = "$dir".q|node114_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:hvalign/;
$ref_files{$key} = "$dir".q|node172_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LPlex-Tracing/;
$ref_files{$key} = "$dir".q|node407_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LBuild-Use/;
$ref_files{$key} = "$dir".q|node370_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HList-Tracing/;
$ref_files{$key} = "$dir".q|node315_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:HLRescore-Use/;
$ref_files{$key} = "$dir".q|node322_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:dischmm/;
$ref_files{$key} = "$dir".q|node107_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:psmooth/;
$ref_files{$key} = "$dir".q|node156_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/s:LGBaseTrace/;
$ref_files{$key} = "$dir".q|node224_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/e:covlike/;
$ref_files{$key} = "$dir".q|node129_ct.html|; 
$noresave{$key} = "$nosave";

$key = q/f:sysoview/;
$ref_files{$key} = "$dir".q|node16_ct.html|; 
$noresave{$key} = "$nosave";

1;

