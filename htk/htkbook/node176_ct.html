<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<!--Converted with jLaTeX2HTML 2002 (1.62) JA patch-1.4
patched version by:  Kenshi Muto, Debian Project.
LaTeX2HTML 2002 (1.62),
original version by:  Nikos Drakos, CBLU, University of Leeds
* revised and updated by:  Marcus Hennecke, Ross Moore, Herb Swan
* with significant contributions from:
  Jens Lippmann, Marek Rouchal, Martin Wilck and others -->
<HTML>
<HEAD>
<TITLE>Contents of Fundamentals of language modelling</TITLE>
<META NAME="description" CONTENT="Contents of Fundamentals of language modelling">
<META NAME="keywords" CONTENT="htkbook">
<META NAME="resource-type" CONTENT="document">
<META NAME="distribution" CONTENT="global">

<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
<META NAME="Generator" CONTENT="jLaTeX2HTML v2002 JA patch-1.4">
<META HTTP-EQUIV="Content-Style-Type" CONTENT="text/css">

<LINK REL="STYLESHEET" HREF="htkbook.css">

<LINK REL="next" HREF="node192_mn.html">
<LINK REL="previous" HREF="node175_mn.html">
<LINK REL="up" HREF="node175_mn.html">
<LINK REL="next" HREF="node177_mn.html">
</HEAD>
 
<BODY bgcolor="#ffffff" text="#000000" link="#9944EE" vlink="#0000ff" alink="#00ff00">

<H1><A NAME="SECTION04100000000000000000">&#160;</A><A NAME="c:hlmfund">&#160;</A>
<BR>
Fundamentals of language modelling
</H1>

<P>
The HTK language modelling tools are designed for constructing and
testing statistical <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram language models.  This chapter introduces
language modelling and provides an overview of the supplied tools.  It
is strongly recommended that you read this chapter and then work
through the tutorial in the following chapter - this will provide you
with everything you need to know to get started building language models.

<P>

<P>
<DIV ALIGN="CENTER">
<A NAME="f:HLMoperation">&#160;</A><IMG
 WIDTH="366" HEIGHT="409" ALIGN="MIDDLE" BORDER="0"
 SRC="img486.png"
 ALT="$\textstyle \parbox{80mm}{ \begin{center}\setlength{\epsfxsize}{80mm}
\epsfbox{HTKFigs//HLMoperation.eps}
\end{center} }$">
</DIV>

<P>
An <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram is a sequence of <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN> symbols (e.g. words, syntactic
categories, etc) and an <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram language model (LM) is used to
predict each symbol in the sequence given its <SPAN CLASS="MATH"><IMG
 WIDTH="41" HEIGHT="28" ALIGN="MIDDLE" BORDER="0"
 SRC="img212.png"
 ALT="$ n-1$"></SPAN> predecessors.  It
is built on the assumption that the probability of a specific <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram
occurring in some unknown test text can be estimated from the
frequency of its occurrence in some given training text.  Thus, as
illustrated by the picture above, <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram construction is a three stage
process.  Firstly, the training text is scanned and its <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-grams are
counted and stored in a database of <SPAN  CLASS="textit">gram</SPAN> files.  In the
second stage some words may be mapped to an out of vocabulary class or
other class mapping may be applied, and then in the final stage
the counts in the resulting gram files are used to compute
<SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram probabalities which are stored in the <SPAN  CLASS="textit">language model</SPAN>
file.  Lastly, the <SPAN  CLASS="textit">goodness</SPAN> of a language model can be
estimated by using it to compute a measure called <SPAN  CLASS="textit">perplexity</SPAN>
on a previously unseen test set.  In general, the better a language model
then the lower its test-set perplexity.

<P>
Although the basic principle of an <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram LM is very simple, in
practice there are usually many more potential <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-grams than can ever
be collected in a training text in sufficient numbers to yield robust
frequency estimates.  Furthermore, for any real application such as
speech recognition, the use of an essentially static and finite
training text makes it difficult to generate a single LM which is
well-matched to varying test material. For example, an LM trained on
newspaper text would be a good predictor for dictating news
reports but the same LM would be a poor predictor for personal letters
or a spoken interface to a flight reservation system.  A final
difficulty is that the <SPAN  CLASS="textit">vocabulary</SPAN> of an <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram LM is finite
and fixed at construction time.  Thus, if the LM is word-based, it can
only predict words within its vocabulary and furthermore new words
cannot be added without rebuilding the LM.

<P>
The following four sections provide a thorough introduction to the
theory behind <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram models.  It is well worth reading through this
section because it will provide you with at least a basic
understanding of what many of the tools and their parameters actually
do - you can safely skip the equations if you choose because the text
explains all the most important parts in plain English.  The final
section of this chapter then introduces the tools provided to
implement the various aspects of <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-gram language modelling that have
been described.

<P>
<BR><HR>
<!--Table of Child-Links-->
<A NAME="CHILD_LINKS"><STRONG>Subsections</STRONG></A>

<UL CLASS="ChildLinks">
<LI><A NAME="tex2html3721" HREF="node177_mn.html" TARGET="main"><SMALL><I>n</I>-gram language models</SMALL></A>
<UL>
<LI><A NAME="tex2html3722" HREF="node178_mn.html" TARGET="main"><SMALL>Word <I>n</I>-gram models</SMALL></A>
<LI><A NAME="tex2html3723" HREF="node179_mn.html" TARGET="main"><SMALL>Equivalence classes</SMALL></A>
<LI><A NAME="tex2html3724" HREF="node180_mn.html" TARGET="main"><SMALL>Class <I>n</I>-gram models</SMALL></A>
</UL>
<BR>
<LI><A NAME="tex2html3725" HREF="node181_mn.html" TARGET="main"><SMALL>Statistically-derived Class Maps</SMALL></A>
<UL>
<LI><A NAME="tex2html3726" HREF="node182_mn.html" TARGET="main"><SMALL>Word exchange algorithm</SMALL></A>
</UL>
<BR>
<LI><A NAME="tex2html3727" HREF="node183_mn.html" TARGET="main"><SMALL>Robust model estimation</SMALL></A>
<UL>
<LI><A NAME="tex2html3728" HREF="node184_mn.html" TARGET="main"><SMALL>Estimating probabilities</SMALL></A>
<UL>
<LI><A NAME="tex2html3729" HREF="node185_mn.html" TARGET="main"><SMALL>Good-Turing discounting</SMALL></A>
<LI><A NAME="tex2html3730" HREF="node186_mn.html" TARGET="main"><SMALL>Absolute discounting</SMALL></A>
</UL>
<LI><A NAME="tex2html3731" HREF="node187_mn.html" TARGET="main"><SMALL>Smoothing probabilities</SMALL></A>
<UL>
<LI><A NAME="tex2html3732" HREF="node188_mn.html" TARGET="main"><SMALL>Cut-offs</SMALL></A>
</UL>
</UL>
<BR>
<LI><A NAME="tex2html3733" HREF="node189_mn.html" TARGET="main"><SMALL>Perplexity</SMALL></A>
<LI><A NAME="tex2html3734" HREF="node190_mn.html" TARGET="main"><SMALL>Overview of <SPAN CLASS="MATH"><IMG
 WIDTH="14" HEIGHT="14" ALIGN="BOTTOM" BORDER="0"
 SRC="img1.png"
 ALT="$ n$"></SPAN>-Gram Construction Process</SMALL></A>
<LI><A NAME="tex2html3735" HREF="node191_mn.html" TARGET="main"><SMALL>Class-Based Language Models</SMALL></A>
</UL>
<!--End of Table of Child-Links-->

<HR>
<ADDRESS>
<A HREF=http://htk.eng.cam.ac.uk/docs/docs.shtml TARGET=_top>Back to HTK site</A><BR>See front page for HTK Authors
</ADDRESS>
</BODY>
</HTML>
