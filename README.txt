                                    README file
                                    ===========
==================================
= Jan Alexandru VADUVA           =
= CIBLF - AAC                    =
= vaduva.jan.alexandru@gmail.com =
==================================


Comenzi
=======
    wave        --- 444440
    grab        --- 555552
    soft grab   --- 333331
    strong grab --- 999994
    point       --- 909990
    open        --- 000000
    close       --- 777770

Console commands
================
    $> padsp julius -input mic -C julian.jconf  (for linux)
    $> julius -input mic -C julian.jconf        (for raspberry pi)

    $> sudo apt-get install python-serial

	$> wget http://pexpect.sourceforge.net/pexpect-2.3.tar.gz
	$> tar xzf pexpect-2.3.tar.gz
	$> cd pexpect-2.3
	$> sudo python ./setup.py install

Raspberry pi setup
==================
    $> sudo modprobe snd_bcm2835
    $> sudo apt-get install alsa-tools alsa-oss flex zlib1g-dev libc-bin libc-dev-bin python-pexpect libasound2
    libasound2-dev cvs

    $> arecord -d 10 -D plughw:1,0 test.wav
    $> aplay test.wav

    $> cvs -z3 -d:pserver:anonymous@cvs.sourceforge.jp:/cvsroot/julius co julius4
    $> export CFLAGS="-O2 -mcpu=arm1176jzf-s -mfpu=vfp -mfloat-abi=hard -pipe -fomit-frame-pointer"
    $> cd julius4
    $> ./configure --with-mictype=alsa
    $> make
    $> sudo make install

    $> cd htk
    $> ./configure --without-x --disable-hslab
    $> make all
    $> sudo make install

    $> export ALSADEV="plughw:1,0"
    $> julius -input mic -C julian.jconf

Requirements
============
    1. Robotic hand
    2. Computer running Linux
    3. Microphone
    4. Hidden Markov Model tool kit (HTK) from Cambridge university
    5. Julius speech recognition decoder developed by CSRC, Japan
    6. Recording software (Audacity, Sound Recorder, etc.)
    7. Libusb version 1.0
    8. A quiet environment to record voice samples

Linux setup
===========
Useful link: http://www.voxforge.org/home/dev/acousticmodels/linux/create/htkjulius/how-to/download
    1. Creating a vocabulary and grammar
    2. Creating a grammar
    3. Training the Model 
    4. Putting everything together
    5. Write the program
    6. Obtaining the output
    7. Robotic hand wifi protocol
    8. Finishing up

    Extended:     
        a. place sample.grammar, sample.voca, prompts in voxforge/auto
        b. put all the wav files (sample1.wav ....etc) into voxforge/auto/train/wav
        c. unzip the Hidden Markov Model tool kit (the HTK_samples archive) and put the following HTK scripts into voxforge/HTK_scripts:
             maketrihed
             mkclscript.prl (from /samples/RMHTK/perl_scripts/)
             prompts2mlf
             prompts2wlist
        d. put voxforge_lexicon into voxforge/lexicon
        e. mkdfa sample
        f. in the voxforge/auto folder, create a file called codetrain.scp and fill it with:
             ../train/wav/sample1.wav ../train/mfcc/sample1.mfc
             ../train/wav/sample2.wav ../train/mfcc/sample2.mfc
             ../train/wav/sample3.wav ../train/mfcc/sample3.mfc
             ../train/wav/sample4.wav ../train/mfcc/sample4.mfc
             ../train/wav/sample5.wav ../train/mfcc/sample5.mfc
             ....
        g. ./HTK_Compile_Model.sh
        h. julius -input mic -C julian.jconf
        i. python control script 
                                    
