#!/usr/bin/python

import pexpect
import serial
import re


armArray= {'GRAB': '555552', 'SOFT GRAB': '333331', 'STRONG GRAB': '999994', 'OPEN': '000000', 'CLOSE': '777770',
'WAVE': '444440', 'POINT': '909990'}

def movearm(action):
	if action != '444440':
	#	serialport = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)
	#	serialport.write(str(action))
	#	response = serialport.readlines(None)
		print action
	else:
	#	serialport = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)
	#	serialport.write(str(action))
	#	serialport.write(str('000000'))
	#	serialport.write(str(action))
	#	serialport.write(str('000000'))
	#	serialport.write(str(action))
	#	serialport.write(str('000000'))
	#	response = serialport.readlines(None)
	#print response
		print action
		print '000000'
		print action
		print '000000'
		print action
		print '000000'

def remove_tags(data):
	p = re.compile(r'<[^<]*?>')
	return p.sub('', data)

def process_sentence(sentence_text):
	str1 = remove_tags(sentence_text)
	str2 = str1.replace('sentence1: ','')
	joints = ['GRAB', 'SOFT', 'STRONG', 'OPEN', 'CLOSE', 'WAVE', 'POINT']
	transcribed = str2.split()
	if transcribed[0] == "SOFT":
		movearm(333331)
	elif transcribed[0] == "STRONG":
		movearm(999994)
	elif transcribed[0] in joints:
		movearm(armArray[transcribed[0]])

def get_confidence(out_text):
	linearray = out_text.split("\n")
	for line in linearray:
		if line.find('sentence1') != -1:
			sentence1 = line
		elif line.find('cmscore1') != -1:
			cmscore1 = line
		elif line.find('score1') != -1:
			score1 = line
	cmscore_array = cmscore1.split()
	err_flag = False
	for score in cmscore_array:
		try:
			ns = float(score)
		except ValueError:
			continue
		if (ns < 0.999):
			err_flag = True
			print "confidence error:", ns, ":", sentence1
	score1_val = float(score1.split()[1])
	if score1_val < -13000:
		err_flag = True
		print "score1 error:", score1_val, sentence1
	if (not err_flag):
		print sentence1
		print score1

		process_sentence(sentence1)
	else:
		pass


def process_julius(out_text):
	match_res = re.match(r'(.*)sentence1(\.*)', out_text, re.S)
	if match_res:
		get_confidence(out_text)
	else:
		pass


if __name__ == "__main__":
	child = pexpect.spawn ('padsp julius -input mic -C julian.jconf')

	while True:
		try:
			child.expect('please speak')
			process_julius(child.before)
		except KeyboardInterrupt:
			child.close(force=True)
			break
